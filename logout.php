<?php 
  require_once("./api/config.php");
  require_once("./api/auto_back.php");
  unset($_SESSION["username"]);
  header('Location: login.php');
?>