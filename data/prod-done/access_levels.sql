-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 01, 2022 at 07:17 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `thehungrypita`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_levels`
--

CREATE TABLE `access_levels` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `access_dashboard` int(11) NOT NULL DEFAULT 0,
  `access_access_levels` int(11) NOT NULL DEFAULT 0,
  `access_user_accounts` int(11) NOT NULL DEFAULT 0,
  `access_franchise_accounts` int(11) NOT NULL DEFAULT 0,
  `access_commissary_accounts` int(11) NOT NULL DEFAULT 0,
  `access_employee_entries` int(11) NOT NULL DEFAULT 0,
  `access_payroll` int(11) NOT NULL DEFAULT 0,
  `access_product_information` int(11) NOT NULL DEFAULT 0,
  `access_purchase_order` int(11) NOT NULL DEFAULT 0,
  `access_franchise_branch` int(11) NOT NULL DEFAULT 0,
  `access_franchise_entries` int(11) NOT NULL DEFAULT 0,
  `access_billing_franchising` int(11) NOT NULL DEFAULT 0,
  `access_billing_payments` int(11) NOT NULL DEFAULT 0,
  `access_order_payments` int(11) NOT NULL DEFAULT 0,
  `access_expenses` int(11) NOT NULL DEFAULT 0,
  `access_reports` int(11) NOT NULL DEFAULT 0,
  `access_logs` int(11) NOT NULL DEFAULT 0,
  `access_approval_power` int(11) NOT NULL DEFAULT 0,
  `access_order` int(11) NOT NULL DEFAULT 0,
  `access_order_tracker` int(11) NOT NULL DEFAULT 0,
  `access_return_goods` int(11) NOT NULL DEFAULT 0,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `access_report_soa` int(11) NOT NULL DEFAULT 0,
  `access_report_sales` int(11) NOT NULL DEFAULT 0,
  `access_report_returns` int(11) NOT NULL DEFAULT 0,
  `access_report_payroll` int(11) NOT NULL DEFAULT 0,
  `access_report_po` int(11) NOT NULL DEFAULT 0,
  `access_report_billing` int(11) NOT NULL DEFAULT 0,
  `access_report_orders` int(11) NOT NULL DEFAULT 0,
  `access_report_expense_summary` int(11) NOT NULL DEFAULT 0,
  `access_report_detailed_expense` int(11) NOT NULL DEFAULT 0,
  `access_report_renewal_logs` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `access_levels`
--

INSERT INTO `access_levels` (`id`, `description`, `access_dashboard`, `access_access_levels`, `access_user_accounts`, `access_franchise_accounts`, `access_commissary_accounts`, `access_employee_entries`, `access_encode_attendance`, `access_payroll`, `access_product_information`, `access_purchase_order`, `access_franchise_branch`, `access_franchise_entries`, `access_billing_franchising`, `access_billing_payments`, `access_order_payments`, `access_expenses`, `access_reports`, `access_logs`, `access_approval_power`, `access_order`, `access_order_tracker`, `access_return_goods`, `is_deleted`, `access_report_soa`, `access_report_sales`, `access_report_returns`, `access_report_payroll`, `access_report_po`, `access_report_billing`, `access_report_orders`, `access_report_expense_summary`, `access_report_detailed_expense`, `access_report_renewal_logs`) VALUES
(3, 'Administrator', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 'User', 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(45, 'Customer', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_levels`
--
ALTER TABLE `access_levels`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access_levels`
--
ALTER TABLE `access_levels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
