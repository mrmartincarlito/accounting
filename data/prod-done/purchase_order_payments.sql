-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 27, 2022 at 02:46 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `thehungrypita`
--

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order_payments`
--

CREATE TABLE `purchase_order_payments` (
  `id` int(11) NOT NULL,
  `po_number` varchar(50) NOT NULL,
  `amount` decimal(10,0) NOT NULL,
  `check_no` varchar(100) NOT NULL,
  `check_details` varchar(100) NOT NULL,
  `bank` varchar(100) NOT NULL,
  `added_by` varchar(100) NOT NULL,
  `date_paid` date NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `remarks` varchar(100) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `purchase_order_payments`
--

INSERT INTO `purchase_order_payments` (`id`, `po_number`, `amount`, `check_no`, `check_details`, `bank`, `added_by`, `date_paid`, `date_time`, `remarks`, `is_deleted`) VALUES
(1, 'D-0001', '1000', '', '', '', 'admin', '2022-02-27', '2022-02-27 01:30:19', '', 0),
(2, 'D-0001', '1000', '', '', '', 'admin', '2022-02-27', '2022-02-27 01:37:16', '', 1),
(3, 'D-0001', '1000', '', '', '', 'admin', '2022-02-27', '2022-02-27 01:35:44', 'update\r\n', 0),
(4, 'D-0001', '200', '', '', '', 'admin', '2022-02-24', '2022-02-27 01:45:29', '', 0),
(5, 'D-0002', '200', '', '', '', 'admin', '2022-02-23', '2022-02-27 01:46:05', '', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `purchase_order_payments`
--
ALTER TABLE `purchase_order_payments`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `purchase_order_payments`
--
ALTER TABLE `purchase_order_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
