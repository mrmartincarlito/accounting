-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 01, 2022 at 01:05 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `thehungrypita`
--

-- --------------------------------------------------------

--
-- Table structure for table `disposal_items`
--

CREATE TABLE `disposal_items` (
  `id` int(11) NOT NULL,
  `disposal_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `qty` float NOT NULL,
  `uom` text NOT NULL,
  `cost` decimal(10,2) NOT NULL,
  `total_amount` decimal(10,2) NOT NULL,
  `reuse_item_id` int(11) NOT NULL,
  `is_disposed` int(11) NOT NULL DEFAULT 0,
  `is_po_received` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `disposal_items`
--

INSERT INTO `disposal_items` (`id`, `disposal_id`, `item_id`, `qty`, `uom`, `cost`, `total_amount`, `reuse_item_id`, `is_disposed`, `is_po_received`) VALUES
(1, 3, 21, 5, 'kg', '1000.00', '5000.00', 22, 0, 0),
(3, 3, 25, 30, 'pc', '20.00', '600.00', 22, 0, 0),
(4, 2, 21, 5, 'kg', '1000.00', '5000.00', 0, 1, 0),
(5, 4, 29, 20, 'kg', '20.00', '400.00', 28, 0, 0),
(6, 5, 29, 25, 'kg', '20.00', '500.00', 21, 0, 0),
(7, 5, 28, 1, 'kg', '20.00', '20.00', 21, 0, 0),
(8, 1, 21, 1, 'kg', '1000.00', '1000.00', 0, 1, 0),
(9, 6, 21, 3, 'can', '1000.00', '3000.00', 0, 0, 1),
(10, 7, 24, 5, 'kg', '200.00', '1000.00', 0, 0, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `disposal_items`
--
ALTER TABLE `disposal_items`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `disposal_items`
--
ALTER TABLE `disposal_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
