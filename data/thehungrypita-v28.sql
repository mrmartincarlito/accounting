-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 22, 2022 at 08:46 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `thehungrypita`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_levels`
--

CREATE TABLE `access_levels` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `access_dashboard` int(11) NOT NULL DEFAULT 0,
  `access_access_levels` int(11) NOT NULL DEFAULT 0,
  `access_user_accounts` int(11) NOT NULL DEFAULT 0,
  `access_franchise_accounts` int(11) NOT NULL DEFAULT 0,
  `access_commissary_accounts` int(11) NOT NULL DEFAULT 0,
  `access_employee_entries` int(11) NOT NULL DEFAULT 0,
  `access_encode_attendance` int(11) NOT NULL DEFAULT 0,
  `access_payroll` int(11) NOT NULL DEFAULT 0,
  `access_product_information` int(11) NOT NULL DEFAULT 0,
  `access_purchase_order` int(11) NOT NULL DEFAULT 0,
  `access_franchise_branch` int(11) NOT NULL DEFAULT 0,
  `access_franchise_entries` int(11) NOT NULL DEFAULT 0,
  `access_billing_franchising` int(11) NOT NULL DEFAULT 0,
  `access_billing_payments` int(11) NOT NULL DEFAULT 0,
  `access_order_payments` int(11) NOT NULL DEFAULT 0,
  `access_expenses` int(11) NOT NULL DEFAULT 0,
  `access_reports` int(11) NOT NULL DEFAULT 0,
  `access_logs` int(11) NOT NULL DEFAULT 0,
  `access_approval_power` int(11) NOT NULL DEFAULT 0,
  `access_order` int(11) NOT NULL DEFAULT 0,
  `access_order_tracker` int(11) NOT NULL DEFAULT 0,
  `access_return_goods` int(11) NOT NULL DEFAULT 0,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `access_levels`
--

INSERT INTO `access_levels` (`id`, `description`, `access_dashboard`, `access_access_levels`, `access_user_accounts`, `access_franchise_accounts`, `access_commissary_accounts`, `access_employee_entries`, `access_encode_attendance`, `access_payroll`, `access_product_information`, `access_purchase_order`, `access_franchise_branch`, `access_franchise_entries`, `access_billing_franchising`, `access_billing_payments`, `access_order_payments`, `access_expenses`, `access_reports`, `access_logs`, `access_approval_power`, `access_order`, `access_order_tracker`, `access_return_goods`, `is_deleted`) VALUES
(3, 'Administrator', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0),
(4, 'User', 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(45, 'Customer', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `image_file` text DEFAULT NULL,
  `is_deleted` int(1) DEFAULT 0,
  `date` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `first_name`, `middle_name`, `last_name`, `email`, `username`, `password`, `role`, `image_file`, `is_deleted`, `date`) VALUES
(1, 'Administrator', 'A', 'Admin', 'admin@gmail.com', 'admin', '$2y$10$7e9rSp7gC.GslurbeSlIL.6NnSdN6/ejq0u/ueIn.00Kc7bMnxWBa', '3', 'uploads/532d0341186be12412c6a4362cc08710.jpg', 0, '2021-11-11 07:55:12');

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE `attendance` (
  `id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `total_minutes` decimal(10,2) NOT NULL,
  `regular_legal_ot` decimal(10,2) NOT NULL,
  `special_ot` decimal(10,2) NOT NULL,
  `special_holiday` decimal(10,2) NOT NULL,
  `legal_holiday` decimal(10,2) NOT NULL,
  `total_hours` decimal(11,2) NOT NULL,
  `legend` varchar(50) NOT NULL,
  `work_date` date NOT NULL,
  `added_by` text NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `attendance`
--

INSERT INTO `attendance` (`id`, `emp_id`, `total_minutes`, `regular_legal_ot`, `special_ot`, `special_holiday`, `legal_holiday`, `total_hours`, `legend`, `work_date`, `added_by`, `is_deleted`, `date_time`) VALUES
(26, 7, '9.00', '38.00', '24.00', '0.00', '0.00', '1.28', 'PRESENT', '2012-08-26', 'admin', 0, '2021-10-12 04:26:26'),
(27, 7, '480.00', '0.00', '0.00', '0.00', '0.00', '8.00', 'PRESENT', '2012-08-27', 'admin', 0, '2021-10-13 23:47:01'),
(28, 10, '480.00', '0.00', '0.00', '0.00', '0.00', '8.00', 'PRESENT', '2021-10-14', 'admin', 0, '2021-10-13 23:47:36'),
(29, 5, '92.00', '98.00', '62.00', '0.00', '0.00', '4.35', 'PRESENT', '2014-04-07', 'admin', 0, '2021-10-13 23:53:15'),
(30, 7, '51.00', '100.00', '56.00', '0.00', '0.00', '3.55', 'DAY-OFF', '1993-07-06', 'admin', 0, '2021-10-13 23:53:19'),
(31, 5, '48.00', '42.00', '38.00', '0.00', '0.00', '2.31', 'DAY-OFF', '2018-08-25', 'admin', 0, '2021-10-13 23:53:22'),
(32, 12, '92.00', '81.00', '7.00', '0.00', '0.00', '3.10', 'PRESENT', '1973-07-25', 'admin', 0, '2021-10-13 23:53:25'),
(33, 10, '86.00', '77.00', '46.00', '0.00', '0.00', '3.31', 'DAY-OFF', '1980-02-11', 'admin', 0, '2021-10-13 23:53:30'),
(34, 9, '100.00', '100.00', '62.00', '0.00', '0.00', '4.45', 'DAY-OFF', '2007-01-28', 'admin', 0, '2021-10-13 23:53:33'),
(35, 7, '39.00', '6.00', '21.00', '0.00', '0.00', '1.27', 'DAY-OFF', '1985-10-27', 'admin', 0, '2021-10-13 23:53:36'),
(36, 13, '3.00', '66.00', '51.00', '480.00', '480.00', '10.00', 'DAY-OFF', '1994-02-25', 'admin', 0, '2021-10-15 07:06:26'),
(37, 1, '6240.00', '0.00', '0.00', '0.00', '0.00', '104.00', 'PRESENT', '2021-10-22', 'admin', 0, '2021-10-15 07:22:43');

-- --------------------------------------------------------

--
-- Table structure for table `billing_franchise`
--

CREATE TABLE `billing_franchise` (
  `id` int(11) NOT NULL,
  `franchise_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `total_amount` decimal(10,2) NOT NULL DEFAULT 0.00,
  `or_number` text NOT NULL,
  `invoice_number` text DEFAULT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'UNPAID',
  `remarks` text NOT NULL,
  `added_by` text NOT NULL,
  `date_time` date NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `billing_franchise`
--

INSERT INTO `billing_franchise` (`id`, `franchise_id`, `description`, `total_amount`, `or_number`, `invoice_number`, `status`, `remarks`, `added_by`, `date_time`, `update_date`, `is_deleted`) VALUES
(23, 15, 'FRANCHISING FEE', '2700.00', '2222', '2222', 'PAID', '', 'admin', '2021-12-20', '2021-12-20 00:13:43', 0),
(24, 16, 'SECURITY DEPOSITS', '5000.00', '', '', 'UNPAID', '', 'admin', '2021-12-20', '2021-12-20 00:13:43', 0),
(25, 16, 'SECURITY DEPOSITS', '500.00', '', '', 'UNPAID', '', 'admin', '2021-12-20', '2021-12-20 02:22:50', 0);

-- --------------------------------------------------------

--
-- Table structure for table `billing_payments`
--

CREATE TABLE `billing_payments` (
  `id` int(11) NOT NULL,
  `franchise_id` int(11) DEFAULT NULL,
  `bill_id` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL,
  `image_file` text DEFAULT NULL,
  `check_no` text DEFAULT NULL,
  `check_details` varchar(50) DEFAULT NULL,
  `bank` varchar(50) DEFAULT NULL,
  `date_paid` date DEFAULT NULL,
  `remarks` text DEFAULT NULL,
  `status` varchar(50) DEFAULT 'PENDING',
  `or_number` text DEFAULT NULL,
  `invoice_number` text DEFAULT NULL,
  `approved_by` varchar(100) DEFAULT NULL,
  `approved_date` datetime DEFAULT NULL,
  `is_deleted` int(11) DEFAULT 0,
  `date_time` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `billing_payments`
--

INSERT INTO `billing_payments` (`id`, `franchise_id`, `bill_id`, `description`, `total_amount`, `image_file`, `check_no`, `check_details`, `bank`, `date_paid`, `remarks`, `status`, `or_number`, `invoice_number`, `approved_by`, `approved_date`, `is_deleted`, `date_time`) VALUES
(44, 15, 23, ' FRANCHISING FEE ', '2700.00', NULL, '', '', '', '2021-12-20', '', 'APPROVED', '2222', '2222', 'admin', '2021-12-20 07:09:07', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `description` varchar(50) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `description`, `is_deleted`, `date_time`) VALUES
(1, 'Cart 20', 0, '2021-10-23 21:31:07'),
(2, 'Cart 1', 0, '2021-10-23 21:53:48'),
(3, 'Cart 11', 0, '2021-10-23 22:22:17');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `category_name` varchar(50) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `type`, `category_name`, `is_deleted`, `date_time`) VALUES
(4, 'Non-Operational', 'Taxes', 0, '2021-10-24 00:02:08'),
(5, 'Non-Operational', 'Interest', 0, '2021-10-24 00:02:16'),
(6, 'Operational', 'Cost of Sales', 0, '2021-10-24 00:02:43'),
(7, 'Operational', 'Marketing, Advertising and Promotion', 0, '2021-10-24 00:03:14'),
(8, 'Operational', 'Travel Expenses', 0, '2021-10-24 00:03:25'),
(9, 'Operational', 'Administrative Expenses', 0, '2021-10-24 00:03:40'),
(10, 'Operational', 'Rent and Insurance', 0, '2021-10-24 00:04:00'),
(11, 'Operational', 'Depreciation and Amortization', 0, '2021-10-24 00:04:18'),
(12, 'Operational', 'Commission', 0, '2021-10-24 00:04:33'),
(13, 'Operational', 'In house expenses', 0, '2021-10-24 00:04:48');

-- --------------------------------------------------------

--
-- Table structure for table `commissary_accounts`
--

CREATE TABLE `commissary_accounts` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `username` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `password` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `image_file` text NOT NULL,
  `is_deleted` int(1) DEFAULT 0,
  `date` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `commissary_accounts`
--

INSERT INTO `commissary_accounts` (`id`, `first_name`, `middle_name`, `last_name`, `email`, `username`, `password`, `image_file`, `is_deleted`, `date`) VALUES
(4, 'Commissary', 'C', 'Commissary', 'commissary@gmail.com', 'commissary', '$2y$10$LYhi9uQUgs5pOSI9sqgF8.jG7Yab94vXpe5qDQQsL/YYAN30V9coC', 'uploads/The Hungry Pita_Accounting_BG.png', 0, '2021-11-30 23:10:50');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `province` varchar(100) DEFAULT NULL,
  `employment_date` date DEFAULT NULL,
  `regularization_date` date DEFAULT NULL,
  `employment_type` varchar(100) DEFAULT NULL,
  `rate` decimal(11,2) DEFAULT NULL,
  `date` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_deleted` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `first_name`, `middle_name`, `last_name`, `city`, `province`, `employment_date`, `regularization_date`, `employment_type`, `rate`, `date`, `is_deleted`) VALUES
(15, 'Theodore', 'Basil Cline', 'Johnston', 'Vitae id neque aperi', 'Voluptatem voluptat', '1980-01-07', '1975-08-09', 'ADMIN - ACCOUNTING', '70.00', '2022-01-25 23:04:50', 0),
(16, 'Denise', 'Cherokee Lopez', 'Delacruz', 'Consequuntur adipisc', 'Sunt irure ut et su', '2003-05-16', '1985-11-14', 'INVOICING OFFICER', '86.00', '2022-01-25 23:04:54', 0),
(17, 'Kiara', 'Brock Robles', 'Greene', 'Iste soluta cupidata', 'Exercitationem exerc', '1994-12-08', '1991-11-18', 'SALES AND MARKETING OFFICER', '69.00', '2022-01-25 23:04:59', 0);

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE `expenses` (
  `id` int(11) NOT NULL,
  `date_paid` date DEFAULT NULL,
  `expense_type` varchar(50) DEFAULT NULL,
  `voucher_number` int(11) DEFAULT NULL,
  `category` varchar(50) DEFAULT NULL,
  `sub_category` varchar(50) DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL,
  `added_by` varchar(50) DEFAULT NULL,
  `remarks` text DEFAULT NULL,
  `is_deleted` int(11) DEFAULT 0,
  `date_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `expenses`
--

INSERT INTO `expenses` (`id`, `date_paid`, `expense_type`, `voucher_number`, `category`, `sub_category`, `total_amount`, `added_by`, `remarks`, `is_deleted`, `date_time`) VALUES
(5, '1978-07-21', 'Operational', 497, 'Cost of Sales', '', '88.00', 'admin', 'Consequat Magnam ab', 0, '2021-12-09 00:09:02'),
(6, '1992-08-24', 'Non-Operational', 518, 'Taxes', '', '81.00', 'admin', 'Laboris dolorem moll', 0, '2021-12-09 00:09:18'),
(7, '2016-07-26', 'Non-Operational', 829, 'Taxes', '', '100.00', 'admin', 'Ad sunt excepteur qu', 0, '2021-12-09 00:10:05'),
(8, '1995-08-10', 'Operational', 172, 'Cost of Sales', '', '34.00', 'admin', 'Elit aut reiciendis', 0, '2021-12-15 23:27:39'),
(9, '1973-07-10', 'Operational', 83, 'Cost of Sales', 'Cost of Goods', '38.00', 'admin', 'At nulla sed ut quam', 0, '2022-02-15 02:58:11');

-- --------------------------------------------------------

--
-- Table structure for table `formulation`
--

CREATE TABLE `formulation` (
  `form_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `date_process` date NOT NULL,
  `remarks` text NOT NULL,
  `added_by` text NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `formulation`
--

INSERT INTO `formulation` (`form_id`, `item_id`, `description`, `date_process`, `remarks`, `added_by`, `date_time`, `is_deleted`) VALUES
(5, 21, 'Sample ITEM 1', '2021-12-04', 'N/A', 'commissary', '2021-12-03 22:45:24', 0),
(6, 24, 'SAmple for Item 4', '2021-12-04', 'N/A', 'commissary', '2021-12-03 22:51:55', 0),
(7, 22, 'Sample Formulation for Sample Item 2', '2021-12-04', 'N/A', 'commissary', '2021-12-04 06:54:38', 0),
(8, 23, 'For Item 3', '2021-12-05', 'N/A', 'commissary', '2021-12-05 02:10:39', 0);

-- --------------------------------------------------------

--
-- Table structure for table `formulation_details`
--

CREATE TABLE `formulation_details` (
  `id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `raw_item_id` int(11) NOT NULL,
  `qty` float NOT NULL,
  `orig_qty` float NOT NULL,
  `orig_uom` varchar(10) NOT NULL,
  `req_qty` float NOT NULL,
  `req_uom` varchar(10) NOT NULL,
  `remarks` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `formulation_details`
--

INSERT INTO `formulation_details` (`id`, `form_id`, `raw_item_id`, `qty`, `orig_qty`, `orig_uom`, `req_qty`, `req_uom`, `remarks`) VALUES
(8, 4, 25, 25, 0, '', 0, '', 'N/A'),
(9, 4, 27, 6, 0, '', 0, '', 'N/A'),
(10, 4, 29, 10, 0, '', 0, '', 'N/A'),
(11, 2, 25, 25, 0, '', 0, '', 'N/A'),
(12, 2, 28, 25, 0, '', 0, '', 'N/A'),
(13, 5, 25, 2, 0, '', 0, '', 'N/A'),
(14, 5, 26, 2, 0, '', 0, '', 'N/A'),
(15, 5, 28, 5, 0, '', 0, '', 'N/A'),
(16, 6, 27, 10, 0, '', 0, '', 'N/A'),
(18, 7, 26, 5, 0, '', 0, '', 'N/A'),
(26, 8, 22, 0.2, 1, 'sack', 5, 'kg', 'N/A'),
(27, 8, 25, 1, 1, 'powder', 1, 'tbsp', 'N/A');

-- --------------------------------------------------------

--
-- Table structure for table `franchisee`
--

CREATE TABLE `franchisee` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `mode_of_transaction` varchar(50) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `contactno` varchar(50) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `tin_number` text DEFAULT NULL,
  `package_type` varchar(50) DEFAULT NULL,
  `date_contract_signing` date DEFAULT NULL,
  `date_branch_opening` date DEFAULT NULL,
  `date_contract_expiry` date DEFAULT NULL,
  `delivery_charge` decimal(10,2) NOT NULL DEFAULT 0.00,
  `special_price_tag` varchar(10) NOT NULL,
  `billing_balance` decimal(10,2) DEFAULT 0.00,
  `order_balance` decimal(10,2) DEFAULT 0.00,
  `total_amount` decimal(10,2) DEFAULT 0.00,
  `last_payment_amount` decimal(10,2) DEFAULT NULL,
  `last_payment_date` datetime DEFAULT NULL,
  `is_deleted` int(11) DEFAULT 0,
  `date_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `franchisee`
--

INSERT INTO `franchisee` (`id`, `branch_id`, `mode_of_transaction`, `name`, `address`, `contactno`, `birthday`, `tin_number`, `package_type`, `date_contract_signing`, `date_branch_opening`, `date_contract_expiry`, `delivery_charge`, `special_price_tag`, `billing_balance`, `order_balance`, `total_amount`, `last_payment_amount`, `last_payment_date`, `is_deleted`, `date_time`) VALUES
(15, 12, 'Delivery', 'Franchise 1', 'Address', '09107102498', '2021-11-17', '33444434', '1', '2021-11-19', '2021-12-01', '2025-02-16', '50.00', 'NO', '0.00', '-2470.00', '-2470.00', '2720.00', '2022-02-22 00:00:00', 0, '2022-02-21 23:02:20'),
(16, 13, 'Delivery', 'Blaine Gibbs', 'Suscipit exercitatio', '92', '2004-10-06', '269', '2', '1974-01-04', '1993-11-22', '1970-09-27', '100.00', 'YES', '5500.00', '8535.00', '14035.00', '20000.00', '2014-02-02 00:00:00', 0, '2022-02-21 23:24:08'),
(17, 12, 'Delivery', 'Stewart Ellis', 'Ullamco velit qui p', '64', '2011-06-07', '621', '1', '2007-09-13', '1996-08-22', '1996-03-05', '25.00', 'YES', '0.00', '0.00', '0.00', '0.00', NULL, 0, '2021-12-19 23:06:45');

-- --------------------------------------------------------

--
-- Table structure for table `franchise_accounts`
--

CREATE TABLE `franchise_accounts` (
  `id` int(11) NOT NULL,
  `franchisee_id` int(11) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `username` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `password` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `is_deleted` int(1) DEFAULT 0,
  `image_file` text DEFAULT '',
  `date` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `franchise_accounts`
--

INSERT INTO `franchise_accounts` (`id`, `franchisee_id`, `first_name`, `middle_name`, `last_name`, `email`, `username`, `password`, `is_deleted`, `image_file`, `date`) VALUES
(4, 15, 'Franchise 1', 'F', 'Franchise 1', 'franchise@gmail.com', 'test', '$2y$10$3K0lir.wBKzkoHlY/8LBv.s4ne5O0S.jkxPsq4NrszMUxMrWzropW', 0, '', '2021-11-11 23:21:24'),
(5, 16, 'Harper', 'Jemima Stokes', 'Vazquez', 'cezun@mailinator.com', 'test2', '$2y$10$L47n04is8vXthPHVqX7/NeGD7uCxZKjkY8WLeMlik/WyILo.UBNzK', 0, '', '2021-11-14 00:54:07');

-- --------------------------------------------------------

--
-- Table structure for table `franchise_branch`
--

CREATE TABLE `franchise_branch` (
  `id` int(11) NOT NULL,
  `branch_code` varchar(100) NOT NULL,
  `branch_name` varchar(100) NOT NULL,
  `branch_location` varchar(100) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `franchise_branch`
--

INSERT INTO `franchise_branch` (`id`, `branch_code`, `branch_name`, `branch_location`, `is_deleted`, `date_time`) VALUES
(12, 'B-0001', 'Branch 1', 'Branch 1', 0, '2021-11-11 23:19:22'),
(13, 'B-0002', 'Branch 2', 'Malolos', 0, '2021-12-20 02:51:45');

-- --------------------------------------------------------

--
-- Table structure for table `franchise_renewal_log`
--

CREATE TABLE `franchise_renewal_log` (
  `id` int(11) NOT NULL,
  `franchise_id` int(11) DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `renewed_date` date DEFAULT NULL,
  `new_expiry_date` date DEFAULT NULL,
  `approved_by` text DEFAULT NULL,
  `date_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `franchise_renewal_log`
--

INSERT INTO `franchise_renewal_log` (`id`, `franchise_id`, `expiry_date`, `renewed_date`, `new_expiry_date`, `approved_by`, `date_time`) VALUES
(10, 15, '2022-01-28', '2022-02-18', '2025-02-16', 'admin', '2022-02-15 23:17:25');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `account_id` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `hostname` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `module` text NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `account_id`, `description`, `ip`, `hostname`, `timestamp`, `module`, `is_deleted`) VALUES
(959, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-11 23:18:44', 'ACCOUNTING', 0),
(960, 'Administrator A Admin', 'Add account: branch 1', '::1', 'DESKTOP-FO8SD5D', '2021-11-11 23:19:22', 'ACCOUNTING', 0),
(961, 'Administrator A Admin', 'Add franchisee: franchise 1', '::1', 'DESKTOP-FO8SD5D', '2021-11-11 23:20:00', 'ACCOUNTING', 0),
(962, 'Administrator A Admin', 'Add supplier name: supplier 1', '::1', 'DESKTOP-FO8SD5D', '2021-11-11 23:20:20', 'ACCOUNTING', 0),
(963, 'Administrator A Admin', 'Add product: item 1', '::1', 'DESKTOP-FO8SD5D', '2021-11-11 23:20:38', 'ACCOUNTING', 0),
(964, 'Administrator A Admin', 'Add product: item 2', '::1', 'DESKTOP-FO8SD5D', '2021-11-11 23:20:51', 'ACCOUNTING', 0),
(965, 'Administrator A Admin', 'Add account: franchise 1', '::1', 'DESKTOP-FO8SD5D', '2021-11-11 23:21:24', 'ACCOUNTING', 0),
(966, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-11 23:21:36', 'FRANCHISEE', 0),
(967, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-12 00:04:38', 'FRANCHISEE', 0),
(968, 'Franchise 1 F Franchise 1', 'Add order payment: 15', '::1', 'DESKTOP-FO8SD5D', '2021-11-12 00:04:51', 'FRANCHISEE', 0),
(969, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-12 08:27:16', 'FRANCHISEE', 0),
(970, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:06:36', 'ACCOUNTING', 0),
(971, 'Administrator A Admin', 'Edit product id 22', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:13:30', 'ACCOUNTING', 0),
(972, 'Administrator A Admin', 'Edit product id 22', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:14:09', 'ACCOUNTING', 0),
(973, 'Administrator A Admin', 'Edit product id 21', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:14:39', 'ACCOUNTING', 0),
(974, 'Administrator A Admin', 'Edit product id 22', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:14:48', 'ACCOUNTING', 0),
(975, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:16:35', 'ACCOUNTING', 0),
(976, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:16:52', 'FRANCHISEE', 0),
(977, 'Franchise 1 F Franchise 1', 'Edit order payment id 26', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:30:38', 'FRANCHISEE', 0),
(978, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:31:39', 'ACCOUNTING', 0),
(979, 'Administrator A Admin', 'Add bill to franchise: franchising fee', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:31:54', 'ACCOUNTING', 0),
(980, 'Franchise 1 F Franchise 1', 'Add billing payment: 11 | franchising fee | 5000.00', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:36:31', 'FRANCHISEE', 0),
(981, 'Administrator A Admin', 'Edit billing payment id 31', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:40:04', 'ACCOUNTING', 0),
(982, 'Franchise 1 F Franchise 1', 'Add billing payment: 11 | franchising fee | 5000.00', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:40:12', 'FRANCHISEE', 0),
(983, 'Administrator A Admin', 'Add bill to franchise: other franchising inclusion', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:40:34', 'ACCOUNTING', 0),
(984, 'Franchise 1 F Franchise 1', 'Add billing payment: 12 | other franchising inclusion | 8500.00', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:40:44', 'FRANCHISEE', 0),
(985, 'Administrator A Admin', 'Edit billing payment id 33', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:43:30', 'ACCOUNTING', 0),
(986, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:50:17', 'FRANCHISEE', 0),
(987, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:50:31', 'FRANCHISEE', 0),
(988, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:51:00', 'FRANCHISEE', 0),
(989, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:51:15', 'FRANCHISEE', 0),
(990, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:51:38', 'FRANCHISEE', 0),
(991, '  ', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:52:46', 'FRANCHISEE', 0),
(992, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:53:18', 'FRANCHISEE', 0),
(993, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:53:37', 'ACCOUNTING', 0),
(994, 'Administrator A Admin', 'Add franchisee: blaine gibbs', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:53:51', 'ACCOUNTING', 0),
(995, 'Administrator A Admin', 'Add account: harper', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:54:07', 'ACCOUNTING', 0),
(996, '  ', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:54:13', 'FRANCHISEE', 0),
(997, 'Harper Jemima Stokes Vazquez', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:54:20', 'FRANCHISEE', 0),
(998, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:56:33', 'FRANCHISEE', 0),
(999, 'Harper Jemima Stokes Vazquez', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:56:51', 'FRANCHISEE', 0),
(1000, 'Administrator A Admin', 'Add bill to franchise: franchising fee', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:57:07', 'ACCOUNTING', 0),
(1001, 'Harper Jemima Stokes Vazquez', 'Add billing payment: 13 | franchising fee | 2720.00', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:57:27', 'FRANCHISEE', 0),
(1002, 'Administrator A Admin', 'Edit billing payment id 34', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:57:42', 'ACCOUNTING', 0),
(1003, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:58:19', 'FRANCHISEE', 0),
(1004, 'Administrator A Admin', 'Add product: item 3', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 02:12:18', 'ACCOUNTING', 0),
(1005, 'Administrator A Admin', 'Add product: item 4', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 02:12:34', 'ACCOUNTING', 0),
(1006, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 02:13:31', 'FRANCHISEE', 0),
(1007, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 02:13:49', 'FRANCHISEE', 0),
(1008, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 02:14:08', 'FRANCHISEE', 0),
(1009, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 02:14:23', 'FRANCHISEE', 0),
(1010, 'Administrator A Admin', 'Edit product id 24', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 02:24:40', 'ACCOUNTING', 0),
(1011, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 04:57:52', 'FRANCHISEE', 0),
(1012, 'Harper Jemima Stokes Vazquez', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 05:04:10', 'FRANCHISEE', 0),
(1013, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 05:33:57', 'FRANCHISEE', 0),
(1014, 'Harper Jemima Stokes Vazquez', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 05:47:12', 'FRANCHISEE', 0),
(1015, 'Administrator A Admin', 'Edit billing payment id 32', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 05:47:47', 'ACCOUNTING', 0),
(1016, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-30 22:58:00', 'ACCOUNTING', 0),
(1017, 'Administrator A Admin', 'Add account: commissary', '::1', 'DESKTOP-FO8SD5D', '2021-11-30 22:58:44', 'ACCOUNTING', 0),
(1018, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-30 23:03:22', 'ACCOUNTING', 0),
(1019, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-30 23:03:29', 'ACCOUNTING', 0),
(1020, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-30 23:12:18', 'ACCOUNTING', 0),
(1021, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-30 23:14:23', 'ACCOUNTING', 0),
(1022, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-30 23:41:11', 'ACCOUNTING', 0),
(1023, 'Administrator A Admin', 'Add purchase order: d-0001', '::1', 'DESKTOP-FO8SD5D', '2021-11-30 23:41:41', 'ACCOUNTING', 0),
(1024, 'Administrator A Admin', 'Edit purcahse order id 12', '::1', 'DESKTOP-FO8SD5D', '2021-11-30 23:43:03', 'ACCOUNTING', 0),
(1025, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-30 23:47:21', 'ACCOUNTING', 0),
(1026, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-30 23:51:12', 'ACCOUNTING', 0),
(1027, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-30 23:53:01', 'ACCOUNTING', 0),
(1028, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-01 00:13:27', 'ACCOUNTING', 0),
(1029, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-01 00:14:03', 'ACCOUNTING', 0),
(1030, 'Commissary C Commissary', 'Add purchase order: d-0002', '::1', 'DESKTOP-FO8SD5D', '2021-12-01 00:50:33', 'ACCOUNTING', 0),
(1031, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-01 06:13:57', 'ACCOUNTING', 0),
(1032, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-01 23:06:42', 'ACCOUNTING', 0),
(1033, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 04:02:22', 'ACCOUNTING', 0),
(1034, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 07:35:48', 'ACCOUNTING', 0),
(1035, 'Administrator A Admin', 'Add purchase order: d-0001', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 07:36:03', 'ACCOUNTING', 0),
(1036, 'Administrator A Admin', 'Add purchase order: d-0002', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 07:36:30', 'ACCOUNTING', 0),
(1037, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 07:36:49', 'ACCOUNTING', 0),
(1038, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 08:26:35', 'FRANCHISEE', 0),
(1039, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 23:06:23', 'ACCOUNTING', 0),
(1040, 'Franchise 1 F Franchise 1', 'Add billing payment: 11 | franchising fee | 5000.00', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 23:21:27', 'FRANCHISEE', 0),
(1041, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 23:22:13', 'ACCOUNTING', 0),
(1042, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 23:23:41', 'ACCOUNTING', 0),
(1043, 'Commissary C Commissary', 'Add product: raw mat 1', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 23:33:24', 'ACCOUNTING', 0),
(1044, 'Commissary C Commissary', 'Add product: raw mat 2', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 23:33:46', 'ACCOUNTING', 0),
(1045, 'Commissary C Commissary', 'Add product: raw mat 3', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 23:34:12', 'ACCOUNTING', 0),
(1046, 'Commissary C Commissary', 'Add product: raw mat 4', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 23:34:33', 'ACCOUNTING', 0),
(1047, 'Commissary C Commissary', 'Add product: raw mat 5', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 23:34:58', 'ACCOUNTING', 0),
(1048, 'Commissary C Commissary', 'Add formulation: 21', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 23:53:07', 'ACCOUNTING', 0),
(1049, 'Commissary C Commissary', 'Add formulation: 21', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 23:53:13', 'ACCOUNTING', 0),
(1050, 'Commissary C Commissary', 'Add formulation: 21', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 23:54:33', 'ACCOUNTING', 0),
(1051, 'Commissary C Commissary', 'Edit formulation: 21', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:08:57', 'COMMISSARY', 0),
(1052, 'Commissary C Commissary', 'Delete formulation: 21', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:10:05', 'COMMISSARY', 0),
(1053, 'Commissary C Commissary', 'Add formulation: 21', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:10:17', 'COMMISSARY', 0),
(1054, 'Commissary C Commissary', 'Addraw formulation: 25', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:28:14', 'COMMISSARY', 0),
(1055, 'Commissary C Commissary', 'Addraw formulation: 25', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:32:19', 'COMMISSARY', 0),
(1056, 'Commissary C Commissary', 'Add formulation: 22', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:33:03', 'COMMISSARY', 0),
(1057, 'Commissary C Commissary', 'Addraw formulation: 27', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:33:17', 'COMMISSARY', 0),
(1058, 'Commissary C Commissary', 'Delete formulation: 22', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:34:28', 'COMMISSARY', 0),
(1059, 'Commissary C Commissary', 'Add formulation: 22', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:37:30', 'COMMISSARY', 0),
(1060, 'Commissary C Commissary', 'Addraw formulation: 26', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:37:37', 'COMMISSARY', 0),
(1061, 'Commissary C Commissary', 'Addraw formulation: 26', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:38:32', 'COMMISSARY', 0),
(1062, 'Commissary C Commissary', 'Edit formulation: 22', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:38:46', 'COMMISSARY', 0),
(1063, 'Commissary C Commissary', 'Addraw formulation: 25', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:38:58', 'COMMISSARY', 0),
(1064, 'Commissary C Commissary', 'Addraw formulation: 29', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:39:02', 'COMMISSARY', 0),
(1065, 'Commissary C Commissary', 'Addraw formulation: 25', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:40:23', 'COMMISSARY', 0),
(1066, 'Commissary C Commissary', 'Addraw formulation: 27', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:40:28', 'COMMISSARY', 0),
(1067, 'Commissary C Commissary', 'Addraw formulation: 29', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:40:32', 'COMMISSARY', 0),
(1068, 'Commissary C Commissary', 'Addraw formulation: 25', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:40:57', 'COMMISSARY', 0),
(1069, 'Commissary C Commissary', 'Addraw formulation: 28', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:44:18', 'COMMISSARY', 0),
(1070, 'Commissary C Commissary', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2021-12-03 12:16:54', 'COMMISSARY', 0),
(1071, 'Commissary C Commissary', 'New production plan :jo 1', '::1', 'LAPTOP-UI866FP5', '2021-12-03 13:11:09', 'COMMISSARY', 0),
(1072, 'Commissary C Commissary', 'New production plan :sample jo', '::1', 'LAPTOP-UI866FP5', '2021-12-03 13:24:00', 'COMMISSARY', 0),
(1073, 'Commissary C Commissary', 'New production plan :sample jo2', '::1', 'LAPTOP-UI866FP5', '2021-12-03 13:24:21', 'COMMISSARY', 0),
(1074, 'Commissary C Commissary', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2021-12-03 13:30:23', 'COMMISSARY', 0),
(1075, 'Commissary C Commissary', 'New production plan :sample 1', '::1', 'LAPTOP-UI866FP5', '2021-12-03 13:30:57', 'COMMISSARY', 0),
(1076, 'Commissary C Commissary', 'New production plan :sample 2', '::1', 'LAPTOP-UI866FP5', '2021-12-03 13:31:18', 'COMMISSARY', 0),
(1077, 'Commissary C Commissary', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2021-12-03 22:08:24', 'COMMISSARY', 0),
(1078, 'Commissary C Commissary', 'New production plan :sample', '::1', 'LAPTOP-UI866FP5', '2021-12-03 22:42:15', 'COMMISSARY', 0),
(1079, 'Commissary C Commissary', 'Add formulation: 21', '::1', 'LAPTOP-UI866FP5', '2021-12-03 22:45:24', 'COMMISSARY', 0),
(1080, 'Commissary C Commissary', 'Addraw formulation: 25', '::1', 'LAPTOP-UI866FP5', '2021-12-03 22:45:32', 'COMMISSARY', 0),
(1081, 'Commissary C Commissary', 'Addraw formulation: 26', '::1', 'LAPTOP-UI866FP5', '2021-12-03 22:45:38', 'COMMISSARY', 0),
(1082, 'Commissary C Commissary', 'Addraw formulation: 28', '::1', 'LAPTOP-UI866FP5', '2021-12-03 22:45:44', 'COMMISSARY', 0),
(1083, 'Commissary C Commissary', 'Add formulation: 24', '::1', 'LAPTOP-UI866FP5', '2021-12-03 22:45:58', 'COMMISSARY', 0),
(1084, 'Commissary C Commissary', 'Edit formulation: 21', '::1', 'LAPTOP-UI866FP5', '2021-12-03 22:46:16', 'COMMISSARY', 0),
(1085, 'Commissary C Commissary', 'Addraw formulation: 27', '::1', 'LAPTOP-UI866FP5', '2021-12-03 22:46:27', 'COMMISSARY', 0),
(1086, 'Commissary C Commissary', 'New production plan :job order1', '::1', 'LAPTOP-UI866FP5', '2021-12-03 22:46:57', 'COMMISSARY', 0),
(1087, 'Commissary C Commissary', 'New production plan :job order1', '::1', 'LAPTOP-UI866FP5', '2021-12-03 22:47:50', 'COMMISSARY', 0),
(1088, 'Commissary C Commissary', 'New production plan :test2', '::1', 'LAPTOP-UI866FP5', '2021-12-03 22:48:44', 'COMMISSARY', 0),
(1089, 'Commissary C Commissary', 'New production plan :test1', '::1', 'LAPTOP-UI866FP5', '2021-12-03 22:50:25', 'COMMISSARY', 0),
(1090, 'Commissary C Commissary', 'Edit formulation: 24', '::1', 'LAPTOP-UI866FP5', '2021-12-03 22:51:55', 'COMMISSARY', 0),
(1091, 'Commissary C Commissary', 'New production plan :test2', '::1', 'LAPTOP-UI866FP5', '2021-12-03 22:52:24', 'COMMISSARY', 0),
(1092, 'Commissary C Commissary', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2021-12-04 04:13:09', 'COMMISSARY', 0),
(1093, 'Commissary C Commissary', 'New production plan :test again', '::1', 'LAPTOP-UI866FP5', '2021-12-04 04:17:16', 'COMMISSARY', 0),
(1094, 'Administrator A Admin', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2021-12-04 04:37:42', 'ACCOUNTING', 0),
(1095, 'Administrator A Admin', 'Add purchase order: d-0003', '::1', 'LAPTOP-UI866FP5', '2021-12-04 04:38:00', 'ACCOUNTING', 0),
(1096, 'Administrator A Admin', 'Edit purcahse order id 16', '::1', 'LAPTOP-UI866FP5', '2021-12-04 04:38:36', 'ACCOUNTING', 0),
(1097, 'Commissary C Commissary', 'Marked production plan id as finished:1', '::1', 'LAPTOP-UI866FP5', '2021-12-04 04:42:29', 'COMMISSARY', 0),
(1098, 'Commissary C Commissary', 'Marked production plan id as finished:1', '::1', 'LAPTOP-UI866FP5', '2021-12-04 04:48:21', 'COMMISSARY', 0),
(1099, 'Commissary C Commissary', 'Marked production plan id as finished:1', '::1', 'LAPTOP-UI866FP5', '2021-12-04 04:48:21', 'COMMISSARY', 0),
(1100, 'Commissary C Commissary', 'Marked production plan id as finished:1', '::1', 'LAPTOP-UI866FP5', '2021-12-04 04:49:28', 'COMMISSARY', 0),
(1101, 'Commissary C Commissary', 'Marked production plan id as finished:1', '::1', 'LAPTOP-UI866FP5', '2021-12-04 04:49:28', 'COMMISSARY', 0),
(1102, 'Commissary C Commissary', 'Marked production plan id as finished:1', '::1', 'LAPTOP-UI866FP5', '2021-12-04 04:51:00', 'COMMISSARY', 0),
(1103, 'Commissary C Commissary', 'Marked production plan id as finished:1', '::1', 'LAPTOP-UI866FP5', '2021-12-04 04:51:00', 'COMMISSARY', 0),
(1104, 'Commissary C Commissary', 'Updated production plan id:3', '::1', 'LAPTOP-UI866FP5', '2021-12-04 04:57:21', 'COMMISSARY', 0),
(1105, 'Commissary C Commissary', 'New production plan :test3', '::1', 'LAPTOP-UI866FP5', '2021-12-04 04:58:47', 'COMMISSARY', 0),
(1106, 'Commissary C Commissary', 'Updated production plan id:4', '::1', 'LAPTOP-UI866FP5', '2021-12-04 04:59:01', 'COMMISSARY', 0),
(1107, 'Commissary C Commissary', 'New production plan :test1', '::1', 'LAPTOP-UI866FP5', '2021-12-04 05:01:59', 'COMMISSARY', 0),
(1108, 'Commissary C Commissary', 'New production plan :test1', '::1', 'LAPTOP-UI866FP5', '2021-12-04 05:03:41', 'COMMISSARY', 0),
(1109, 'Commissary C Commissary', 'Updated production plan id:1', '::1', 'LAPTOP-UI866FP5', '2021-12-04 05:03:55', 'COMMISSARY', 0),
(1110, 'Commissary C Commissary', 'Marked production plan id as finished:1', '::1', 'LAPTOP-UI866FP5', '2021-12-04 05:06:03', 'COMMISSARY', 0),
(1111, 'Commissary C Commissary', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2021-12-04 06:05:42', 'COMMISSARY', 0),
(1112, '  ', 'Logged in', '192.168.137.103', 'HUAWEI_Mate_30_Pro-f24a83.mshome.net', '2021-12-04 06:09:10', 'FRANCHISEE', 0),
(1113, 'Franchise 1 F Franchise 1', 'Logged in', '192.168.137.103', 'HUAWEI_Mate_30_Pro-f24a83.mshome.net', '2021-12-04 06:09:17', 'FRANCHISEE', 0),
(1114, 'Commissary C Commissary', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2021-12-04 06:14:24', 'COMMISSARY', 0),
(1115, '  ', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2021-12-04 06:14:56', 'FRANCHISEE', 0),
(1116, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2021-12-04 06:14:59', 'FRANCHISEE', 0),
(1117, 'Administrator A Admin', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2021-12-04 06:16:04', 'ACCOUNTING', 0),
(1118, 'Commissary C Commissary', 'Add formulation: 22', '::1', 'LAPTOP-UI866FP5', '2021-12-04 06:54:38', 'COMMISSARY', 0),
(1119, 'Commissary C Commissary', 'Addraw formulation: 25', '::1', 'LAPTOP-UI866FP5', '2021-12-04 06:54:57', 'COMMISSARY', 0),
(1120, 'Commissary C Commissary', 'Addraw formulation: 26', '::1', 'LAPTOP-UI866FP5', '2021-12-04 06:55:26', 'COMMISSARY', 0),
(1121, 'Commissary C Commissary', 'Addraw formulation: 28', '::1', 'LAPTOP-UI866FP5', '2021-12-04 06:55:31', 'COMMISSARY', 0),
(1122, 'Commissary C Commissary', 'New production plan :test jo', '::1', 'LAPTOP-UI866FP5', '2021-12-04 07:02:57', 'COMMISSARY', 0),
(1123, 'Commissary C Commissary', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2021-12-04 23:35:42', 'COMMISSARY', 0),
(1124, 'Commissary C Commissary', 'Marked production plan id as finished:2', '::1', 'LAPTOP-UI866FP5', '2021-12-04 23:44:21', 'COMMISSARY', 0),
(1125, 'Commissary C Commissary', 'New production plan :test3', '::1', 'LAPTOP-UI866FP5', '2021-12-04 23:49:30', 'COMMISSARY', 0),
(1126, 'Commissary C Commissary', 'New production plan :test', '::1', 'LAPTOP-UI866FP5', '2021-12-05 00:11:47', 'COMMISSARY', 0),
(1127, 'Commissary C Commissary', 'Updated production plan id:4', '::1', 'LAPTOP-UI866FP5', '2021-12-05 00:12:01', 'COMMISSARY', 0),
(1128, 'Commissary C Commissary', 'New production plan :', '::1', 'LAPTOP-UI866FP5', '2021-12-05 00:17:01', 'COMMISSARY', 0),
(1129, 'Commissary C Commissary', 'Updated production plan id:5', '::1', 'LAPTOP-UI866FP5', '2021-12-05 00:17:37', 'COMMISSARY', 0),
(1130, 'Commissary C Commissary', 'Updated production plan id:5', '::1', 'LAPTOP-UI866FP5', '2021-12-05 00:17:54', 'COMMISSARY', 0),
(1131, 'Commissary C Commissary', 'Marked production plan id as finished:4', '::1', 'LAPTOP-UI866FP5', '2021-12-05 00:30:34', 'COMMISSARY', 0),
(1132, 'Administrator A Admin', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2021-12-05 00:53:02', 'ACCOUNTING', 0),
(1133, 'Administrator A Admin', 'Add purchase order: d-0004', '::1', 'LAPTOP-UI866FP5', '2021-12-05 01:38:30', 'ACCOUNTING', 0),
(1134, 'Commissary C Commissary', 'New production plan :sample test again', '::1', 'LAPTOP-UI866FP5', '2021-12-05 01:45:22', 'COMMISSARY', 0),
(1135, 'Commissary C Commissary', 'Updated production plan id:6', '::1', 'LAPTOP-UI866FP5', '2021-12-05 01:45:51', 'COMMISSARY', 0),
(1136, 'Commissary C Commissary', 'Marked production plan id as finished:6', '::1', 'LAPTOP-UI866FP5', '2021-12-05 01:46:11', 'COMMISSARY', 0),
(1137, 'Commissary C Commissary', 'New production plan :test', '::1', 'LAPTOP-UI866FP5', '2021-12-05 01:53:45', 'COMMISSARY', 0),
(1138, 'Commissary C Commissary', 'New production plan :test again', '::1', 'LAPTOP-UI866FP5', '2021-12-05 01:55:40', 'COMMISSARY', 0),
(1139, 'Commissary C Commissary', 'Updated production plan id:8', '::1', 'LAPTOP-UI866FP5', '2021-12-05 01:56:03', 'COMMISSARY', 0),
(1140, 'Commissary C Commissary', 'Marked production plan id as finished:8', '::1', 'LAPTOP-UI866FP5', '2021-12-05 01:56:23', 'COMMISSARY', 0),
(1141, 'Administrator A Admin', 'Add purchase order: d-0005', '::1', 'LAPTOP-UI866FP5', '2021-12-05 01:57:49', 'ACCOUNTING', 0),
(1142, 'Commissary C Commissary', 'New production plan :for item 4', '::1', 'LAPTOP-UI866FP5', '2021-12-05 02:00:40', 'COMMISSARY', 0),
(1143, 'Commissary C Commissary', 'Marked production plan id as finished:9', '::1', 'LAPTOP-UI866FP5', '2021-12-05 02:00:57', 'COMMISSARY', 0),
(1144, 'Administrator A Admin', 'Add purchase order: d-0006', '::1', 'LAPTOP-UI866FP5', '2021-12-05 02:05:55', 'ACCOUNTING', 0),
(1145, 'Commissary C Commissary', 'New production plan :test again', '::1', 'LAPTOP-UI866FP5', '2021-12-05 02:09:05', 'COMMISSARY', 0),
(1146, 'Commissary C Commissary', 'Marked production plan id as finished:10', '::1', 'LAPTOP-UI866FP5', '2021-12-05 02:09:29', 'COMMISSARY', 0),
(1147, 'Commissary C Commissary', 'Add formulation: 23', '::1', 'LAPTOP-UI866FP5', '2021-12-05 02:10:39', 'COMMISSARY', 0),
(1148, 'Commissary C Commissary', 'Addraw formulation: 25', '::1', 'LAPTOP-UI866FP5', '2021-12-05 02:10:53', 'COMMISSARY', 0),
(1149, 'Commissary C Commissary', 'Addraw formulation: 26', '::1', 'LAPTOP-UI866FP5', '2021-12-05 02:10:57', 'COMMISSARY', 0),
(1150, 'Commissary C Commissary', 'Addraw formulation: 27', '::1', 'LAPTOP-UI866FP5', '2021-12-05 02:11:05', 'COMMISSARY', 0),
(1151, 'Commissary C Commissary', 'Addraw formulation: 28', '::1', 'LAPTOP-UI866FP5', '2021-12-05 02:11:11', 'COMMISSARY', 0),
(1152, 'Commissary C Commissary', 'Addraw formulation: 29', '::1', 'LAPTOP-UI866FP5', '2021-12-05 02:11:21', 'COMMISSARY', 0),
(1153, 'Commissary C Commissary', 'New production plan :test', '::1', 'LAPTOP-UI866FP5', '2021-12-05 02:11:44', 'COMMISSARY', 0),
(1154, 'Commissary C Commissary', 'Marked production plan id as finished:11', '::1', 'LAPTOP-UI866FP5', '2021-12-05 02:12:07', 'COMMISSARY', 0),
(1155, 'Franchise 1 F Franchise 1', 'Delete order payment id 26', '::1', 'LAPTOP-UI866FP5', '2021-12-05 02:19:37', 'FRANCHISEE', 0),
(1156, 'Franchise 1 F Franchise 1', 'Add order payment: 15', '::1', 'LAPTOP-UI866FP5', '2021-12-05 02:20:21', 'FRANCHISEE', 0),
(1157, 'Administrator A Admin', 'Edit order payment id 27', '::1', 'LAPTOP-UI866FP5', '2021-12-05 02:20:48', 'ACCOUNTING', 0),
(1158, 'Commissary C Commissary', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2021-12-05 06:20:29', 'COMMISSARY', 0),
(1159, 'Commissary C Commissary', 'Edit product id 29', '::1', 'LAPTOP-UI866FP5', '2021-12-05 06:41:09', 'COMMISSARY', 0),
(1160, 'Commissary C Commissary', 'Edit product id 24', '::1', 'LAPTOP-UI866FP5', '2021-12-05 06:42:11', 'COMMISSARY', 0),
(1161, 'Commissary C Commissary', 'Edit product id 23', '::1', 'LAPTOP-UI866FP5', '2021-12-05 06:42:17', 'COMMISSARY', 0),
(1162, 'Commissary C Commissary', 'Edit product id 23', '::1', 'LAPTOP-UI866FP5', '2021-12-05 06:42:28', 'COMMISSARY', 0),
(1163, 'Commissary C Commissary', 'New production plan :test', '::1', 'LAPTOP-UI866FP5', '2021-12-05 06:47:58', 'COMMISSARY', 0),
(1164, 'Commissary C Commissary', 'Marked production plan id as finished:12', '::1', 'LAPTOP-UI866FP5', '2021-12-05 06:50:00', 'COMMISSARY', 0),
(1165, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2021-12-05 06:57:53', 'FRANCHISEE', 0),
(1166, 'Franchise 1 F Franchise 1', 'Add order payment: 15', '::1', 'LAPTOP-UI866FP5', '2021-12-05 07:11:15', 'FRANCHISEE', 0),
(1167, 'Administrator A Admin', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2021-12-05 07:11:38', 'ACCOUNTING', 0),
(1168, 'Administrator A Admin', 'Edit order payment id 28', '::1', 'LAPTOP-UI866FP5', '2021-12-05 07:11:59', 'ACCOUNTING', 0),
(1169, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2021-12-05 07:17:19', 'FRANCHISEE', 0),
(1170, 'Harper Jemima Stokes Vazquez', 'Add order payment: 16', '::1', 'LAPTOP-UI866FP5', '2021-12-05 07:18:12', 'FRANCHISEE', 0),
(1171, 'Administrator A Admin', 'Edit order payment id 29', '::1', 'LAPTOP-UI866FP5', '2021-12-05 07:18:38', 'ACCOUNTING', 0),
(1172, 'Harper Jemima Stokes Vazquez', 'Add order payment: 16', '::1', 'LAPTOP-UI866FP5', '2021-12-05 07:18:56', 'FRANCHISEE', 0),
(1173, 'Administrator A Admin', 'Edit order payment id 30', '::1', 'LAPTOP-UI866FP5', '2021-12-05 07:19:13', 'ACCOUNTING', 0),
(1174, 'Commissary C Commissary', 'New production plan :test', '::1', 'LAPTOP-UI866FP5', '2021-12-05 07:21:38', 'COMMISSARY', 0),
(1175, 'Commissary C Commissary', 'Marked production plan id as finished:13', '::1', 'LAPTOP-UI866FP5', '2021-12-05 07:21:55', 'COMMISSARY', 0),
(1176, 'Administrator A Admin', 'Add franchisee: stewart ellis', '::1', 'LAPTOP-UI866FP5', '2021-12-05 07:33:53', 'ACCOUNTING', 0),
(1177, 'Administrator A Admin', 'Edit franchisee id 15', '::1', 'LAPTOP-UI866FP5', '2021-12-05 07:34:14', 'ACCOUNTING', 0),
(1178, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-05 23:06:38', 'ACCOUNTING', 0),
(1179, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-06 22:55:40', 'ACCOUNTING', 0),
(1180, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-06 23:23:43', 'COMMISSARY', 0),
(1181, 'Franchise 1 F Franchise 1', 'Add order payment: 15', '::1', 'DESKTOP-FO8SD5D', '2021-12-06 23:26:01', 'FRANCHISEE', 0),
(1182, 'Administrator A Admin', 'Edit order payment id 31', '::1', 'DESKTOP-FO8SD5D', '2021-12-06 23:27:31', 'ACCOUNTING', 0),
(1183, 'Franchise 1 F Franchise 1', 'Add order payment: 15', '::1', 'DESKTOP-FO8SD5D', '2021-12-06 23:30:46', 'FRANCHISEE', 0),
(1184, 'Administrator A Admin', 'Edit order payment id 32', '::1', 'DESKTOP-FO8SD5D', '2021-12-06 23:30:59', 'ACCOUNTING', 0),
(1185, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-08 23:05:15', 'ACCOUNTING', 0),
(1186, 'Administrator A Admin', 'Edit billing payment id 35', '::1', 'DESKTOP-FO8SD5D', '2021-12-09 00:08:49', 'ACCOUNTING', 0),
(1187, 'Administrator A Admin', 'Add expense: operational', '::1', 'DESKTOP-FO8SD5D', '2021-12-09 00:09:02', 'ACCOUNTING', 0),
(1188, 'Administrator A Admin', 'Add expense: non-operational', '::1', 'DESKTOP-FO8SD5D', '2021-12-09 00:09:18', 'ACCOUNTING', 0),
(1189, 'Administrator A Admin', 'Add expense: non-operational', '::1', 'DESKTOP-FO8SD5D', '2021-12-09 00:10:05', 'ACCOUNTING', 0),
(1190, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-09 22:55:33', 'ACCOUNTING', 0),
(1191, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-10 06:40:29', 'COMMISSARY', 0),
(1192, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-10 06:41:00', 'ACCOUNTING', 0),
(1193, 'Administrator A Admin', 'Add purchase order: d-0001', '::1', 'DESKTOP-FO8SD5D', '2021-12-10 07:15:14', 'ACCOUNTING', 0),
(1194, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-10 07:18:03', 'COMMISSARY', 0),
(1195, 'Commissary C Commissary', 'New production plan :item 3', '::1', 'DESKTOP-FO8SD5D', '2021-12-10 07:28:13', 'COMMISSARY', 0),
(1196, 'Commissary C Commissary', 'New production plan :item 2', '::1', 'DESKTOP-FO8SD5D', '2021-12-10 07:28:55', 'COMMISSARY', 0),
(1197, 'Commissary C Commissary', 'Marked production plan id as finished:2', '::1', 'DESKTOP-FO8SD5D', '2021-12-10 07:30:25', 'COMMISSARY', 0),
(1198, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-13 00:08:46', 'ACCOUNTING', 0),
(1199, 'Administrator A Admin', 'Add billing payment: 11 | franchising fee | 5000.00', '::1', 'DESKTOP-FO8SD5D', '2021-12-13 00:10:16', 'ACCOUNTING', 0),
(1200, 'Administrator A Admin', 'Edit billing payment id 36', '::1', 'DESKTOP-FO8SD5D', '2021-12-13 00:10:27', 'ACCOUNTING', 0),
(1201, 'Administrator A Admin', 'Add billing payment: 11 | franchising fee | 5000.00', '::1', 'DESKTOP-FO8SD5D', '2021-12-13 00:31:54', 'ACCOUNTING', 0),
(1202, 'Administrator A Admin', 'Delete bill to franchise 11', '::1', 'DESKTOP-FO8SD5D', '2021-12-13 00:35:38', 'ACCOUNTING', 0),
(1203, 'Administrator A Admin', 'Add bill to franchise: franchising fee', '::1', 'DESKTOP-FO8SD5D', '2021-12-13 00:35:46', 'ACCOUNTING', 0),
(1204, 'Administrator A Admin', 'Edit bill to franchise 14', '::1', 'DESKTOP-FO8SD5D', '2021-12-13 00:36:07', 'ACCOUNTING', 0),
(1205, 'Administrator A Admin', 'Delete bill to franchise 14', '::1', 'DESKTOP-FO8SD5D', '2021-12-13 00:38:01', 'ACCOUNTING', 0),
(1206, 'Administrator A Admin', 'Add bill to franchise: franchising fee', '::1', 'DESKTOP-FO8SD5D', '2021-12-13 00:38:09', 'ACCOUNTING', 0),
(1207, 'Administrator A Admin', 'Delete bill to franchise 15', '::1', 'DESKTOP-FO8SD5D', '2021-12-13 00:38:30', 'ACCOUNTING', 0),
(1208, 'Administrator A Admin', 'Add bill to franchise: franchising fee', '::1', 'DESKTOP-FO8SD5D', '2021-12-13 00:38:38', 'ACCOUNTING', 0),
(1209, 'Administrator A Admin', 'Delete bill to franchise 16', '::1', 'DESKTOP-FO8SD5D', '2021-12-13 00:40:05', 'ACCOUNTING', 0),
(1210, 'Administrator A Admin', 'Add bill to franchise: franchising fee', '::1', 'DESKTOP-FO8SD5D', '2021-12-13 00:40:41', 'ACCOUNTING', 0),
(1211, 'Administrator A Admin', 'Add bill to franchise: franchising fee', '::1', 'DESKTOP-FO8SD5D', '2021-12-13 00:41:04', 'ACCOUNTING', 0),
(1212, 'Administrator A Admin', 'Add bill to franchise: security deposits', '::1', 'DESKTOP-FO8SD5D', '2021-12-13 00:41:30', 'ACCOUNTING', 0),
(1213, 'Franchise 1 F Franchise 1', 'Add billing payment: 17 | franchising fee | 2720.00', '::1', 'DESKTOP-FO8SD5D', '2021-12-13 00:41:47', 'FRANCHISEE', 0),
(1214, 'Franchise 1 F Franchise 1', 'Add billing payment: 18 | franchising fee | 5500.00', '::1', 'DESKTOP-FO8SD5D', '2021-12-13 00:42:27', 'FRANCHISEE', 0),
(1215, 'Franchise 1 F Franchise 1', 'Add billing payment: 18 | franchising fee | 5500.00', '::1', 'DESKTOP-FO8SD5D', '2021-12-13 00:42:55', 'FRANCHISEE', 0),
(1216, 'Franchise 1 F Franchise 1', 'Add billing payment: 19 | security deposits | 650.00', '::1', 'DESKTOP-FO8SD5D', '2021-12-13 00:43:16', 'FRANCHISEE', 0),
(1217, 'Administrator A Admin', 'Edit access level id 3', '::1', 'DESKTOP-FO8SD5D', '2021-12-13 00:44:03', 'ACCOUNTING', 0),
(1218, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-13 23:08:17', 'ACCOUNTING', 0),
(1219, 'Administrator A Admin', 'Edit bill to franchise 19', '::1', 'DESKTOP-FO8SD5D', '2021-12-14 00:29:09', 'ACCOUNTING', 0),
(1220, 'Administrator A Admin', 'Edit bill to franchise 19', '::1', 'DESKTOP-FO8SD5D', '2021-12-14 00:31:58', 'ACCOUNTING', 0),
(1221, 'Administrator A Admin', 'Edit bill to franchise 18', '::1', 'DESKTOP-FO8SD5D', '2021-12-14 00:32:21', 'ACCOUNTING', 0),
(1222, 'Administrator A Admin', 'Delete bill to franchise 19', '::1', 'DESKTOP-FO8SD5D', '2021-12-14 00:34:36', 'ACCOUNTING', 0),
(1223, 'Administrator A Admin', 'Delete bill to franchise 18', '::1', 'DESKTOP-FO8SD5D', '2021-12-14 00:34:40', 'ACCOUNTING', 0),
(1224, 'Administrator A Admin', 'Delete bill to franchise 17', '::1', 'DESKTOP-FO8SD5D', '2021-12-14 00:34:58', 'ACCOUNTING', 0),
(1225, 'Administrator A Admin', 'Delete bill to franchise 13', '::1', 'DESKTOP-FO8SD5D', '2021-12-14 00:35:02', 'ACCOUNTING', 0),
(1226, 'Administrator A Admin', 'Delete bill to franchise 12', '::1', 'DESKTOP-FO8SD5D', '2021-12-14 00:35:06', 'ACCOUNTING', 0),
(1227, 'Administrator A Admin', 'Add bill to franchise: franchising fee', '::1', 'DESKTOP-FO8SD5D', '2021-12-14 00:37:27', 'ACCOUNTING', 0),
(1228, 'Administrator A Admin', 'Delete bill to franchise 20', '::1', 'DESKTOP-FO8SD5D', '2021-12-14 00:40:07', 'ACCOUNTING', 0),
(1229, 'Administrator A Admin', 'Add bill to franchise: franchising fee', '::1', 'DESKTOP-FO8SD5D', '2021-12-14 00:40:44', 'ACCOUNTING', 0),
(1230, 'Administrator A Admin', 'Add billing payment: 21 | franchising fee | 8500.00', '::1', 'DESKTOP-FO8SD5D', '2021-12-14 00:41:06', 'ACCOUNTING', 0),
(1231, 'Administrator A Admin', 'Edit access level id 3', '::1', 'DESKTOP-FO8SD5D', '2021-12-14 00:41:26', 'ACCOUNTING', 0),
(1232, 'Administrator A Admin', 'Edit bill to franchise 21', '::1', 'DESKTOP-FO8SD5D', '2021-12-14 00:41:59', 'ACCOUNTING', 0),
(1233, 'Administrator A Admin', 'Add bill to franchise: franchising fee', '::1', 'DESKTOP-FO8SD5D', '2021-12-14 06:55:13', 'ACCOUNTING', 0),
(1234, 'Administrator A Admin', 'Add billing payment: 22 | franchising fee | 1960.00', '::1', 'DESKTOP-FO8SD5D', '2021-12-14 06:55:32', 'ACCOUNTING', 0),
(1235, 'Administrator A Admin', 'Edit bill to franchise 22', '::1', 'DESKTOP-FO8SD5D', '2021-12-14 06:56:35', 'ACCOUNTING', 0),
(1236, 'Administrator A Admin', 'Add order payment: 15', '::1', 'DESKTOP-FO8SD5D', '2021-12-14 08:19:46', 'ACCOUNTING', 0),
(1237, 'Administrator A Admin', 'Edit order payment id 33', '::1', 'DESKTOP-FO8SD5D', '2021-12-14 08:19:54', 'ACCOUNTING', 0),
(1238, 'Administrator A Admin', 'Edit order payment id 33', '::1', 'DESKTOP-FO8SD5D', '2021-12-14 08:26:09', 'ACCOUNTING', 0),
(1239, 'Administrator A Admin', 'Edit order payment id 33', '::1', 'DESKTOP-FO8SD5D', '2021-12-14 08:28:50', 'ACCOUNTING', 0),
(1240, 'Administrator A Admin', 'Edit order payment id 33', '::1', 'DESKTOP-FO8SD5D', '2021-12-14 08:48:05', 'ACCOUNTING', 0),
(1241, 'Administrator A Admin', 'Add order payment: 16', '::1', 'DESKTOP-FO8SD5D', '2021-12-14 08:48:29', 'ACCOUNTING', 0),
(1242, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-14 22:57:20', 'ACCOUNTING', 0),
(1243, 'Administrator A Admin', 'Add order payment: 16', '::1', 'DESKTOP-FO8SD5D', '2021-12-14 22:57:47', 'ACCOUNTING', 0),
(1244, 'Administrator A Admin', 'Edit order payment id 35', '::1', 'DESKTOP-FO8SD5D', '2021-12-14 22:58:08', 'ACCOUNTING', 0),
(1245, 'Administrator A Admin', 'Add order payment: 16', '::1', 'DESKTOP-FO8SD5D', '2021-12-14 22:59:32', 'ACCOUNTING', 0),
(1246, 'Administrator A Admin', 'Edit order payment id 36', '::1', 'DESKTOP-FO8SD5D', '2021-12-14 22:59:46', 'ACCOUNTING', 0),
(1247, 'Administrator A Admin', 'Add order payment: 16', '::1', 'DESKTOP-FO8SD5D', '2021-12-14 23:00:04', 'ACCOUNTING', 0),
(1248, 'Administrator A Admin', 'Add order payment: 16', '::1', 'DESKTOP-FO8SD5D', '2021-12-14 23:02:12', 'ACCOUNTING', 0),
(1249, 'Administrator A Admin', 'Add order payment: 15', '::1', 'DESKTOP-FO8SD5D', '2021-12-14 23:03:00', 'ACCOUNTING', 0),
(1250, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-14 23:05:25', 'COMMISSARY', 0),
(1251, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-15 22:57:57', 'COMMISSARY', 0),
(1252, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-15 23:07:59', 'ACCOUNTING', 0),
(1253, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-15 23:25:04', 'COMMISSARY', 0),
(1254, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-15 23:26:02', 'ACCOUNTING', 0),
(1255, 'Administrator A Admin', 'Add purchase order: d-0002', '::1', 'DESKTOP-FO8SD5D', '2021-12-15 23:26:53', 'ACCOUNTING', 0),
(1256, 'Administrator A Admin', 'Add expense: operational', '::1', 'DESKTOP-FO8SD5D', '2021-12-15 23:27:39', 'ACCOUNTING', 0),
(1257, 'Administrator A Admin', 'Edit purcahse order id 2', '::1', 'DESKTOP-FO8SD5D', '2021-12-15 23:33:17', 'ACCOUNTING', 0),
(1258, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-15 23:34:50', 'COMMISSARY', 0),
(1259, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-16 08:32:25', 'COMMISSARY', 0),
(1260, 'Commissary C Commissary', 'New production plan :sample', '::1', 'DESKTOP-FO8SD5D', '2021-12-16 08:37:45', 'COMMISSARY', 0),
(1261, 'Commissary C Commissary', 'New production plan :', '::1', 'DESKTOP-FO8SD5D', '2021-12-16 08:39:45', 'COMMISSARY', 0),
(1262, 'Commissary C Commissary', 'New production plan :sdfsdf', '::1', 'DESKTOP-FO8SD5D', '2021-12-16 08:45:56', 'COMMISSARY', 0),
(1263, 'Commissary C Commissary', 'Updated production plan id:5', '::1', 'DESKTOP-FO8SD5D', '2021-12-16 08:46:05', 'COMMISSARY', 0),
(1264, 'Commissary C Commissary', 'Edit product id 28', '::1', 'DESKTOP-FO8SD5D', '2021-12-16 08:55:59', 'COMMISSARY', 0),
(1265, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-16 22:59:59', 'COMMISSARY', 0),
(1266, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-16 23:01:17', 'ACCOUNTING', 0),
(1267, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-19 22:59:09', 'ACCOUNTING', 0),
(1268, 'Administrator A Admin', 'Add bill to franchise: franchising fee', '::1', 'DESKTOP-FO8SD5D', '2021-12-19 23:07:55', 'ACCOUNTING', 0),
(1269, 'Administrator A Admin', 'Add bill to franchise: security deposits', '::1', 'DESKTOP-FO8SD5D', '2021-12-19 23:08:06', 'ACCOUNTING', 0),
(1270, 'Administrator A Admin', 'Add billing payment: 23 | franchising fee | 2700.00', '::1', 'DESKTOP-FO8SD5D', '2021-12-19 23:09:02', 'ACCOUNTING', 0),
(1271, 'Administrator A Admin', 'Edit bill to franchise 23', '::1', 'DESKTOP-FO8SD5D', '2021-12-19 23:09:46', 'ACCOUNTING', 0),
(1272, 'Administrator A Admin', 'Add bill to franchise: security deposits', '::1', 'DESKTOP-FO8SD5D', '2021-12-20 02:22:50', 'ACCOUNTING', 0),
(1273, 'Administrator A Admin', 'Add account: branch 2', '::1', 'DESKTOP-FO8SD5D', '2021-12-20 02:51:45', 'ACCOUNTING', 0),
(1274, 'Administrator A Admin', 'Edit franchisee id 16', '::1', 'DESKTOP-FO8SD5D', '2021-12-20 02:51:53', 'ACCOUNTING', 0),
(1275, 'Administrator A Admin', 'Edit franchisee id 16', '::1', 'DESKTOP-FO8SD5D', '2021-12-20 02:52:51', 'ACCOUNTING', 0),
(1276, 'Administrator A Admin', 'Add order payment: 16', '::1', 'DESKTOP-FO8SD5D', '2021-12-20 06:16:23', 'ACCOUNTING', 0),
(1277, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-20 07:32:24', 'ACCOUNTING', 0),
(1278, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-01-11 23:08:00', 'ACCOUNTING', 0),
(1279, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-01-12 23:05:07', 'ACCOUNTING', 0),
(1280, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-01-16 23:06:40', 'ACCOUNTING', 0),
(1281, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-01-17 22:51:27', 'ACCOUNTING', 0),
(1282, 'Administrator A Admin', 'Edit franchisee id 16', '::1', 'DESKTOP-FO8SD5D', '2022-01-17 23:01:53', 'ACCOUNTING', 0),
(1283, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-01-17 23:11:09', 'COMMISSARY', 0),
(1284, '  ', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-01-17 23:21:48', 'FRANCHISEE', 0),
(1285, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-01-17 23:21:57', 'FRANCHISEE', 0),
(1286, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-01-17 23:22:37', 'COMMISSARY', 0),
(1287, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-01-18 22:52:13', 'COMMISSARY', 0),
(1288, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-01-18 23:16:16', 'ACCOUNTING', 0),
(1289, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-01-19 22:55:36', 'COMMISSARY', 0),
(1290, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-01-19 23:05:48', 'ACCOUNTING', 0),
(1291, 'Administrator A Admin', 'Add purchase order: d-0003', '::1', 'DESKTOP-FO8SD5D', '2022-01-19 23:13:04', 'ACCOUNTING', 0),
(1292, 'Administrator A Admin', 'Edit purcahse order id 3', '::1', 'DESKTOP-FO8SD5D', '2022-01-19 23:13:42', 'ACCOUNTING', 0),
(1293, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-01-20 05:05:15', 'ACCOUNTING', 0),
(1294, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-01-20 08:51:50', 'COMMISSARY', 0),
(1295, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-01-20 22:57:14', 'ACCOUNTING', 0),
(1296, 'Administrator A Admin', 'Add purchase order: d-0004', '::1', 'DESKTOP-FO8SD5D', '2022-01-20 23:16:39', 'ACCOUNTING', 0),
(1297, 'Administrator A Admin', 'Edit purcahse order id 4', '::1', 'DESKTOP-FO8SD5D', '2022-01-20 23:16:58', 'ACCOUNTING', 0),
(1298, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-01-20 23:17:29', 'COMMISSARY', 0),
(1299, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-01-25 23:03:27', 'COMMISSARY', 0),
(1300, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-01-25 23:04:37', 'ACCOUNTING', 0),
(1301, 'Administrator A Admin', 'Add account: theodore', '::1', 'DESKTOP-FO8SD5D', '2022-01-25 23:04:50', 'ACCOUNTING', 0),
(1302, 'Administrator A Admin', 'Add account: denise', '::1', 'DESKTOP-FO8SD5D', '2022-01-25 23:04:54', 'ACCOUNTING', 0),
(1303, 'Administrator A Admin', 'Add account: kiara', '::1', 'DESKTOP-FO8SD5D', '2022-01-25 23:04:59', 'ACCOUNTING', 0),
(1304, 'Commissary C Commissary', 'New production plan :j-0006', '::1', 'DESKTOP-FO8SD5D', '2022-01-25 23:14:18', 'COMMISSARY', 0),
(1305, 'Commissary C Commissary', 'New production plan :j-0007', '::1', 'DESKTOP-FO8SD5D', '2022-01-25 23:18:01', 'COMMISSARY', 0),
(1306, 'Commissary C Commissary', 'New production plan :j-0008', '::1', 'DESKTOP-FO8SD5D', '2022-01-25 23:20:37', 'COMMISSARY', 0),
(1307, 'Commissary C Commissary', 'Updated production plan id:8', '::1', 'DESKTOP-FO8SD5D', '2022-01-25 23:32:04', 'COMMISSARY', 0),
(1308, 'Commissary C Commissary', 'Updated production plan id:8', '::1', 'DESKTOP-FO8SD5D', '2022-01-25 23:32:08', 'COMMISSARY', 0),
(1309, 'Commissary C Commissary', 'Updated production plan id:8', '::1', 'DESKTOP-FO8SD5D', '2022-01-25 23:32:33', 'COMMISSARY', 0),
(1310, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-01-26 22:57:08', 'COMMISSARY', 0),
(1311, 'Commissary C Commissary', 'Marked production plan id as finished:8', '::1', 'DESKTOP-FO8SD5D', '2022-01-26 22:57:49', 'COMMISSARY', 0),
(1312, 'Commissary C Commissary', 'Updated production plan id:7', '::1', 'DESKTOP-FO8SD5D', '2022-01-26 22:59:24', 'COMMISSARY', 0),
(1313, 'Commissary C Commissary', 'Updated production plan id:7', '::1', 'DESKTOP-FO8SD5D', '2022-01-26 23:25:26', 'COMMISSARY', 0),
(1314, 'Commissary C Commissary', 'Updated production plan id:7', '::1', 'DESKTOP-FO8SD5D', '2022-01-26 23:26:12', 'COMMISSARY', 0),
(1315, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-01-27 23:10:09', 'COMMISSARY', 0),
(1316, 'Commissary C Commissary', 'Addraw formulation: 25', '::1', 'DESKTOP-FO8SD5D', '2022-01-27 23:23:10', 'COMMISSARY', 0),
(1317, 'Commissary C Commissary', 'Addraw formulation: 22', '::1', 'DESKTOP-FO8SD5D', '2022-01-27 23:25:42', 'COMMISSARY', 0),
(1318, 'Commissary C Commissary', 'New production plan :j-0009', '::1', 'DESKTOP-FO8SD5D', '2022-01-27 23:26:29', 'COMMISSARY', 0),
(1319, 'Commissary C Commissary', 'Marked production plan id as finished:9', '::1', 'DESKTOP-FO8SD5D', '2022-01-27 23:26:44', 'COMMISSARY', 0),
(1320, 'Commissary C Commissary', 'New production plan :j-0010', '::1', 'DESKTOP-FO8SD5D', '2022-01-27 23:27:55', 'COMMISSARY', 0),
(1321, 'Commissary C Commissary', 'Marked production plan id as finished:10', '::1', 'DESKTOP-FO8SD5D', '2022-01-27 23:28:28', 'COMMISSARY', 0),
(1322, 'Commissary C Commissary', 'Addraw formulation: 25', '::1', 'DESKTOP-FO8SD5D', '2022-01-27 23:29:42', 'COMMISSARY', 0),
(1323, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-02-10 07:56:08', 'COMMISSARY', 0),
(1324, 'Franchise 1 F Franchise 1', 'Add order payment: 15', '::1', 'DESKTOP-FO8SD5D', '2022-02-10 08:14:55', 'FRANCHISEE', 0),
(1325, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-02-10 08:42:08', 'COMMISSARY', 0),
(1326, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-02-15 02:27:42', 'COMMISSARY', 0),
(1327, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-02-15 02:34:36', 'ACCOUNTING', 0),
(1328, 'Administrator A Admin', 'Add expense: operational', '::1', 'DESKTOP-FO8SD5D', '2022-02-15 02:58:11', 'ACCOUNTING', 0),
(1329, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-02-15 22:45:15', 'COMMISSARY', 0),
(1330, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-02-15 23:14:37', 'ACCOUNTING', 0),
(1331, 'Administrator A Admin', 'Add payroll: p-0001', '::1', 'DESKTOP-FO8SD5D', '2022-02-15 23:36:06', 'ACCOUNTING', 0),
(1332, 'Administrator A Admin', 'Edit payroll id 13', '::1', 'DESKTOP-FO8SD5D', '2022-02-15 23:36:18', 'ACCOUNTING', 0),
(1333, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-02-21 06:28:36', 'COMMISSARY', 0),
(1334, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-02-21 22:55:07', 'COMMISSARY', 0),
(1335, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-02-21 22:57:56', 'ACCOUNTING', 0),
(1336, 'Administrator A Admin', 'Add order payment: 15', '::1', 'DESKTOP-FO8SD5D', '2022-02-21 23:01:56', 'ACCOUNTING', 0),
(1337, 'Administrator A Admin', 'Edit order payment id 42', '::1', 'DESKTOP-FO8SD5D', '2022-02-21 23:02:20', 'ACCOUNTING', 0),
(1338, 'Administrator A Admin', 'Edit order payment id 42', '::1', 'DESKTOP-FO8SD5D', '2022-02-21 23:02:31', 'ACCOUNTING', 0),
(1339, 'Administrator A Admin', 'Add order payment: 16', '::1', 'DESKTOP-FO8SD5D', '2022-02-21 23:06:27', 'ACCOUNTING', 0),
(1340, 'Administrator A Admin', 'Edit order payment id 43', '::1', 'DESKTOP-FO8SD5D', '2022-02-21 23:23:53', 'ACCOUNTING', 0),
(1341, 'Administrator A Admin', 'Edit order payment id 43', '::1', 'DESKTOP-FO8SD5D', '2022-02-21 23:24:08', 'ACCOUNTING', 0);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `order_ref` text NOT NULL,
  `franchisee_id` int(11) DEFAULT NULL,
  `total_amount` decimal(10,2) NOT NULL,
  `rec_total_amount` decimal(10,2) NOT NULL DEFAULT 0.00,
  `added_by` text NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'PENDING',
  `date_ordered` date DEFAULT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `approved_by` varchar(50) DEFAULT NULL,
  `approved_date_time` timestamp NULL DEFAULT NULL,
  `remarks` text DEFAULT NULL,
  `date_delivered` date DEFAULT NULL,
  `discount_voucher` varchar(100) DEFAULT NULL,
  `discount_amount` decimal(10,2) DEFAULT 0.00,
  `delivery_method` varchar(50) NOT NULL,
  `delivery_charge` float NOT NULL DEFAULT 0,
  `due_date` date DEFAULT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_ref`, `franchisee_id`, `total_amount`, `rec_total_amount`, `added_by`, `status`, `date_ordered`, `date_time`, `approved_by`, `approved_date_time`, `remarks`, `date_delivered`, `discount_voucher`, `discount_amount`, `delivery_method`, `delivery_charge`, `due_date`, `is_deleted`) VALUES
(3, 'O-0001', 15, '290.00', '0.00', 'admin', 'APPROVED', '2021-12-20', '2021-12-19 23:10:13', NULL, NULL, NULL, NULL, '', '0.00', '', 50, '2021-12-21', 0),
(4, 'O-0002', 16, '400.00', '0.00', 'admin', 'APPROVED', '2021-12-20', '2021-12-20 05:28:26', NULL, NULL, NULL, NULL, '', '0.00', '', 0, '2021-12-21', 0),
(5, 'O-0003', 15, '16550.00', '0.00', 'admin', 'APPROVED', '2022-01-13', '2022-01-12 23:24:31', NULL, NULL, NULL, NULL, '', '0.00', '', 50, '2022-01-14', 0),
(6, 'O-0004', 15, '1550.00', '0.00', 'admin', 'RECEIVED', '2022-01-17', '2022-02-10 08:02:34', NULL, NULL, 'lalamove', '2022-02-10', '', '0.00', 'Delivery', 50, '2022-01-18', 0),
(7, 'O-0005', 15, '2250.00', '0.00', 'admin', 'RECEIVED', '2022-01-18', '2022-01-20 05:04:48', NULL, NULL, 'sa lalamove ito pinadala', '2022-01-20', '', '0.00', 'Delivery', 50, '2022-01-19', 0),
(8, 'O-0006', 15, '2200.00', '0.00', 'admin', 'RECEIVED', '2022-01-18', '2022-02-15 02:54:51', NULL, NULL, 'Lalamove', NULL, '', '0.00', 'Pick-up', 0, '2022-01-19', 0),
(9, 'O-0007', 16, '1500.00', '0.00', 'admin', 'RECEIVED', '2022-01-18', '2022-01-21 00:09:23', NULL, NULL, 'lalamove', '2022-01-21', '', '0.00', 'Delivery', 0, '2022-01-19', 0),
(10, 'O-0008', 15, '1550.00', '0.00', 'admin', 'RECEIVED', '2022-01-18', '2022-01-20 08:53:09', NULL, NULL, 'lalamove', '2022-01-20', '', '0.00', 'Delivery', 50, '2022-01-19', 0),
(11, 'O-0009', 16, '2400.00', '0.00', 'admin', 'RECEIVED', '2022-01-18', '2022-01-19 23:19:59', 'admin', '2022-01-18 23:44:56', 'Lalamove ito', '2022-01-20', '', '0.00', 'Delivery', 100, '2022-01-20', 0),
(12, 'O-0010', 16, '1500.00', '0.00', 'admin', 'CANCELLED', '2022-01-18', '2022-01-17 23:02:45', NULL, NULL, 'Cancelled', NULL, '', '0.00', 'Pick-up', 0, '2022-01-19', 0),
(13, 'O-0011', 16, '1500.00', '0.00', 'admin', 'RECEIVED', '2022-01-18', '2022-01-18 23:27:39', NULL, NULL, NULL, '2022-01-19', '', '0.00', 'Pick-up', 0, '2022-01-19', 0),
(14, 'O-0012', 15, '1725.00', '0.00', 'admin', 'RECEIVED', '2022-01-19', '2022-01-18 23:37:14', NULL, NULL, 'Some orders are ready for Delivery', '2022-01-19', '', '0.00', 'Delivery', 50, '2022-01-20', 0),
(15, 'O-0013', 16, '8000.00', '0.00', 'admin', 'RECEIVED', '2022-01-19', '2022-01-18 23:42:41', NULL, NULL, 'Some orders are ready for Received', '2022-01-19', '', '0.00', 'Pick-up', 0, '2022-01-20', 0),
(16, 'O-0014', 15, '320.00', '0.00', 'test', 'CANCELLED', '2022-02-10', '2022-02-10 08:21:21', 'admin', '2022-02-10 08:17:28', 'cancelled', NULL, 'null', '0.00', '', 50, '2022-02-11', 0),
(17, 'O-0015', 15, '270.00', '0.00', 'test', 'CANCELLED', '2022-02-10', '2022-02-10 08:22:30', 'admin', '2022-02-10 08:21:41', 'cancelled', NULL, 'null', '0.00', '', 50, '2022-02-11', 0),
(18, 'O-0016', 15, '270.00', '0.00', 'test', 'CANCELLED', '2022-02-10', '2022-02-10 08:27:22', 'admin', '2022-02-10 08:27:02', 'cancelled', NULL, 'null', '0.00', '', 50, '2022-02-11', 0),
(19, 'O-0017', 15, '270.00', '0.00', 'test', 'CANCELLED', '2022-02-10', '2022-02-10 08:31:59', 'admin', '2022-02-10 08:27:58', NULL, NULL, 'null', '0.00', '', 50, '2022-02-11', 0),
(20, 'O-0018', 15, '270.00', '0.00', 'test', 'CANCELLED', '2022-02-10', '2022-02-10 08:32:45', 'admin', '2022-02-10 08:32:29', 'cancelled', NULL, 'null', '0.00', '', 50, '2022-02-11', 0),
(21, 'O-0019', 15, '270.00', '0.00', 'test', 'CANCELLED', '2022-02-10', '2022-02-10 08:35:47', 'admin', '2022-02-10 08:34:34', 'cancelled', NULL, 'null', '0.00', '', 50, '2022-02-11', 0),
(22, 'O-0020', 15, '270.00', '0.00', 'test', 'CANCELLED', '2022-02-10', '2022-02-10 08:37:04', 'admin', '2022-02-10 08:36:54', 'cancelled', NULL, 'null', '0.00', '', 50, '2022-02-11', 0),
(23, 'O-0021', 15, '320.00', '0.00', 'test', 'CANCELLED', '2022-02-10', '2022-02-10 08:41:23', 'admin', '2022-02-10 08:41:00', 'cancelled', NULL, 'null', '0.00', 'Delivery', 50, '2022-02-11', 0),
(24, 'O-0022', 15, '250.00', '0.00', 'test', 'APPROVED', '2022-02-10', '2022-02-10 08:41:45', 'admin', '2022-02-10 08:41:45', NULL, NULL, 'null', '0.00', 'Delivery', 50, '2022-02-11', 0),
(25, 'O-0023', 16, '8835.00', '0.00', 'admin', 'APPROVED', '2022-02-16', '2022-02-15 23:23:45', NULL, NULL, NULL, NULL, '', '0.00', 'Delivery', 100, '2022-02-17', 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_ref` text NOT NULL,
  `item_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `qty` decimal(10,2) NOT NULL,
  `total_amount` decimal(10,2) NOT NULL,
  `rec_qty` decimal(10,2) NOT NULL DEFAULT 0.00,
  `rec_total_amount` decimal(10,2) NOT NULL DEFAULT 0.00,
  `status` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `order_id`, `order_ref`, `item_id`, `price`, `qty`, `total_amount`, `rec_qty`, `rec_total_amount`, `status`) VALUES
(6, 3, 'O-0001', 29, '20.00', '10.00', '200.00', '0.00', '0.00', ''),
(7, 3, 'O-0001', 28, '20.00', '1.00', '20.00', '0.00', '0.00', ''),
(8, 3, 'O-0001', 27, '20.00', '1.00', '20.00', '0.00', '0.00', ''),
(9, 4, 'O-0002', 29, '20.00', '10.00', '200.00', '0.00', '0.00', ''),
(10, 4, 'O-0002', 28, '20.00', '10.00', '200.00', '0.00', '0.00', ''),
(11, 5, 'O-0003', 23, '300.00', '15.00', '4500.00', '0.00', '0.00', ''),
(12, 5, 'O-0003', 22, '700.00', '10.00', '7000.00', '0.00', '0.00', ''),
(13, 5, 'O-0003', 24, '250.00', '20.00', '5000.00', '0.00', '0.00', ''),
(14, 6, 'O-0004', 23, '300.00', '0.00', '1500.00', '5.00', '0.00', 'Received'),
(15, 7, 'O-0005', 23, '300.00', '0.00', '1500.00', '5.00', '0.00', 'Received'),
(16, 7, 'O-0005', 22, '700.00', '0.00', '700.00', '1.00', '0.00', 'Received'),
(17, 8, 'O-0006', 23, '300.00', '5.00', '1500.00', '0.00', '0.00', ''),
(18, 8, 'O-0006', 22, '700.00', '0.00', '700.00', '1.00', '0.00', 'Received'),
(19, 9, 'O-0007', 23, '300.00', '0.00', '1500.00', '5.00', '0.00', 'Received'),
(20, 10, 'O-0008', 23, '300.00', '0.00', '1500.00', '5.00', '0.00', 'Received'),
(21, 11, 'O-0009', 23, '300.00', '0.00', '1500.00', '5.00', '0.00', 'Received'),
(22, 11, 'O-0009', 22, '700.00', '0.00', '700.00', '1.00', '0.00', 'Received'),
(23, 12, 'O-0010', 23, '300.00', '5.00', '1500.00', '0.00', '0.00', ''),
(24, 13, 'O-0011', 23, '300.00', '0.00', '1500.00', '0.00', '0.00', 'Received'),
(25, 14, 'O-0012', 21, '1500.00', '0.00', '1500.00', '1.00', '0.00', 'Received'),
(26, 14, 'O-0012', 25, '35.00', '0.00', '175.00', '4.00', '0.00', 'Received'),
(27, 15, 'O-0013', 21, '1500.00', '0.00', '4500.00', '3.00', '0.00', 'Received'),
(28, 15, 'O-0013', 22, '700.00', '0.00', '3500.00', '5.00', '0.00', 'Received'),
(29, 16, 'O-0014', 29, '20.00', '10.00', '200.00', '0.00', '0.00', NULL),
(30, 16, 'O-0014', 28, '20.00', '1.00', '20.00', '0.00', '0.00', NULL),
(31, 17, 'O-0015', 29, '20.00', '10.00', '200.00', '0.00', '0.00', NULL),
(32, 17, 'O-0015', 28, '20.00', '1.00', '20.00', '0.00', '0.00', NULL),
(33, 18, 'O-0016', 29, '20.00', '10.00', '200.00', '0.00', '0.00', NULL),
(34, 18, 'O-0016', 28, '20.00', '1.00', '20.00', '0.00', '0.00', NULL),
(35, 19, 'O-0017', 29, '20.00', '10.00', '200.00', '0.00', '0.00', NULL),
(36, 19, 'O-0017', 28, '20.00', '1.00', '20.00', '0.00', '0.00', NULL),
(37, 20, 'O-0018', 29, '20.00', '10.00', '200.00', '0.00', '0.00', NULL),
(38, 20, 'O-0018', 28, '20.00', '1.00', '20.00', '0.00', '0.00', NULL),
(39, 21, 'O-0019', 29, '20.00', '10.00', '200.00', '0.00', '0.00', NULL),
(40, 21, 'O-0019', 28, '20.00', '1.00', '20.00', '0.00', '0.00', NULL),
(41, 22, 'O-0020', 29, '20.00', '10.00', '200.00', '0.00', '0.00', NULL),
(42, 22, 'O-0020', 28, '20.00', '1.00', '20.00', '0.00', '0.00', NULL),
(43, 23, 'O-0021', 29, '20.00', '10.00', '200.00', '0.00', '0.00', NULL),
(44, 23, 'O-0021', 28, '20.00', '1.00', '20.00', '0.00', '0.00', NULL),
(45, 24, 'O-0022', 29, '20.00', '10.00', '200.00', '0.00', '0.00', NULL),
(46, 25, 'O-0023', 23, '300.00', '5.00', '1500.00', '0.00', '0.00', NULL),
(47, 25, 'O-0023', 22, '700.00', '1.00', '700.00', '0.00', '0.00', NULL),
(48, 25, 'O-0023', 24, '250.00', '20.00', '5000.00', '0.00', '0.00', NULL),
(49, 25, 'O-0023', 21, '1500.00', '1.00', '1500.00', '0.00', '0.00', NULL),
(50, 25, 'O-0023', 25, '35.00', '1.00', '35.00', '0.00', '0.00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_payments`
--

CREATE TABLE `order_payments` (
  `id` int(11) NOT NULL,
  `franchise_id` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL,
  `image_file` text DEFAULT NULL,
  `date_paid` date DEFAULT NULL,
  `check_no` text DEFAULT NULL,
  `check_details` varchar(50) DEFAULT NULL,
  `bank` varchar(50) DEFAULT NULL,
  `remarks` text DEFAULT NULL,
  `status` varchar(50) DEFAULT 'PENDING',
  `or_number` text DEFAULT NULL,
  `invoice_number` text DEFAULT NULL,
  `approved_by` varchar(100) DEFAULT NULL,
  `approved_date` datetime DEFAULT NULL,
  `is_deleted` int(11) DEFAULT 0,
  `date_time` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_payments`
--

INSERT INTO `order_payments` (`id`, `franchise_id`, `order_id`, `total_amount`, `image_file`, `date_paid`, `check_no`, `check_details`, `bank`, `remarks`, `status`, `or_number`, `invoice_number`, `approved_by`, `approved_date`, `is_deleted`, `date_time`) VALUES
(40, 16, NULL, '400.00', NULL, '2021-12-20', '', '', '', '', 'APPROVED', '111222', '111222', 'admin', '2021-12-20 14:16:23', 0, NULL),
(41, 15, NULL, '25815.00', 'uploads/dp.jpg', '2022-02-10', '', '', '', '', 'APPROVED', NULL, NULL, 'admin', '2022-02-10 16:15:22', 0, NULL),
(42, 15, NULL, '2720.00', NULL, '2022-02-22', '5568989888', 'Check name', 'BDO', '', 'APPROVED', '0666', '33444', 'admin', '2022-02-22 07:02:31', 0, NULL),
(43, 16, NULL, '20000.00', NULL, '2014-02-02', '54345345345345', '11-02-2020', 'BDO', 'Dolorem dolor cupida', 'APPROVED', '000999', '000099', 'admin', '2022-02-22 07:24:07', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_returns`
--

CREATE TABLE `order_returns` (
  `id` int(11) NOT NULL,
  `order_ref_no` varchar(50) NOT NULL,
  `return_ref_no` varchar(50) NOT NULL,
  `franchisee_id` int(11) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'PENDING',
  `added_by` varchar(50) NOT NULL,
  `remarks` text NOT NULL,
  `item_condition` varchar(20) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_returns`
--

INSERT INTO `order_returns` (`id`, `order_ref_no`, `return_ref_no`, `franchisee_id`, `status`, `added_by`, `remarks`, `item_condition`, `date_time`, `is_deleted`) VALUES
(2, 'O-0013', 'R-0001', 16, 'APPROVED', 'admin', 'sira kase', '', '2022-02-15 23:14:14', 0),
(3, 'O-0009', 'R-0002', 16, 'REJECTED', 'admin', 'Sira', '', '2022-02-15 23:14:16', 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_return_items`
--

CREATE TABLE `order_return_items` (
  `id` int(11) NOT NULL,
  `return_ref` varchar(50) NOT NULL,
  `item_id` int(11) NOT NULL,
  `order_qty` int(11) NOT NULL,
  `return_qty` int(11) NOT NULL,
  `cost` decimal(10,0) NOT NULL,
  `total_amount` decimal(10,0) NOT NULL,
  `status` varchar(50) NOT NULL,
  `reuse_item_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_return_items`
--

INSERT INTO `order_return_items` (`id`, `return_ref`, `item_id`, `order_qty`, `return_qty`, `cost`, `total_amount`, `status`, `reuse_item_id`) VALUES
(2, 'R-0001', 22, 0, 5, '700', '3500', '', 0),
(3, 'R-0002', 22, 0, 1, '700', '700', '', 21);

-- --------------------------------------------------------

--
-- Table structure for table `payroll`
--

CREATE TABLE `payroll` (
  `id` int(11) NOT NULL,
  `payroll_no` varchar(100) DEFAULT NULL,
  `cut_off_date` date DEFAULT NULL,
  `release_date` date DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL,
  `added_by` varchar(100) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `image_file` text DEFAULT NULL,
  `remarks` text DEFAULT NULL,
  `is_deleted` int(11) DEFAULT 0,
  `date_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `payroll`
--

INSERT INTO `payroll` (`id`, `payroll_no`, `cut_off_date`, `release_date`, `total_amount`, `added_by`, `status`, `image_file`, `remarks`, `is_deleted`, `date_time`) VALUES
(13, 'P-0001', '2022-02-01', '2022-02-16', '15800.00', 'admin', 'APPROVED', NULL, '', 0, '2022-02-15 23:36:18');

-- --------------------------------------------------------

--
-- Table structure for table `payroll_employee`
--

CREATE TABLE `payroll_employee` (
  `id` int(11) NOT NULL,
  `payroll_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `base_rate` decimal(10,2) NOT NULL,
  `allowance` decimal(10,2) NOT NULL,
  `comm_allowance` decimal(10,2) NOT NULL,
  `ot_reg_day` decimal(10,2) NOT NULL,
  `ot_spcl_day` decimal(10,2) NOT NULL,
  `ot_leg_day` decimal(10,2) NOT NULL,
  `special_holiday` decimal(10,2) NOT NULL,
  `regular_holiday` decimal(10,2) NOT NULL,
  `sss` decimal(10,2) NOT NULL,
  `pag_ibig` decimal(10,2) NOT NULL,
  `philhealth` decimal(10,2) NOT NULL,
  `lates` decimal(10,2) NOT NULL,
  `absent` decimal(10,2) NOT NULL,
  `tax_deduct` decimal(10,2) NOT NULL,
  `sss_loan` decimal(10,2) NOT NULL,
  `pag_ibig_loan` decimal(10,2) NOT NULL,
  `cash_advance` decimal(10,2) NOT NULL,
  `short_remittance` decimal(10,2) NOT NULL,
  `gross_pay` decimal(10,2) NOT NULL,
  `total_deductions` decimal(10,2) NOT NULL,
  `net_pay` decimal(10,2) NOT NULL,
  `total_min_work` decimal(10,2) NOT NULL,
  `reg_legal_ot_work` decimal(10,2) NOT NULL,
  `special_ot_work` decimal(10,2) NOT NULL,
  `special_holiday_work` decimal(10,2) NOT NULL,
  `legal_holiday_work` decimal(10,2) NOT NULL,
  `added_by` varchar(100) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `production`
--

CREATE TABLE `production` (
  `id` int(11) NOT NULL,
  `production_id` int(11) NOT NULL,
  `job_order` varchar(50) NOT NULL,
  `batch_no` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `is_started` int(11) NOT NULL DEFAULT 0,
  `manufacturing` date DEFAULT NULL,
  `expiration` date DEFAULT NULL,
  `added_by` text NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `production`
--

INSERT INTO `production` (`id`, `production_id`, `job_order`, `batch_no`, `description`, `start_date`, `end_date`, `is_started`, `manufacturing`, `expiration`, `added_by`, `date_time`, `is_deleted`) VALUES
(1, 1, 'J-0001', 'B-0001', 'item 3', '2021-12-10', '2021-12-10', 0, NULL, NULL, 'commissary', '2021-12-10 07:28:13', 0),
(2, 2, 'J-0002', 'B-0002', 'item 2', '2021-12-10', '2021-12-10', 2, NULL, NULL, 'commissary', '2021-12-10 07:30:25', 0),
(3, 3, 'J-0003', 'B-0003', 'sample', '2021-12-16', '2021-12-16', 1, NULL, NULL, 'commissary', '2021-12-16 08:37:49', 0),
(4, 4, 'J-0004', 'B-0004', '', '0000-00-00', '0000-00-00', 0, NULL, NULL, 'commissary', '2021-12-16 08:39:53', 1),
(5, 5, 'J-0005', 'B-0005', 'sdfsdf', '2021-12-16', '2021-12-16', 0, '2021-12-16', '2021-12-18', 'commissary', '2021-12-16 08:46:05', 0),
(6, 6, 'J-0006', 'B-0006', 'J-0006', '2021-12-05', '2021-12-05', 0, '2022-01-26', '2022-01-26', 'commissary', '2022-01-25 23:14:17', 0),
(7, 7, 'J-0007', 'B-0007', 'J-0007', '2022-01-27', '2022-01-27', 0, '2022-01-26', '2022-01-26', 'commissary', '2022-01-26 22:59:24', 0),
(8, 8, 'J-0008', 'B-0008', 'J-0008', '2021-12-05', '2021-12-05', 2, '2022-01-06', '2022-01-20', 'commissary', '2022-01-26 22:57:49', 0),
(9, 9, 'J-0009', 'B-0009', 'J-0009', '2021-12-05', '2022-01-27', 2, '2021-12-16', '2022-01-26', 'commissary', '2022-01-27 23:26:44', 0),
(10, 10, 'J-0010', 'B-0010', 'J-0010', '2021-12-05', '2022-01-27', 2, '2021-12-16', '2022-01-26', 'commissary', '2022-01-27 23:28:29', 0);

-- --------------------------------------------------------

--
-- Table structure for table `production_employees`
--

CREATE TABLE `production_employees` (
  `id` int(11) NOT NULL,
  `job_order` varchar(50) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `qty_made` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `production_employees`
--

INSERT INTO `production_employees` (`id`, `job_order`, `emp_id`, `qty_made`) VALUES
(6, 'J-0008', 16, 120),
(11, 'J-0008', 15, 200),
(12, 'J-0007', 16, 0),
(13, 'J-0008', 17, 300),
(14, 'J-0009', 15, 0),
(15, 'J-0010', 15, 5);

-- --------------------------------------------------------

--
-- Table structure for table `production_items`
--

CREATE TABLE `production_items` (
  `id` int(11) NOT NULL,
  `production_id` int(11) NOT NULL,
  `formulation_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `production_qty` float NOT NULL,
  `final_qty` float NOT NULL,
  `credit` float NOT NULL,
  `remarks` text NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `production_items`
--

INSERT INTO `production_items` (`id`, `production_id`, `formulation_id`, `item_id`, `production_qty`, `final_qty`, `credit`, `remarks`, `is_deleted`) VALUES
(1, 1, 8, 23, 10, 0, 0, '', 0),
(2, 2, 7, 22, 10, 0, 0, '', 0),
(3, 3, 6, 24, 1, 0, 0, '', 0),
(5, 5, 6, 24, 1, 0, 0, '', 0),
(6, 6, 6, 24, 100, 0, 0, '', 0),
(7, 6, 7, 22, 100, 0, 0, '', 0),
(12, 8, 5, 21, 1, 0, 0, '', 0),
(15, 7, 5, 21, 100, 0, 0, '', 0),
(16, 9, 8, 23, 1, 0, 0, '', 0),
(17, 10, 8, 23, 1, 0, 0, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `image_file` text DEFAULT NULL,
  `product_code` varchar(50) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `cost` decimal(10,2) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `category` varchar(50) DEFAULT NULL,
  `stocks` float DEFAULT 0,
  `order_count` int(11) NOT NULL DEFAULT 1,
  `uom` text NOT NULL,
  `is_deleted` int(11) DEFAULT 0,
  `date_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `supplier_id`, `image_file`, `product_code`, `description`, `cost`, `price`, `category`, `stocks`, `order_count`, `uom`, `is_deleted`, `date_time`) VALUES
(21, 4, 'uploads/alb1.jpg', 'I-0001', 'ITEM 1', '1000.00', '1500.00', 'FOOD', 18, 1, 'PC', 0, '2022-02-15 08:21:19'),
(22, 4, 'uploads/vd4.jpg', 'I-0002', 'ITEM 2', '500.00', '700.00', 'FOOD', 4.8, 1, 'PC', 0, '2022-02-15 08:21:21'),
(23, 4, 'uploads/vd9.jpg', 'I-0003', 'ITEM 3', '200.00', '300.00', 'FOOD', 227, 5, 'PC', 0, '2022-02-15 08:21:22'),
(24, 4, 'uploads/vd11.jpg', 'I-0004', 'ITEM 4', '200.00', '250.00', 'FOOD', 20, 20, 'PC', 0, '2022-02-15 08:21:23'),
(25, 4, NULL, 'I-0005', 'RAW MAT 1', '20.00', '35.00', 'NON FOOD', 50, 1, 'PC', 0, '2022-02-15 08:21:24'),
(26, 4, NULL, 'I-0006', 'RAW MAT 2', '20.00', '20.00', 'NON FOOD', 54, 1, 'PC', 0, '2022-02-15 08:21:26'),
(27, 4, NULL, 'I-0007', 'RAW MAT 3', '20.00', '20.00', 'NON FOOD', 68, 1, 'PC', 0, '2022-02-15 08:21:27'),
(28, 4, NULL, 'I-0008', 'RAW MAT 4', '20.00', '20.00', 'NON FOOD', 31, 1, 'PC', 0, '2022-02-15 08:21:28'),
(29, 4, NULL, 'I-0009', 'RAW MAT 5', '20.00', '20.00', 'NON FOOD', 195, 10, 'PC', 0, '2022-02-15 08:21:30');

-- --------------------------------------------------------

--
-- Table structure for table `products_itemize`
--

CREATE TABLE `products_itemize` (
  `id` int(11) NOT NULL,
  `item_count` int(11) NOT NULL COMMENT 'Quantity counter ',
  `item_id` int(11) NOT NULL,
  `process` varchar(50) NOT NULL,
  `ref_no` varchar(50) NOT NULL,
  `batch_no` varchar(50) NOT NULL,
  `is_available` int(11) NOT NULL DEFAULT 0,
  `manufacturing` date NOT NULL,
  `expiry` date NOT NULL,
  `order_ref` varchar(50) DEFAULT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products_itemize`
--

INSERT INTO `products_itemize` (`id`, `item_count`, `item_id`, `process`, `ref_no`, `batch_no`, `is_available`, `manufacturing`, `expiry`, `order_ref`, `date_time`) VALUES
(1, 1, 29, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0002', '2021-12-14 23:05:55'),
(2, 2, 29, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0002', '2021-12-14 23:05:55'),
(3, 3, 29, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0002', '2021-12-14 23:05:55'),
(4, 4, 29, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0002', '2021-12-14 23:05:55'),
(5, 5, 29, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0002', '2021-12-14 23:05:55'),
(6, 6, 29, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0002', '2021-12-14 23:05:55'),
(7, 7, 29, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0002', '2021-12-14 23:05:55'),
(8, 8, 29, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0002', '2021-12-14 23:05:55'),
(9, 9, 29, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0002', '2021-12-14 23:05:55'),
(10, 10, 29, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0002', '2021-12-14 23:05:55'),
(11, 1, 28, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0002', '2021-12-14 23:05:56'),
(12, 2, 28, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0001', '2021-12-15 23:58:38'),
(13, 3, 28, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'J-0008', '2022-01-26 22:57:49'),
(14, 4, 28, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'J-0008', '2022-01-26 22:57:49'),
(15, 5, 28, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'J-0008', '2022-01-26 22:57:49'),
(16, 6, 28, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'J-0008', '2022-01-26 22:57:49'),
(17, 7, 28, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'J-0008', '2022-01-26 22:57:49'),
(18, 8, 28, 'PURCHASE_ORDER', 'D-0001', 'D-001', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-10 07:19:46'),
(19, 9, 28, 'PURCHASE_ORDER', 'D-0001', 'D-001', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-10 07:19:46'),
(20, 10, 28, 'PURCHASE_ORDER', 'D-0001', 'D-001', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-10 07:19:46'),
(21, 1, 27, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0001', '2021-12-15 23:58:38'),
(22, 2, 27, 'PURCHASE_ORDER', 'D-0001', 'D-001', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-10 07:19:46'),
(23, 3, 27, 'PURCHASE_ORDER', 'D-0001', 'D-001', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-10 07:19:46'),
(24, 4, 27, 'PURCHASE_ORDER', 'D-0001', 'D-001', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-10 07:19:46'),
(25, 5, 27, 'PURCHASE_ORDER', 'D-0001', 'D-001', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-10 07:19:46'),
(26, 6, 27, 'PURCHASE_ORDER', 'D-0001', 'D-001', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-10 07:19:46'),
(27, 7, 27, 'PURCHASE_ORDER', 'D-0001', 'D-001', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-10 07:19:46'),
(28, 8, 27, 'PURCHASE_ORDER', 'D-0001', 'D-001', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-10 07:19:46'),
(29, 9, 27, 'PURCHASE_ORDER', 'D-0001', 'D-001', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-10 07:19:46'),
(30, 10, 27, 'PURCHASE_ORDER', 'D-0001', 'D-001', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-10 07:19:46'),
(31, 1, 26, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'J-0002', '2021-12-10 07:30:25'),
(32, 2, 26, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'J-0002', '2021-12-10 07:30:25'),
(33, 3, 26, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'J-0002', '2021-12-10 07:30:25'),
(34, 4, 26, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'J-0002', '2021-12-10 07:30:25'),
(35, 5, 26, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'J-0002', '2021-12-10 07:30:25'),
(36, 6, 26, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'J-0002', '2021-12-10 07:30:25'),
(37, 7, 26, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'J-0002', '2021-12-10 07:30:25'),
(38, 8, 26, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'J-0002', '2021-12-10 07:30:25'),
(39, 9, 26, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'J-0002', '2021-12-10 07:30:25'),
(40, 10, 26, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'J-0002', '2021-12-10 07:30:25'),
(41, 1, 25, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0012', '2022-01-18 23:19:31'),
(42, 2, 25, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0012', '2022-01-18 23:22:34'),
(43, 3, 25, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0012', '2022-01-18 23:22:34'),
(44, 4, 25, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0012', '2022-01-18 23:23:08'),
(45, 5, 25, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0012', '2022-01-18 23:23:08'),
(46, 6, 25, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'J-0008', '2022-01-26 22:57:49'),
(47, 7, 25, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'J-0008', '2022-01-26 22:57:49'),
(48, 8, 25, 'PURCHASE_ORDER', 'D-0001', 'D-001', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-10 07:19:46'),
(49, 9, 25, 'PURCHASE_ORDER', 'D-0001', 'D-001', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-10 07:19:46'),
(50, 10, 25, 'PURCHASE_ORDER', 'D-0001', 'D-001', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-10 07:19:46'),
(51, 1, 24, 'PURCHASE_ORDER', 'D-0001', 'D-001', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-10 07:19:46'),
(52, 2, 24, 'PURCHASE_ORDER', 'D-0001', 'D-001', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-10 07:19:46'),
(53, 3, 24, 'PURCHASE_ORDER', 'D-0001', 'D-001', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-10 07:19:46'),
(54, 4, 24, 'PURCHASE_ORDER', 'D-0001', 'D-001', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-10 07:19:46'),
(55, 5, 24, 'PURCHASE_ORDER', 'D-0001', 'D-001', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-10 07:19:46'),
(56, 6, 24, 'PURCHASE_ORDER', 'D-0001', 'D-001', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-10 07:19:46'),
(57, 7, 24, 'PURCHASE_ORDER', 'D-0001', 'D-001', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-10 07:19:46'),
(58, 8, 24, 'PURCHASE_ORDER', 'D-0001', 'D-001', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-10 07:19:46'),
(59, 9, 24, 'PURCHASE_ORDER', 'D-0001', 'D-001', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-10 07:19:46'),
(60, 10, 24, 'PURCHASE_ORDER', 'D-0001', 'D-001', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-10 07:19:46'),
(61, 1, 23, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0011', '2022-01-17 23:54:37'),
(62, 2, 23, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0011', '2022-01-17 23:54:37'),
(63, 3, 23, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0011', '2022-01-17 23:54:37'),
(64, 4, 23, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0011', '2022-01-17 23:54:37'),
(65, 5, 23, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0011', '2022-01-17 23:54:37'),
(66, 6, 23, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0011', '2022-01-18 23:05:56'),
(67, 7, 23, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0011', '2022-01-18 23:05:56'),
(68, 8, 23, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0011', '2022-01-18 23:06:11'),
(69, 9, 23, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0011', '2022-01-18 23:06:11'),
(70, 10, 23, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0011', '2022-01-18 23:06:11'),
(71, 1, 22, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', NULL, '2022-01-17 23:53:56'),
(72, 2, 22, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0009', '2022-01-17 23:54:15'),
(73, 3, 22, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0009', '2022-01-18 23:28:39'),
(74, 4, 22, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0013', '2022-01-18 23:38:24'),
(75, 5, 22, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0013', '2022-01-18 23:38:24'),
(76, 6, 22, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0013', '2022-01-18 23:38:47'),
(77, 7, 22, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0013', '2022-01-18 23:39:09'),
(78, 8, 22, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0013', '2022-01-18 23:39:09'),
(79, 9, 22, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0006', '2022-01-19 22:59:55'),
(80, 10, 22, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0005', '2022-01-19 23:05:01'),
(81, 1, 21, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0012', '2022-01-18 23:24:18'),
(82, 2, 21, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0013', '2022-01-18 23:39:14'),
(83, 3, 21, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0013', '2022-01-18 23:39:14'),
(84, 4, 21, 'PURCHASE_ORDER', 'D-0001', 'D-001', 0, '2021-12-01', '2021-12-01', 'O-0013', '2022-01-18 23:39:14'),
(85, 5, 21, 'PURCHASE_ORDER', 'D-0001', 'D-001', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-10 07:19:46'),
(86, 6, 21, 'PURCHASE_ORDER', 'D-0001', 'D-001', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-10 07:19:46'),
(87, 7, 21, 'PURCHASE_ORDER', 'D-0001', 'D-001', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-10 07:19:46'),
(88, 8, 21, 'PURCHASE_ORDER', 'D-0001', 'D-001', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-10 07:19:46'),
(89, 9, 21, 'PURCHASE_ORDER', 'D-0001', 'D-001', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-10 07:19:46'),
(90, 10, 21, 'PURCHASE_ORDER', 'D-0001', 'D-001', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-10 07:19:46'),
(91, 1, 22, 'PRODUCTION (+)', 'J-0002', 'B-0002', 0, '0000-00-00', '0000-00-00', 'R-0001', '2022-01-19 23:37:11'),
(92, 2, 22, 'PRODUCTION (+)', 'J-0002', 'B-0002', 0, '0000-00-00', '0000-00-00', 'R-0001', '2022-01-19 23:37:11'),
(93, 3, 22, 'PRODUCTION (+)', 'J-0002', 'B-0002', 0, '0000-00-00', '0000-00-00', 'R-0001', '2022-01-19 23:37:11'),
(94, 4, 22, 'PRODUCTION (+)', 'J-0002', 'B-0002', 0, '0000-00-00', '0000-00-00', 'R-0001', '2022-01-19 23:37:11'),
(95, 5, 22, 'PRODUCTION (+)', 'J-0002', 'B-0002', 0, '0000-00-00', '0000-00-00', 'R-0001', '2022-01-19 23:37:11'),
(96, 6, 22, 'PRODUCTION (+)', 'J-0002', 'B-0002', 1, '0000-00-00', '0000-00-00', NULL, '2021-12-10 07:30:24'),
(97, 7, 22, 'PRODUCTION (+)', 'J-0002', 'B-0002', 1, '0000-00-00', '0000-00-00', NULL, '2021-12-10 07:30:24'),
(98, 8, 22, 'PRODUCTION (+)', 'J-0002', 'B-0002', 1, '0000-00-00', '0000-00-00', NULL, '2021-12-10 07:30:24'),
(99, 9, 22, 'PRODUCTION (+)', 'J-0002', 'B-0002', 1, '0000-00-00', '0000-00-00', NULL, '2021-12-10 07:30:24'),
(100, 10, 22, 'PRODUCTION (+)', 'J-0002', 'B-0002', 1, '0000-00-00', '0000-00-00', NULL, '2021-12-10 07:30:24'),
(101, 1, 29, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 0, '2021-12-01', '2021-12-01', 'O-0001', '2021-12-15 23:58:38'),
(102, 2, 29, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 0, '2021-12-01', '2021-12-01', 'O-0001', '2021-12-15 23:58:38'),
(103, 3, 29, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 0, '2021-12-01', '2021-12-01', 'O-0001', '2021-12-15 23:58:38'),
(104, 4, 29, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 0, '2021-12-01', '2021-12-01', 'O-0001', '2021-12-15 23:58:38'),
(105, 5, 29, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 0, '2021-12-01', '2021-12-01', 'O-0001', '2021-12-15 23:58:38'),
(106, 6, 29, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 0, '2021-12-01', '2021-12-01', 'O-0001', '2021-12-15 23:58:38'),
(107, 7, 29, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 0, '2021-12-01', '2021-12-01', 'O-0001', '2021-12-15 23:58:38'),
(108, 8, 29, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 0, '2021-12-01', '2021-12-01', 'O-0001', '2021-12-15 23:58:38'),
(109, 9, 29, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 0, '2021-12-01', '2021-12-01', 'O-0001', '2021-12-15 23:58:38'),
(110, 10, 29, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 0, '2021-12-01', '2021-12-01', 'O-0001', '2021-12-15 23:58:38'),
(111, 11, 29, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 0, '2021-12-01', '2021-12-01', 'R-0001', '2021-12-15 23:59:34'),
(112, 12, 29, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 0, '2021-12-01', '2021-12-01', 'R-0001', '2021-12-15 23:59:34'),
(113, 13, 29, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 0, '2021-12-01', '2021-12-01', 'R-0001', '2021-12-15 23:59:34'),
(114, 14, 29, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 0, '2021-12-01', '2021-12-01', 'R-0001', '2021-12-15 23:59:34'),
(115, 15, 29, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 0, '2021-12-01', '2021-12-01', 'R-0001', '2021-12-15 23:59:34'),
(116, 16, 29, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 0, '2021-12-01', '2021-12-01', 'R-0001', '2021-12-15 23:59:40'),
(117, 17, 29, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 0, '2021-12-01', '2021-12-01', 'R-0001', '2021-12-15 23:59:40'),
(118, 18, 29, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 0, '2021-12-01', '2021-12-01', 'R-0001', '2021-12-15 23:59:40'),
(119, 19, 29, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 0, '2021-12-01', '2021-12-01', 'R-0001', '2021-12-15 23:59:40'),
(120, 20, 29, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 0, '2021-12-01', '2021-12-01', 'R-0001', '2021-12-15 23:59:40'),
(121, 1, 28, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(122, 2, 28, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(123, 3, 28, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(124, 4, 28, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(125, 5, 28, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(126, 6, 28, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(127, 7, 28, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(128, 8, 28, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(129, 9, 28, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(130, 10, 28, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(131, 11, 28, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(132, 12, 28, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(133, 13, 28, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(134, 14, 28, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(135, 15, 28, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(136, 16, 28, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(137, 17, 28, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(138, 18, 28, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(139, 19, 28, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(140, 20, 28, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(141, 1, 27, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(142, 2, 27, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(143, 3, 27, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(144, 4, 27, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(145, 5, 27, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(146, 6, 27, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(147, 7, 27, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(148, 8, 27, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(149, 9, 27, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(150, 10, 27, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(151, 11, 27, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(152, 12, 27, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(153, 13, 27, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(154, 14, 27, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(155, 15, 27, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(156, 16, 27, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(157, 17, 27, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(158, 18, 27, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(159, 19, 27, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(160, 20, 27, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(161, 1, 26, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 0, '2021-12-01', '2021-12-01', 'J-0008', '2022-01-26 22:57:49'),
(162, 2, 26, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 0, '2021-12-01', '2021-12-01', 'J-0008', '2022-01-26 22:57:49'),
(163, 3, 26, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(164, 4, 26, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(165, 5, 26, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(166, 6, 26, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(167, 7, 26, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(168, 8, 26, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(169, 9, 26, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(170, 10, 26, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(171, 11, 26, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(172, 12, 26, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(173, 13, 26, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(174, 14, 26, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(175, 15, 26, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(176, 16, 26, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(177, 17, 26, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(178, 18, 26, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(179, 19, 26, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(180, 20, 26, 'PURCHASE_ORDER', 'D-0002', 'D-0002', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-15 23:57:25'),
(181, 1, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 0, '2022-01-01', '2022-01-01', 'O-0009', '2022-01-19 23:19:35'),
(182, 2, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 0, '2022-01-01', '2022-01-01', 'O-0009', '2022-01-19 23:19:35'),
(183, 3, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 0, '2022-01-01', '2022-01-01', 'O-0009', '2022-01-19 23:19:35'),
(184, 4, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 0, '2022-01-01', '2022-01-01', 'O-0009', '2022-01-19 23:19:35'),
(185, 5, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 0, '2022-01-01', '2022-01-01', 'O-0009', '2022-01-19 23:19:35'),
(186, 6, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 0, '2022-01-01', '2022-01-01', 'O-0005', '2022-01-20 05:04:35'),
(187, 7, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 0, '2022-01-01', '2022-01-01', 'O-0005', '2022-01-20 05:04:35'),
(188, 8, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 0, '2022-01-01', '2022-01-01', 'O-0005', '2022-01-20 05:04:35'),
(189, 9, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 0, '2022-01-01', '2022-01-01', 'O-0005', '2022-01-20 05:04:35'),
(190, 10, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 0, '2022-01-01', '2022-01-01', 'O-0005', '2022-01-20 05:04:35'),
(191, 11, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 0, '2022-01-01', '2022-01-01', 'O-0008', '2022-01-20 08:52:28'),
(192, 12, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 0, '2022-01-01', '2022-01-01', 'O-0008', '2022-01-20 08:52:28'),
(193, 13, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 0, '2022-01-01', '2022-01-01', 'O-0008', '2022-01-20 08:52:28'),
(194, 14, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 0, '2022-01-01', '2022-01-01', 'O-0008', '2022-01-20 08:52:28'),
(195, 15, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 0, '2022-01-01', '2022-01-01', 'O-0008', '2022-01-20 08:52:28'),
(196, 16, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 0, '2022-01-01', '2022-01-01', 'O-0007', '2022-01-21 00:07:25'),
(197, 17, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 0, '2022-01-01', '2022-01-01', 'O-0007', '2022-01-21 00:07:25'),
(198, 18, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 0, '2022-01-01', '2022-01-01', 'O-0007', '2022-01-21 00:07:25'),
(199, 19, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 0, '2022-01-01', '2022-01-01', 'O-0007', '2022-01-21 00:07:25'),
(200, 20, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 0, '2022-01-01', '2022-01-01', 'O-0007', '2022-01-21 00:07:25'),
(201, 21, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 0, '2022-01-01', '2022-01-01', 'O-0004', '2022-02-10 07:56:43'),
(202, 22, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 0, '2022-01-01', '2022-01-01', 'O-0004', '2022-02-10 07:56:43'),
(203, 23, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 0, '2022-01-01', '2022-01-01', 'O-0004', '2022-02-10 07:56:43'),
(204, 24, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 0, '2022-01-01', '2022-01-01', 'O-0004', '2022-02-10 08:01:55'),
(205, 25, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 0, '2022-01-01', '2022-01-01', 'O-0004', '2022-02-10 08:01:55'),
(206, 26, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-19 23:18:49'),
(207, 27, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-19 23:18:49'),
(208, 28, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-19 23:18:49'),
(209, 29, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-19 23:18:49'),
(210, 30, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-19 23:18:49'),
(211, 31, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-19 23:18:49'),
(212, 32, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-19 23:18:49'),
(213, 33, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-19 23:18:49'),
(214, 34, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-19 23:18:49'),
(215, 35, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-19 23:18:49'),
(216, 36, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-19 23:18:49'),
(217, 37, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-19 23:18:49'),
(218, 38, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-19 23:18:49'),
(219, 39, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-19 23:18:49'),
(220, 40, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-19 23:18:49'),
(221, 41, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-19 23:18:49'),
(222, 42, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-19 23:18:49'),
(223, 43, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-19 23:18:49'),
(224, 44, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-19 23:18:49'),
(225, 45, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-19 23:18:49'),
(226, 46, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-19 23:18:49'),
(227, 47, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-19 23:18:49'),
(228, 48, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-19 23:18:49'),
(229, 49, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-19 23:18:49'),
(230, 50, 23, 'PURCHASE_ORDER', 'D-0003', 'D-0003', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-19 23:18:49'),
(331, 1, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(332, 2, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(333, 3, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(334, 4, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(335, 5, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(336, 6, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(337, 7, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(338, 8, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(339, 9, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(340, 10, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(341, 11, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(342, 12, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(343, 13, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(344, 14, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(345, 15, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(346, 16, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(347, 17, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(348, 18, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(349, 19, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(350, 20, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(351, 21, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(352, 22, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(353, 23, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(354, 24, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(355, 25, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(356, 26, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(357, 27, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(358, 28, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(359, 29, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(360, 30, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(361, 31, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(362, 32, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(363, 33, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(364, 34, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(365, 35, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(366, 36, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(367, 37, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(368, 38, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(369, 39, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(370, 40, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(371, 41, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(372, 42, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(373, 43, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(374, 44, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(375, 45, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(376, 46, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(377, 47, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(378, 48, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(379, 49, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(380, 50, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(381, 51, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(382, 52, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(383, 53, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(384, 54, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(385, 55, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(386, 56, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(387, 57, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(388, 58, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(389, 59, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(390, 60, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(391, 61, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(392, 62, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(393, 63, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(394, 64, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(395, 65, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(396, 66, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(397, 67, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(398, 68, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(399, 69, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(400, 70, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(401, 71, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(402, 72, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(403, 73, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(404, 74, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(405, 75, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(406, 76, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(407, 77, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(408, 78, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(409, 79, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(410, 80, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(411, 81, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(412, 82, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(413, 83, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(414, 84, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(415, 85, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(416, 86, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(417, 87, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(418, 88, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(419, 89, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(420, 90, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(421, 91, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(422, 92, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(423, 93, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(424, 94, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(425, 95, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(426, 96, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(427, 97, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(428, 98, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(429, 99, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(430, 100, 23, 'PURCHASE_ORDER', 'D-0004', 'D-0004', 1, '2022-01-01', '2022-01-01', NULL, '2022-01-20 23:22:14'),
(431, 1, 21, 'PRODUCTION (+)', 'J-0008', 'B-0008', 1, '2022-01-06', '2022-01-20', NULL, '2022-01-26 22:57:49'),
(432, 1, 23, 'PRODUCTION (+)', 'J-0009', 'B-0009', 1, '2021-12-16', '2022-01-26', NULL, '2022-01-27 23:26:44'),
(433, 1, 23, 'PRODUCTION (+)', 'J-0010', 'B-0010', 1, '2021-12-16', '2022-01-26', NULL, '2022-01-27 23:28:28'),
(434, 0, 25, 'REUSED', 'D-0001', 'D-001', 1, '2021-12-01', '2021-12-01', NULL, '2022-02-15 02:35:43'),
(435, 0, 25, 'REUSED', 'D-0001', 'D-001', 1, '2021-12-01', '2021-12-01', NULL, '2022-02-15 02:40:45'),
(436, 0, 26, 'REUSED', 'D-0001', 'D-001', 1, '2021-12-01', '2021-12-01', NULL, '2022-02-15 02:40:54'),
(437, 0, 21, 'REUSED', 'D-0001', 'D-001', 1, '2021-12-01', '2021-12-01', NULL, '2022-02-15 02:41:16');

-- --------------------------------------------------------

--
-- Table structure for table `product_journal`
--

CREATE TABLE `product_journal` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `transaction` text DEFAULT NULL,
  `ref_no` varchar(50) DEFAULT NULL,
  `qty` float DEFAULT NULL,
  `reason` text DEFAULT NULL,
  `date_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_journal`
--

INSERT INTO `product_journal` (`id`, `product_id`, `transaction`, `ref_no`, `qty`, `reason`, `date_time`) VALUES
(99, 25, 'PURCHASE ORDER', 'D-0006', 200, NULL, '2021-12-05 02:08:11'),
(100, 26, 'PURCHASE ORDER', 'D-0006', 250, NULL, '2021-12-05 02:08:11'),
(101, 27, 'PURCHASE ORDER', 'D-0006', 300, NULL, '2021-12-05 02:08:11'),
(102, 28, 'PURCHASE ORDER', 'D-0006', 350, NULL, '2021-12-05 02:08:11'),
(103, 29, 'PURCHASE ORDER', 'D-0006', 350, NULL, '2021-12-05 02:08:11'),
(104, 21, 'PRODUCTION (+)', 'J-0010', 10, NULL, '2021-12-05 02:09:29'),
(105, 24, 'PRODUCTION (+)', 'J-0010', 10, NULL, '2021-12-05 02:09:29'),
(106, 22, 'PRODUCTION (+)', 'J-0010', 5, NULL, '2021-12-05 02:09:29'),
(107, 25, 'PRODUCTION (-)', 'J-0010', 30, NULL, '2021-12-05 02:09:29'),
(108, 26, 'PRODUCTION (-)', 'J-0010', 30, NULL, '2021-12-05 02:09:29'),
(109, 28, 'PRODUCTION (-)', 'J-0010', 75, NULL, '2021-12-05 02:09:29'),
(110, 27, 'PRODUCTION (-)', 'J-0010', 100, NULL, '2021-12-05 02:09:29'),
(111, 23, 'PRODUCTION (+)', 'J-0011', 15, NULL, '2021-12-05 02:12:07'),
(112, 25, 'PRODUCTION (-)', 'J-0011', 75, NULL, '2021-12-05 02:12:07'),
(113, 26, 'PRODUCTION (-)', 'J-0011', 75, NULL, '2021-12-05 02:12:07'),
(114, 27, 'PRODUCTION (-)', 'J-0011', 30, NULL, '2021-12-05 02:12:07'),
(115, 28, 'PRODUCTION (-)', 'J-0011', 120, NULL, '2021-12-05 02:12:07'),
(116, 29, 'PRODUCTION (-)', 'J-0011', 75, NULL, '2021-12-05 02:12:07'),
(117, 24, 'ORDER', 'O-0001', 10, NULL, '2021-12-05 02:12:48'),
(118, 23, 'ORDER', 'O-0001', 10, NULL, '2021-12-05 02:12:48'),
(119, 21, 'ORDER', 'O-0002', 5, NULL, '2021-12-05 02:22:29'),
(120, 23, 'ORDER', 'O-0003', 5, NULL, '2021-12-05 02:30:35'),
(121, 22, 'ORDER', 'O-0003', 5, NULL, '2021-12-05 02:30:35'),
(124, 21, 'ORDER', 'O-0004', 5, NULL, '2021-12-05 02:41:17'),
(125, 21, 'PRODUCTION (+)', 'J-0012', 10, NULL, '2021-12-05 06:50:00'),
(126, 24, 'PRODUCTION (+)', 'J-0012', 10, NULL, '2021-12-05 06:50:00'),
(127, 25, 'PRODUCTION (-)', 'J-0012', 20, NULL, '2021-12-05 06:50:00'),
(128, 26, 'PRODUCTION (-)', 'J-0012', 20, NULL, '2021-12-05 06:50:00'),
(129, 28, 'PRODUCTION (-)', 'J-0012', 50, NULL, '2021-12-05 06:50:00'),
(130, 27, 'PRODUCTION (-)', 'J-0012', 100, NULL, '2021-12-05 06:50:00'),
(131, 29, 'ORDER', 'O-0005', 10, NULL, '2021-12-05 07:13:31'),
(132, 28, 'ORDER', 'O-0005', 1, NULL, '2021-12-05 07:13:31'),
(133, 27, 'ORDER', 'O-0005', 1, NULL, '2021-12-05 07:13:31'),
(134, 23, 'PRODUCTION (+)', 'J-0013', 10, NULL, '2021-12-05 07:21:55'),
(135, 25, 'PRODUCTION (-)', 'J-0013', 50, NULL, '2021-12-05 07:21:55'),
(136, 26, 'PRODUCTION (-)', 'J-0013', 50, NULL, '2021-12-05 07:21:55'),
(137, 27, 'PRODUCTION (-)', 'J-0013', 20, NULL, '2021-12-05 07:21:55'),
(138, 28, 'PRODUCTION (-)', 'J-0013', 80, NULL, '2021-12-05 07:21:55'),
(139, 29, 'PRODUCTION (-)', 'J-0013', 50, NULL, '2021-12-05 07:21:55'),
(140, 29, 'ORDER', 'O-0001', 10, NULL, '2021-12-06 23:23:49'),
(141, 28, 'ORDER', 'O-0001', 5, NULL, '2021-12-06 23:23:49'),
(142, 29, 'ORDER', 'O-0002', 10, NULL, '2021-12-06 23:29:09'),
(143, 28, 'ORDER', 'O-0002', 10, NULL, '2021-12-06 23:29:09'),
(144, 27, 'ORDER', 'O-0002', 10, NULL, '2021-12-06 23:29:10'),
(145, 29, 'ORDER', 'O-0015', 10, NULL, '2021-12-10 06:40:40'),
(146, 28, 'ORDER', 'O-0015', 1, NULL, '2021-12-10 06:40:40'),
(147, 21, 'PURCHASE ORDER', 'D-0001', 10, NULL, '2021-12-10 07:19:45'),
(148, 22, 'PURCHASE ORDER', 'D-0001', 10, NULL, '2021-12-10 07:19:45'),
(149, 23, 'PURCHASE ORDER', 'D-0001', 10, NULL, '2021-12-10 07:19:45'),
(150, 24, 'PURCHASE ORDER', 'D-0001', 10, NULL, '2021-12-10 07:19:45'),
(151, 25, 'PURCHASE ORDER', 'D-0001', 10, NULL, '2021-12-10 07:19:45'),
(152, 26, 'PURCHASE ORDER', 'D-0001', 10, NULL, '2021-12-10 07:19:45'),
(153, 27, 'PURCHASE ORDER', 'D-0001', 10, NULL, '2021-12-10 07:19:46'),
(154, 28, 'PURCHASE ORDER', 'D-0001', 10, NULL, '2021-12-10 07:19:46'),
(155, 29, 'PURCHASE ORDER', 'D-0001', 10, NULL, '2021-12-10 07:19:46'),
(156, 22, 'PRODUCTION (+)', 'J-0002', 10, NULL, '2021-12-10 07:30:24'),
(157, 26, 'PRODUCTION (-)', 'J-0002', 50, NULL, '2021-12-10 07:30:24'),
(158, 29, 'ORDER', 'O-0002', 10, NULL, '2021-12-14 23:05:56'),
(159, 28, 'ORDER', 'O-0002', 1, NULL, '2021-12-14 23:05:56'),
(160, 25, 'PURCHASE ORDER', 'D-0002', 20, NULL, '2021-12-15 23:57:24'),
(161, 26, 'PURCHASE ORDER', 'D-0002', 20, NULL, '2021-12-15 23:57:24'),
(162, 27, 'PURCHASE ORDER', 'D-0002', 20, NULL, '2021-12-15 23:57:24'),
(163, 28, 'PURCHASE ORDER', 'D-0002', 20, NULL, '2021-12-15 23:57:24'),
(164, 29, 'PURCHASE ORDER', 'D-0002', 20, NULL, '2021-12-15 23:57:24'),
(165, 29, 'ORDER', 'O-0001', 10, NULL, '2021-12-15 23:58:38'),
(166, 28, 'ORDER', 'O-0001', 1, NULL, '2021-12-15 23:58:38'),
(167, 27, 'ORDER', 'O-0001', 1, NULL, '2021-12-15 23:58:38'),
(168, 22, 'ORDER', 'O-0009', 1, NULL, '2022-01-17 23:53:56'),
(169, 22, 'ORDER', 'O-0009', 1, NULL, '2022-01-17 23:54:15'),
(170, 23, 'ORDER', 'O-0011', 5, NULL, '2022-01-17 23:54:37'),
(171, 23, 'ORDER', 'O-0011', 2, NULL, '2022-01-18 23:05:56'),
(172, 23, 'ORDER', 'O-0011', 3, NULL, '2022-01-18 23:06:11'),
(173, 23, 'ORDER', 'O-0011', 0, NULL, '2022-01-18 23:08:34'),
(174, 23, 'ORDER', 'O-0011', 0, NULL, '2022-01-18 23:09:54'),
(175, 23, 'ORDER', 'O-0011', 0, NULL, '2022-01-18 23:12:22'),
(176, 25, 'ORDER', 'O-0012', 1, NULL, '2022-01-18 23:19:31'),
(177, 25, 'ORDER', 'O-0012', 2, NULL, '2022-01-18 23:22:34'),
(178, 25, 'ORDER', 'O-0012', 2, NULL, '2022-01-18 23:23:08'),
(179, 25, 'ORDER', 'O-0012', 0, NULL, '2022-01-18 23:23:14'),
(180, 25, 'ORDER', 'O-0012', 0, NULL, '2022-01-18 23:23:29'),
(181, 21, 'ORDER', 'O-0012', 1, NULL, '2022-01-18 23:24:18'),
(182, 21, 'ORDER', 'O-0012', 0, NULL, '2022-01-18 23:24:22'),
(183, 21, 'ORDER', 'O-0012', 0, NULL, '2022-01-18 23:25:05'),
(184, 21, 'ORDER', 'O-0012', 0, NULL, '2022-01-18 23:27:00'),
(185, 22, 'ORDER', 'O-0009', 1, NULL, '2022-01-18 23:28:39'),
(186, 22, 'ORDER', 'O-0009', 0, NULL, '2022-01-18 23:28:45'),
(187, 21, 'ORDER', 'O-0012', 0, NULL, '2022-01-18 23:32:52'),
(188, 22, 'ORDER', 'O-0013', 2, NULL, '2022-01-18 23:38:24'),
(189, 22, 'ORDER', 'O-0013', 1, NULL, '2022-01-18 23:38:47'),
(190, 22, 'ORDER', 'O-0013', 2, NULL, '2022-01-18 23:39:09'),
(191, 22, 'ORDER', 'O-0013', 0, NULL, '2022-01-18 23:39:11'),
(192, 21, 'ORDER', 'O-0013', 3, NULL, '2022-01-18 23:39:15'),
(193, 21, 'ORDER', 'O-0013', 0, NULL, '2022-01-18 23:39:18'),
(194, 21, 'ORDER', 'O-0013', 0, NULL, '2022-01-18 23:40:47'),
(195, 21, 'ORDER', 'O-0013', 0, NULL, '2022-01-18 23:41:26'),
(196, 22, 'ORDER', 'O-0006', 1, NULL, '2022-01-19 22:59:55'),
(197, 22, 'ORDER', 'O-0006', 0, NULL, '2022-01-19 23:02:04'),
(198, 22, 'ORDER', 'O-0005', 1, NULL, '2022-01-19 23:05:01'),
(199, 22, 'ORDER', 'O-0005', 0, NULL, '2022-01-19 23:05:23'),
(200, 23, 'PURCHASE ORDER', 'D-0003', 50, NULL, '2022-01-19 23:18:49'),
(201, 23, 'ORDER', 'O-0009', 5, NULL, '2022-01-19 23:19:35'),
(202, 23, 'ORDER', 'O-0009', 0, NULL, '2022-01-19 23:19:49'),
(203, 22, 'RETURN GOODS', 'R-0001', 5, NULL, '2022-01-19 23:37:11'),
(204, 23, 'ORDER', 'O-0005', 5, NULL, '2022-01-20 05:04:35'),
(205, 23, 'ORDER', 'O-0005', 0, NULL, '2022-01-20 05:04:38'),
(206, 23, 'ORDER', 'O-0008', 5, NULL, '2022-01-20 08:52:28'),
(207, 23, 'ORDER', 'O-0008', 0, NULL, '2022-01-20 08:52:55'),
(208, 23, 'PURCHASE ORDER', 'D-0004', 100, NULL, '2022-01-20 23:18:08'),
(209, 23, 'PURCHASE ORDER', 'D-0004', 100, NULL, '2022-01-20 23:22:14'),
(210, 23, 'ORDER', 'O-0007', 5, NULL, '2022-01-21 00:07:25'),
(211, 23, 'ORDER', 'O-0007', 0, NULL, '2022-01-21 00:09:13'),
(212, 21, 'PRODUCTION (+)', 'J-0008', 1, NULL, '2022-01-26 22:57:48'),
(213, 25, 'PRODUCTION (-)', 'J-0008', 2, NULL, '2022-01-26 22:57:49'),
(214, 26, 'PRODUCTION (-)', 'J-0008', 2, NULL, '2022-01-26 22:57:49'),
(215, 28, 'PRODUCTION (-)', 'J-0008', 5, NULL, '2022-01-26 22:57:49'),
(216, 23, 'PRODUCTION (+)', 'J-0009', 1, NULL, '2022-01-27 23:26:43'),
(217, 22, 'PRODUCTION (-)', 'J-0009', 0, NULL, '2022-01-27 23:26:44'),
(218, 23, 'PRODUCTION (+)', 'J-0010', 1, NULL, '2022-01-27 23:28:28'),
(219, 22, 'PRODUCTION (-)', 'J-0010', 0.2, NULL, '2022-01-27 23:28:28'),
(220, 23, 'ORDER', 'O-0004', 3, NULL, '2022-02-10 07:56:43'),
(221, 23, 'ORDER', 'O-0004', 2, NULL, '2022-02-10 08:01:55'),
(222, 23, 'ORDER', 'O-0004', 0, NULL, '2022-02-10 08:02:15'),
(223, 25, 'REUSED GOODS', 'R-0002', 1, NULL, '2022-02-15 02:35:43'),
(224, 25, 'REUSED GOODS', 'R-0002', 1, NULL, '2022-02-15 02:40:45'),
(225, 26, 'REUSED GOODS', 'R-0002', 1, NULL, '2022-02-15 02:40:54'),
(226, 21, 'REUSED GOODS', 'R-0002', 1, NULL, '2022-02-15 02:41:16');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order`
--

CREATE TABLE `purchase_order` (
  `id` int(11) NOT NULL,
  `po_number` varchar(100) NOT NULL,
  `date_po` date DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL,
  `mode_of_payment` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `added_by` varchar(100) DEFAULT NULL,
  `approved_by` varchar(50) DEFAULT NULL,
  `received_by` varchar(50) DEFAULT NULL,
  `is_deleted` int(11) DEFAULT 0,
  `date_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `purchase_order`
--

INSERT INTO `purchase_order` (`id`, `po_number`, `date_po`, `supplier_id`, `total_amount`, `mode_of_payment`, `status`, `added_by`, `approved_by`, `received_by`, `is_deleted`, `date_time`) VALUES
(1, 'D-0001', '2021-12-01', 4, '20000.00', 'DELIVERY', 'RECEIVED', 'admin', 'admin', 'commissary', 0, '2021-12-10 07:19:46'),
(2, 'D-0002', '2021-12-01', 4, '2000.00', 'PICKUP', 'RECEIVED', 'admin', 'admin', 'commissary', 0, '2021-12-15 23:57:24'),
(3, 'D-0003', '2022-01-01', 4, '10000.00', 'PICKUP', 'RECEIVED', 'admin', 'admin', 'commissary', 0, '2022-01-19 23:18:49'),
(4, 'D-0004', '2022-01-01', 4, '20000.00', 'PICKUP', 'RECEIVED', 'admin', 'admin', 'commissary', 0, '2022-01-20 23:22:14');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order_items`
--

CREATE TABLE `purchase_order_items` (
  `id` int(11) NOT NULL,
  `purchase_order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `cost` decimal(10,2) NOT NULL,
  `total_amount` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `purchase_order_items`
--

INSERT INTO `purchase_order_items` (`id`, `purchase_order_id`, `product_id`, `qty`, `cost`, `total_amount`) VALUES
(1, 1, 21, 10, '1000.00', '10000.00'),
(2, 1, 22, 10, '500.00', '5000.00'),
(3, 1, 23, 10, '200.00', '2000.00'),
(4, 1, 24, 10, '200.00', '2000.00'),
(5, 1, 25, 10, '20.00', '200.00'),
(6, 1, 26, 10, '20.00', '200.00'),
(7, 1, 27, 10, '20.00', '200.00'),
(8, 1, 28, 10, '20.00', '200.00'),
(9, 1, 29, 10, '20.00', '200.00'),
(10, 2, 25, 20, '20.00', '400.00'),
(11, 2, 26, 20, '20.00', '400.00'),
(12, 2, 27, 20, '20.00', '400.00'),
(13, 2, 28, 20, '20.00', '400.00'),
(14, 2, 29, 20, '20.00', '400.00'),
(15, 3, 23, 50, '200.00', '10000.00'),
(16, 4, 23, 100, '200.00', '20000.00');

-- --------------------------------------------------------

--
-- Table structure for table `received_po`
--

CREATE TABLE `received_po` (
  `id` int(11) NOT NULL,
  `purchase_order_number` varchar(50) DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL,
  `received_by` varchar(50) DEFAULT NULL,
  `date_received` date DEFAULT NULL,
  `supplier_dr` varchar(100) DEFAULT NULL,
  `batch_no` varchar(100) NOT NULL,
  `remarks` text DEFAULT NULL,
  `added_by` varchar(50) DEFAULT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `received_po`
--

INSERT INTO `received_po` (`id`, `purchase_order_number`, `total_amount`, `received_by`, `date_received`, `supplier_dr`, `batch_no`, `remarks`, `added_by`, `date_time`) VALUES
(1, 'D-0001', '20000.00', 'Sample receiver', '2021-12-10', '2021000001', 'D-001', 'testest', 'admin', '2022-01-21 00:05:05'),
(2, 'D-0002', '2.00', 'Sample receiver', '2021-12-17', '2021000001', 'D-0002', '', 'commissary', '2021-12-15 23:57:24'),
(3, 'D-0003', '10000.00', 'Sample receiver', '2022-01-20', '2021000001', 'D-0003', 'Sakto', 'admin', '2022-01-21 00:04:46'),
(5, 'D-0004', '20000.00', 'Sample receiver', '2022-01-01', '2021000001', 'D-0004', '', 'admin', '2022-01-21 00:04:32');

-- --------------------------------------------------------

--
-- Table structure for table `received_po_items`
--

CREATE TABLE `received_po_items` (
  `id` int(11) NOT NULL,
  `purchase_order_number` varchar(50) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `rec_qty` int(11) DEFAULT NULL,
  `cost` decimal(10,2) DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `received_po_items`
--

INSERT INTO `received_po_items` (`id`, `purchase_order_number`, `product_id`, `rec_qty`, `cost`, `total_amount`) VALUES
(1, 'D-0001', 21, 10, '1000.00', '10000.00'),
(2, 'D-0001', 22, 10, '500.00', '5000.00'),
(3, 'D-0001', 23, 10, '200.00', '2000.00'),
(4, 'D-0001', 24, 10, '200.00', '2000.00'),
(5, 'D-0001', 25, 10, '20.00', '200.00'),
(6, 'D-0001', 26, 10, '20.00', '200.00'),
(7, 'D-0001', 27, 10, '20.00', '200.00'),
(8, 'D-0001', 28, 10, '20.00', '200.00'),
(9, 'D-0001', 29, 10, '20.00', '200.00'),
(10, 'D-0002', 25, 20, '20.00', '400.00'),
(11, 'D-0002', 26, 20, '20.00', '400.00'),
(12, 'D-0002', 27, 20, '20.00', '400.00'),
(13, 'D-0002', 28, 20, '20.00', '400.00'),
(14, 'D-0002', 29, 20, '20.00', '400.00'),
(15, 'D-0003', 23, 50, '200.00', '10000.00'),
(17, 'D-0004', 23, 100, '200.00', '20000.00');

-- --------------------------------------------------------

--
-- Table structure for table `sub_category`
--

CREATE TABLE `sub_category` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `sub_category_name` varchar(50) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sub_category`
--

INSERT INTO `sub_category` (`id`, `category_id`, `sub_category_name`, `is_deleted`, `date_time`) VALUES
(1, 6, 'Cost of Goods', 0, '2021-10-24 00:05:25'),
(2, 8, 'Gas Allowance', 0, '2021-10-24 00:05:43'),
(3, 8, 'Food Allowance', 0, '2021-10-24 00:05:58'),
(4, 8, 'Toll Fee', 0, '2021-10-24 00:06:07'),
(5, 8, 'Board and Lodging', 0, '2021-10-24 00:06:25'),
(6, 9, 'Repair and Maintenance Works', 0, '2021-10-24 00:06:50'),
(7, 9, 'Telephone and Internet Expenses', 0, '2021-10-24 00:07:05'),
(8, 9, 'Electricity and Water Bill', 0, '2021-10-24 00:07:22'),
(9, 9, 'Office Supplies', 0, '2021-10-24 00:07:34'),
(10, 9, 'Professional Fee', 0, '2021-10-24 00:07:46'),
(11, 9, 'Permit and Licenses', 0, '2021-10-24 00:07:56');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `supplier_name` varchar(100) NOT NULL,
  `supplier_address` varchar(100) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `supplier_name`, `supplier_address`, `is_deleted`, `date_time`) VALUES
(4, 'Supplier 1', 'Supplier 1', 0, '2021-11-11 23:20:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_levels`
--
ALTER TABLE `access_levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billing_franchise`
--
ALTER TABLE `billing_franchise`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billing_payments`
--
ALTER TABLE `billing_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `commissary_accounts`
--
ALTER TABLE `commissary_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `formulation`
--
ALTER TABLE `formulation`
  ADD PRIMARY KEY (`form_id`);

--
-- Indexes for table `formulation_details`
--
ALTER TABLE `formulation_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `franchisee`
--
ALTER TABLE `franchisee`
  ADD PRIMARY KEY (`id`),
  ADD KEY `franchisee_ibfk_1` (`branch_id`);

--
-- Indexes for table `franchise_accounts`
--
ALTER TABLE `franchise_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `franchise_branch`
--
ALTER TABLE `franchise_branch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `franchise_renewal_log`
--
ALTER TABLE `franchise_renewal_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_payments`
--
ALTER TABLE `order_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_returns`
--
ALTER TABLE `order_returns`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_return_items`
--
ALTER TABLE `order_return_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payroll`
--
ALTER TABLE `payroll`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payroll_employee`
--
ALTER TABLE `payroll_employee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `production`
--
ALTER TABLE `production`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `production_employees`
--
ALTER TABLE `production_employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `production_items`
--
ALTER TABLE `production_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_code` (`product_code`);

--
-- Indexes for table `products_itemize`
--
ALTER TABLE `products_itemize`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_journal`
--
ALTER TABLE `product_journal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_order`
--
ALTER TABLE `purchase_order`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `po_number` (`po_number`);

--
-- Indexes for table `purchase_order_items`
--
ALTER TABLE `purchase_order_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `received_po`
--
ALTER TABLE `received_po`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `received_po_items`
--
ALTER TABLE `received_po_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_category`
--
ALTER TABLE `sub_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_category` (`category_id`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access_levels`
--
ALTER TABLE `access_levels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `attendance`
--
ALTER TABLE `attendance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `billing_franchise`
--
ALTER TABLE `billing_franchise`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `billing_payments`
--
ALTER TABLE `billing_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `commissary_accounts`
--
ALTER TABLE `commissary_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `expenses`
--
ALTER TABLE `expenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `formulation`
--
ALTER TABLE `formulation`
  MODIFY `form_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `formulation_details`
--
ALTER TABLE `formulation_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `franchisee`
--
ALTER TABLE `franchisee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `franchise_accounts`
--
ALTER TABLE `franchise_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `franchise_branch`
--
ALTER TABLE `franchise_branch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `franchise_renewal_log`
--
ALTER TABLE `franchise_renewal_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1342;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `order_payments`
--
ALTER TABLE `order_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `order_returns`
--
ALTER TABLE `order_returns`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `order_return_items`
--
ALTER TABLE `order_return_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `payroll`
--
ALTER TABLE `payroll`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `payroll_employee`
--
ALTER TABLE `payroll_employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `production`
--
ALTER TABLE `production`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `production_employees`
--
ALTER TABLE `production_employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `production_items`
--
ALTER TABLE `production_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `products_itemize`
--
ALTER TABLE `products_itemize`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=438;

--
-- AUTO_INCREMENT for table `product_journal`
--
ALTER TABLE `product_journal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=227;

--
-- AUTO_INCREMENT for table `purchase_order`
--
ALTER TABLE `purchase_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `purchase_order_items`
--
ALTER TABLE `purchase_order_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `received_po`
--
ALTER TABLE `received_po`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `received_po_items`
--
ALTER TABLE `received_po_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `sub_category`
--
ALTER TABLE `sub_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `franchisee`
--
ALTER TABLE `franchisee`
  ADD CONSTRAINT `franchisee_ibfk_1` FOREIGN KEY (`branch_id`) REFERENCES `franchise_branch` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
