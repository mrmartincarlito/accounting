<?php
    $links = array( 
        array( 
            "name" => "Dashboard", 
            "data-icon" => "a", 
            "link" => "./?dashboard", 
            "linea-type" => "linea-basic",
            "child_items" => array(
            ) 
        ),
        array( 
            "name" => "User Accounts", 
            "data-icon" => "&#xe01b;", 
            "link" => "javascript:void(0)", 
            "linea-type" => "linea-basic",
            "child_items" => array(
                array( 
                    "name" => "Access Levels", 
                    "data-icon" => "9", 
                    "link" => "./?access_levels", 
                    "linea-type" => "linea-basic",
                    "child_items" => array(
                    ) 
                ),
                array( 
                    "name" => "User Accounts", 
                    "data-icon" => "3", 
                    "link" => "./?user_accounts", 
                    "linea-type" => "linea-basic",
                ),
                array( 
                    "name" => "Franchise Accounts", 
                    "data-icon" => ">", 
                    "link" => "./?franchise_accounts", 
                    "linea-type" => "linea-basic",
                ),
                array( 
                    "name" => "Commissary Accounts", 
                    "data-icon" => "&#xe005;", 
                    "link" => "./?commissary_accounts", 
                    "linea-type" => "linea-basic",
                ),
            ) 
        ),
        array( 
            "name" => "HR", 
            "data-icon" => "s", 
            "link" => "javascript:void(0)", 
            "linea-type" => "linea-elaborate",
            "child_items" => array(
                array( 
                    "name" => "Employee Entries", 
                    "data-icon" => "&#xe026;", 
                    "link" => "./?employee_entries", 
                    "linea-type" => "linea-basic",
                ),
                array( 
                    "name" => "Payroll Expenses", 
                    "data-icon" => "q", 
                    "link" => "./?payroll", 
                    "linea-type" => "linea-basic",
                ),
            ) 
        ),
        array( 
            "name" => "Product Information", 
            "data-icon" => "m", 
            "link" => "./?product_information", 
            "linea-type" => "linea-ecommerce",
            "child_items" => array(
            ) 
        ),
        array( 
            "name" => "PO Payments", 
            "data-icon" => "H", 
            "link" => "./?purchase_order_payments", 
            "linea-type" => "linea-ecommerce",
            "child_items" => array(
            ) 
        ),
        array( 
            "name" => "Franchise Transaction", 
            "data-icon" => "&#xe01b;", 
            "link" => "javascript:void(0)", 
            "linea-type" => "linea-basic",
            "child_items" => array(
                array( 
                    "name" => "Place Order", 
                    "data-icon" => ">", 
                    "link" => "./?order", 
                    "linea-type" => "linea-basic",
                ),
                array( 
                    "name" => "Order Tracker", 
                    "data-icon" => "#", 
                    "link" => "./?order_tracker", 
                    "linea-type" => "linea-basic",
                ),
                array( 
                    "name" => "Initiate Returns", 
                    "data-icon" => "&#xe003;", 
                    "link" => "./?return_goods", 
                    "linea-type" => "linea-basic",
                ),
                array( 
                    "name" => "Return Goods Approval", 
                    "data-icon" => "&#xe003;", 
                    "link" => "./?return_goods_approval", 
                    "linea-type" => "linea-basic",
                )
            ) 
        ),
        array( 
            "name" => "Franchisee", 
            "data-icon" => "Z", 
            "link" => "javascript:void(0)", 
            "linea-type" => "linea-basic",
            "child_items" => array(
                array( 
                    "name" => "Branch", 
                    "data-icon" => "f", 
                    "link" => "./?franchise_branch", 
                    "linea-type" => "linea-ecommerce",
                ),
                array( 
                    "name" => "Franchisee Entries", 
                    "data-icon" => "&#xe003;", 
                    "link" => "./?franchise_entries", 
                    "linea-type" => "linea-basic",
                ),
                array( 
                    "name" => "Franchise Billing", 
                    "data-icon" => "&#xe019;", 
                    "link" => "./?billing_franchising", 
                    "linea-type" => "linea-basic",
                ),
                array( 
                    "name" => "Billing Payments", 
                    "data-icon" => "&#xe019;", 
                    "link" => "./?billing_payments", 
                    "linea-type" => "linea-basic",
                ),
                array( 
                    "name" => "Order Payments", 
                    "data-icon" => "p", 
                    "link" => "./?order_payments", 
                    "linea-type" => "linea-ecommerce",
                ),
            ) 
        ),
        array( 
            "name" => "Expenses", 
            "data-icon" => "l", 
            "link" => "./?expenses", 
            "linea-type" => "linea-ecommerce",
            "child_items" => array(
            ) 
        ),
        array( 
            "name" => "Others", 
            "data-icon" => "N", 
            "link" => "javascript:void(0)", 
            "linea-type" => "linea-basic",
            "child_items" => array(
                array( 
                    "name" => "Cart", 
                    "data-icon" => "H", 
                    "link" => "./?cart",
                    "linea-type" => "linea-ecommerce",
                ),
                array( 
                    "name" => "Category", 
                    "data-icon" => "f", 
                    "link" => "./?category", 
                    "linea-type" => "linea-basic",
                ),
                array( 
                    "name" => "Sub-Category", 
                    "data-icon" => "e", 
                    "link" => "./?sub_category", 
                    "linea-type" => "linea-basic",
                ),
                array( 
                    "name" => "Supplier", 
                    "data-icon" => "l", 
                    "link" => "./?supplier", 
                    "linea-type" => "linea-basic",
                ),
            ) 
        ),

        array( 
            "name" => "Reports", 
            "data-icon" => "m", 
            "link" => "./?reports", 
            "linea-type" => "linea-basic",
            "child_items" => array(
            ) 
        ),
        array( 
            "name" => "Logs", 
            "data-icon" => "/", 
            "link" => "./?logs", 
            "linea-type" => "linea-basic",
            "child_items" => array(
            ) 
        ),
    ); 

    //do not meddle with the code below

    $links_HTML = "";
    foreach ($links as $link) {
        $isactive = '';
        if($page_name == strtolower($link['name'])){
            $isactive = 'active';
        }

        $child_links = '';
        $arrow_extra = '';
        if(count($link['child_items']) > 0){
            $child_links = '<ul class="nav nav-second-level">';
            $arrow_extra = '<span class="fa arrow"></span>';
            foreach ($link['child_items'] as $child_item) {
                $child_links .= '
                <li>
                    <a href="'.$child_item['link'].'">
                        <i data-icon="'.$child_item['data-icon'].'" class="linea-icon '.$child_item["linea-type"].' fa-fw"></i><span class="hide-menu">'.$child_item['name'].'</span>
                    </a>
                </li>
                ';
            }
            $child_links .= '</ul>';
        }

        $links_HTML .= '
            <li>
                <a href="'.$link['link'].'" class="waves-effect '.$isactive.'">
                    <i data-icon="'.$link['data-icon'].'" class="linea-icon '.$link["linea-type"].' fa-fw"></i>
                    <span class="hide-menu">'.$link['name'].'</span>
                    '.$arrow_extra.'
                </a> 
                    '.$child_links.'
            </li>
        ';
    }
?>
