const FRANCHISE_BRANCH_API = './api/franchise_branch.php';

$(document).ready(function() {
	var myTable = $('#datatable').DataTable({
		processing: true,
		ajax: {
			url: FRANCHISE_BRANCH_API + '?get',
			dataSrc: function(json) {
				var return_data = new Array()
				for (var i = 0; i < json.length; i++) {
					return_data.push({
						id: json[i].id,
						branch_code: json[i].branch_code,
						branch_name: json[i].branch_name,
						branch_location: json[i].branch_location						
					})
				}
				return return_data
			},

			complete: function() {
				$('#datatable tbody').on('dblclick', 'tr', function() {
					$('form').trigger('reset')
					$('input[type=checkbox]').prop('checked', false)
					var data = $('#datatable')
						.DataTable()
						.row(this)
						.data().id
					$('input[name*="modifyId"]').val(data)
					$('input[name*="formAction"]').val('edit')
					$.blockUI()

					$.ajax({
						url: FRANCHISE_BRANCH_API + '?getDetails=' + data, //branch code
						processData: false
					})
						.done(data => {
							var json = $.parseJSON(data)

							$.unblockUI()
							populateForm($('form'), json)
							$('#formModal').modal('show')
						})
						.fail(errorThrown => {
							$.unblockUI()
							console.log('Details Get Error', errorThrown)
							return false
						})
				})

				$('#addModal').on('click', function() {
					$('form').trigger('reset')
					$('input[name*="modifyId"]').val('')
					$('input[name*="formAction"]').val('add')
					$('#formModal').modal('show')
				})
			}
		},
		columns: [
			{ data: 'id' },
			{ data: 'branch_code' },
			{ data: 'branch_name' },
			{ data: 'branch_location' }
		],
		order: [[0, 'desc']]
	})

	$('form').on('submit', function(e) {
		e.preventDefault()

		$.blockUI({
			baseZ: 2000
		})

		var data = $('form').serializeArray()
		var params = postParams('', data)

		$.ajax({
			url: FRANCHISE_BRANCH_API,
			type: 'post',
			data: 'data=' + params,
			processData: false
		})
			.done(data => {
				$.unblockUI()
				console.log('Save Changes Response: ', data)

				responseJSON = $.parseJSON(data)

				new PNotify(responseJSON)

				if (responseJSON.type == 'success') {
					myTable.ajax.reload(null, false)
					$('#formModal').modal('hide')
				}
			})
			.fail(errorThrown => {
				$.unblockUI()
				console.log('Save Changes Post Error: ', errorThrown)
				return false
			})
	})

	//delete button
	$('#deleteButton').on('click', function() {
		if (!confirm('Are you sure you want to remove this branch?')) {
			return false
		}

		$.blockUI({
			baseZ: 2000
		})

		$('input[name*="formAction"]').val('delete')

		var data = $('form').serializeArray()
		var params = postParams('', data)

		$.ajax({
			url: FRANCHISE_BRANCH_API,
			type: 'post',
			data: 'data=' + params,
			processData: false
		})
			.done(data => {
				$.unblockUI()
				console.log('Delete Response: ', data)

				responseJSON = $.parseJSON(data)
				new PNotify(responseJSON)
				if (responseJSON.type == 'success') {
					myTable.ajax.reload(null, false)
					$('#formModal').modal('hide')
				}
			})
			.fail(errorThrown => {
				$.unblockUI()
				console.log('Delete Post Error: ', errorThrown)
				return false
			})

		return false
	})
})
