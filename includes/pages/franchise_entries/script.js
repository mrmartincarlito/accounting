const FRANCHISE_ENTRIES_API = './api/franchise_entries.php';
const BRANCH_API = './api/franchise_branch.php';
const CART_API = './api/cart.php';

$(document).ready(function() {

	// get branch code list
	$.ajax({
		url: BRANCH_API + '?get',
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)
			var newVal = '<option selected disabled>Select Branch Code</option>'
			
			newVal += json.map(value => {
				return `<option value="${value.id}">[${value.branch_code}] ${value.branch_name}</option>`
			})
			$(`#branch_id`).html(newVal)
		})
		.fail(errorThrown => {
			console.log('Get Error: ', errorThrown)
			return false
	})

	// get cart list (package_type) 
	$.ajax({
		url: CART_API + '?get',
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)
			var newVal = '<option selected disabled>Select Package Type</option>'
			
			newVal += json.map(value => {
				return `<option value="${value.id}">${value.description}</option>`
			})
			$(`#package_type`).html(newVal)
		})
		.fail(errorThrown => {
			console.log('Get Error: ', errorThrown)
			return false
	})

	var myTable = $('#datatable').DataTable({
		processing: true,
		ajax: {
			url: FRANCHISE_ENTRIES_API + '?get',
			dataSrc: function(json) {
				var return_data = new Array()
				for (var i = 0; i < json.length; i++) {
					
					return_data.push({
						id: json[i].id,
						branch_code: json[i].branch_code,
						mode_of_transaction: json[i].mode_of_transaction,
						name: json[i].name,
						address: json[i].address,
						contact: json[i].contactno,
						birthday:  moment(json[i].birthday).format('MMMM D, YYYY'),
						tin_number:  json[i].tin_number,
						package_type: json[i].description,
						contract_signing: moment(json[i].date_contract_signing).format('MMMM D, YYYY'),
						opening: moment(json[i].date_branch_opening).format('MMMM D, YYYY'),
						expiry: moment(json[i].date_contract_expiry).format('MMMM D, YYYY')
					})
				}
				return return_data
			},

			complete: function() {
				$('#datatable tbody').on('dblclick', 'tr', function() {
					$('form').trigger('reset')
					$('input[type=checkbox]').prop('checked', false)

					var data = $('#datatable')
						.DataTable()
						.row(this)
						.data().id
					$('input[name*="modifyId"]').val(data)
					$('input[name*="formAction"]').val('edit')

					$.blockUI()

					$.ajax({
						url: FRANCHISE_ENTRIES_API + '?getDetails=' + data, //employee id
						processData: false
					})
						.done(data => {
							var json = $.parseJSON(data)

							$.unblockUI()
							populateForm($('form'), json)
							$('#formModal').modal('show')
						})
						.fail(errorThrown => {
							$.unblockUI()
							console.log('Details Get Error', errorThrown)
							return false
						})
				})

				$('#addModal').on('click', function() {
					$('form').trigger('reset')
					$('input[name*="modifyId"]').val('')
					$('input[name*="formAction"]').val('add')
					$('#formModal').modal('show')
				})
			}
		},
		columns: [
			{ data: 'id' },
			{ data: 'branch_code' },
			{ data: 'mode_of_transaction' },
			{ data: 'name' },
			{ data: 'address' },
			{ data: 'contact' },
			{ data: 'birthday' },
			{ data: 'tin_number' },
			{ data: 'package_type' },
			{ data: 'contract_signing' },
			{ data: 'opening' },
			{ data: 'expiry' }
		],
		order: [[0, 'desc']],
		rowReorder: {
			selector: 'td:nth-child(4)'
		},
		responsive: true,
	})

	$('form').on('submit', function(e) {
		e.preventDefault()

		$.blockUI({
			baseZ: 2000
		})

		var data = $('form').serializeArray()
		var params = postParams('', data)

		$.ajax({
			url: FRANCHISE_ENTRIES_API,
			type: 'post',
			data: 'data=' + params,
			processData: false
		})
			.done(data => {
				$.unblockUI()
				console.log('Save Changes Response: ', data)

				responseJSON = $.parseJSON(data)

				new PNotify(responseJSON)

				if (responseJSON.type == 'success') {
					myTable.ajax.reload(null, false)
					$('#formModal').modal('hide')
				}
			})
			.fail(errorThrown => {
				$.unblockUI()
				console.log('Save Changes Post Error: ', errorThrown)
				return false
			})
	})

	//delete button
	$('#deleteButton').on('click', function() {
		if (!confirm('Are you sure you want to remove?')) {
			return false
		}

		$.blockUI({
			baseZ: 2000
		})

		$('input[name*="formAction"]').val('delete')

		var data = $('form').serializeArray()
		var params = postParams('', data)

		$.ajax({
			url: FRANCHISE_ENTRIES_API,
			type: 'post',
			data: 'data=' + params,
			processData: false
		})
			.done(data => {
				$.unblockUI()
				console.log('Delete Response: ', data)

				responseJSON = $.parseJSON(data)
				new PNotify(responseJSON)
				if (responseJSON.type == 'success') {
					myTable.ajax.reload(null, false)
					$('#formModal').modal('hide')
				}
			})
			.fail(errorThrown => {
				$.unblockUI()
				console.log('Delete Post Error: ', errorThrown)
				return false
			})

		return false
	})
})

function changeDelivery(value){
	// if(value == "Delivery"){
	// 	$("#delivery_charge").attr("disabled", false)
	// }else{
	// 	$("#delivery_charge").attr("disabled", true)
	// }
}

function addYear() {
	var myDate = new Date($("#date_branch_opening").val());
	myDate.setFullYear(myDate.getFullYear() + 3);

	var dateString = moment(myDate).format('YYYY-MM-DD');

	$("#date_contract_expiry").val(dateString)
}
