const API = './api/dashboard.php'

var myTable = $('#expirationDatatable').DataTable({
	processing: true,
	ajax: {
		url: API + '?getFranchiseExpiry',
		dataSrc: function (json) {
			var return_data = new Array()
			for (var i = 0; i < json.length; i++) {
				return_data.push({
					franchisee_owner: json[i].name,
					expiry: json[i].date_contract_expiry,
					renew : '<button type="button" class="btn btn-danger" onclick="renewModal('+json[i].id+')">RENEW</button>'
				})
			}
			return return_data
		}
	},
	columns: [
		{ data: 'franchisee_owner' },
		{ data: 'expiry' },
		{ data: 'renew' }
	],
	order: [[0, 'desc']],
	rowReorder: {
		selector: 'td:nth-child(2)'
	},
	responsive: true,
})


var balanceTable = $('#balancesDatatable').DataTable({
	processing: true,
	ajax: {
		url: API + '?getFranchiseBalances',
		dataSrc: function (json) {
			var return_data = new Array()
			for (var i = 0; i < json.length; i++) {
				return_data.push({
					franchisee_owner: json[i].name,
					billing_balance: parseInt(json[i].billing_balance) > -1 ? formatNumber(json[i].billing_balance) : "(" + formatNumber(Math.abs(json[i].billing_balance)) + ")",
					order_balance: parseInt(json[i].order_balance) > -1 ? formatNumber(json[i].order_balance) : "(" + formatNumber(Math.abs(json[i].order_balance)) + ")",
					total_amount: parseInt(json[i].total_amount) > -1 ? formatNumber(json[i].total_amount) : "(" + formatNumber(Math.abs(json[i].total_amount)) + ")",
					last_payment_amount: formatNumber(json[i].last_payment_amount),
					last_payment_date: json[i].last_payment_date,
				})
			}
			return return_data
		}
	},
	columns: [
		{ data: 'franchisee_owner' },
		{ data: 'billing_balance' },
		{ data: 'order_balance' },
		{ data: 'total_amount' },
		{ data: 'last_payment_amount' },
		{ data: 'last_payment_date' }
	],
	rowReorder: {
		selector: 'td:nth-child(2)'
	},
	responsive: true,
})

// cards numbers
setInterval(getCards, 10000)
getCards()
function getCards () {
	$.ajax({
		url: API + '?getCards',
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)
	
			$.each(json, function (i, item) {
				$("#" + i).html(item)
	
			});
		})
		.fail(errorThrown => {
			console.log('Get Error: ', errorThrown)
			return false
	})
}

function renewModal(id){
	$("#id").val(id)
	$('#formModal').modal('show')
}

function addYear() {
	var myDate = new Date();
	myDate.setFullYear(myDate.getFullYear() + 3);

	var dateString = moment(myDate).format('YYYY-MM-DD');

	$("#new_expiry_date").val(dateString)
}

$('form').on('submit', function(e) {
	e.preventDefault()
	$.blockUI({
		baseZ: 2000
	})
	var data = $('form').serializeArray()
	var params = postParams('', data)

	$.ajax({
		url: API,
		type: 'post',
		data: 'renew=' + params,
		processData: false
	})
		.done(data => {
			$.unblockUI()
			
			responseJSON = $.parseJSON(data)
			new PNotify(responseJSON)
			if (responseJSON.type == 'success') {
				myTable.ajax.reload(null, false)
				$('#formModal').modal('hide')
			}
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Delete Post Error: ', errorThrown)
			return false
		})
})