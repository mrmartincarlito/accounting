
  const API = './api/payroll.php'
  const EMP_API = './api/employee.php';
  const IMAGE = './api/upload_image.payroll.php'

  var myTable = null;
	$.fn.dataTable.ext.search.push(
		function (settings, data, dataIndex) {
			var startDate = Date.parse($('#start-date').val(), 10);
			var endDate = Date.parse($('#end-date').val(), 10);
			var columnDate = Date.parse(data[4]) || 0; // use data for the age column
			if ((isNaN(startDate) && isNaN(endDate)) ||
				(isNaN(startDate) && columnDate <= endDate) ||
				(startDate <= columnDate && isNaN(endDate)) ||
				(startDate <= columnDate && columnDate <= endDate)) {
				return true;
			}
			return false;
		}
	);

	$('.date-range-filter').change(function () {
		myTable.draw();
	});

	myTable = $('#datatable').DataTable({
		processing: true,
		serverSide: true,
		rowReorder: {
			selector: 'td:nth-child(3)'
		},
		responsive: true,
		order: [
			[0, 'desc']
		],
		buttons: [
			{
				extend: 'excel',
				text: 'Export to Excel'
			},
			{

				extend: 'pdf',
				text: 'Export to Pdf',
				orientation: 'portrait',
				filename: 'Product Information',
				paging: true,
				customize: function (doc) {
					doc.content.splice(0, 1);
					var now = new Date();
					var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();
					doc.pageMargins = [20, 60, 20, 30];
					doc.defaultStyle.fontSize = 8;
					doc.styles.tableHeader.fontSize = 8;
					doc['header'] = (function () {
						return {
							columns: [{
								alignment: 'left',
								text: 'Product Information',
								fontSize: 20,
								margin: [20, 20]
							}]
						}
					});
					doc['footer'] = (function () {
						return {
							columns: [{
								alignment: 'left',
								text: 'Created on: ' + jsDate.toString(),
								margin: [10, 10]
							}]
						}
					})
				}
			}
		],
		dom: 'lBfrtip',
		"language": {
			"lengthMenu": 'Display <select>' +
				'<option value="10">10</option>' +
				'<option value="50">50</option>' +
				'<option value="70">70</option>' +
				'<option value="80">80</option>' +
				'<option value="100">100</option>' +
				'<option value="-1">All</option>' +
				'</select> records'
		},
		ajax: {
			url: API + '?get&status=PENDING',
			complete: function () {

				$('#addModal').on('click', function () {
					$('form').trigger('reset')
					$('input[type=checkbox]').prop('checked', false)
					$(`input[name*="modifyId"]`).val('')
					$(`input[name*="formAction"]`).val('add')
					$('#formModal').modal('show')
				})
			}
		}
	})

  $('form').on('submit', function(e) {
	  e.preventDefault()

	  $.blockUI({
		  baseZ: 2000
	  })

	  var data = $('form').serializeArray()
	  var params = postParams('', data)

	  $.ajax({
		  url: API,
		  type: 'post',
		  data: 'data=' + params,
		  processData: false
	  })
		  .done(data => {
			  $.unblockUI()
			  console.log('Save Changes Response: ', data)

			  responseJSON = $.parseJSON(data)

			  new PNotify(responseJSON)

			  if (responseJSON.type == 'success') {
				  myTable.ajax.reload(null, false)
				  $('#formModal').modal('hide')
				  saveImage()
			  }
		  })
		  .fail(errorThrown => {
			  $.unblockUI()
			  console.log('Save Changes Post Error: ', errorThrown)
			  return false
		  })
  })

  //delete button
  $('#deleteButton').on('click', function() {
	  if (!confirm('Are you sure you want to remove this user?')) {
		  return false
	  }

	  $.blockUI({
		  baseZ: 2000
	  })

	  $(`input[name*="formAction"]`).val('delete')

	  var data = $('form').serializeArray()
	  var params = postParams('', data)

	  $.ajax({
		  url: API,
		  type: 'post',
		  data: 'data=' + params,
		  processData: false
	  })
		  .done(data => {
			  $.unblockUI()
			  console.log('Delete Response: ', data)

			  responseJSON = $.parseJSON(data)
			  new PNotify(responseJSON)
			  if (responseJSON.type == 'success') {
				  myTable.ajax.reload(null, false)
				  $('#formModal').modal('hide')
			  }
		  })
		  .fail(errorThrown => {
			  $.unblockUI()
			  console.log('Delete Post Error: ', errorThrown)
			  return false
		  })

	  return false
  })

  function saveImage() {
	var formData = new FormData();
	formData.append('image_file', $('#image_file')[0].files[0]);

	$.ajax({
		url: IMAGE,
		type: 'post',
		data: formData,
		cache: false,
		contentType: false,
		processData: false
	})
	.done(data => {
		console.log(data)
		response = JSON.parse(data)
		myTable.ajax.reload(null, false)
	})
	.fail(errorThrown => {
		console.log('Save Changes Post Error: ', errorThrown)
		return false
	})

}

function modify(data){
	$(`input[name*="modifyId"]`).val(data)
	$(`input[name*="formAction"]`).val('edit')

	$.ajax({
		url: API + '?getDetails=' + data,
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)

			$.unblockUI()
			populateForm($('form'), json)
			$('#formModal').modal('show')
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Account Details Get Error', errorThrown)
			return false
		})
}

function addEmployee(payroll_id){
	$("#empPayrollForm").trigger('reset')
	$("#payroll_id").val(payroll_id)
	$('#employeePayrollModal').modal('show')
	viewEmployeePayrollTable(payroll_id)
}

//employee datatable
var employeePayrollDataTable = null

function viewEmployeePayrollTable(payroll_id){

	$('#payrollEmpTable').DataTable().destroy()
	employeePayrollDataTable = $('#payrollEmpTable').DataTable({
		processing: true,
		ajax: {
			url: API + '?getAddedEmployeesToPayroll=' + payroll_id,
			dataSrc: function(json) {
				var return_data = new Array()
				for (var i = 0; i < json.length; i++) {
					
					return_data.push({
						id: json[i].id,
						emp_id: json[i].first_name + " " + json[i].middle_name + " "+ json[i].last_name ,
						gross_pay: `<label style="text-align: left;">`+json[i].gross_pay+`</label>`,
						total_deductions: `<label style="text-align: left;">`+json[i].total_deductions+`</label>`,
						net_pay: `<label style="text-align: left;">`+json[i].net_pay+`</label>`,
						action: `<button title="VIEW" type="button" class="btn btn-warning btn-circle" onclick="viewAddedEmp(`+json[i].id+`)"><i class="fa fa-eye"></i> </button>
								<button title="DELETE" type="button" class="btn btn-danger btn-circle" onclick="deleteAddedEmp(`+json[i].id+`)"><i class="fa fa-times"></i> </button>`,
					})
				}
				return return_data
			},

			complete: function() {
				//do something soon
			}
		},
		columns: [
			{ data: 'id' },
			{ data: 'emp_id' },
			{ data: 'gross_pay' },
			{ data: 'total_deductions' },
			{ data: 'net_pay' },
			{ data: 'action' },
		],
		order: [[0, 'desc']],
		rowReorder: {
			selector: 'td:nth-child(4)'
		},
		responsive: true,
	})
}

function viewEmployeeWithAttendance(id){
	var payrollInfo = {
		"payroll_id" : $("#payroll_id").val(),
		"emp_id" : id
	}

	$.blockUI({
		baseZ: 2000
	})

	$.ajax({
		url: API,
		type: 'post',
		data: 'getAttendanceLogsByEmployee=' + JSON.stringify(payrollInfo),
		processData: false
	})
		.done(data => {
			$.unblockUI()
			
			responseJSON = $.parseJSON(data)
			
			$("#position").val(responseJSON.employment_type)
			$("#employee_rate").val(responseJSON.rate)
			$("#cut_off").html(responseJSON.cut_off_date)
			$("#release").html(responseJSON.release_date)

			//attendance logs iteration
			$.each(responseJSON.attendance_logs, function (i, item) {
				$("#"+ i).html(item)
			});
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Error: ', errorThrown)
			return false
		})
}


function compute(action){
	var form = $("#empPayrollForm").serializeArray()
	var params = postParams(action, form)
	
	if($("#emp_id").val() == null){
		alert("No Employee Selected")
		return
	}

	if(action == "save"){
		if(!confirm("Are you sure you want to save employee payroll ?")){
			return
		}
	}

	$.ajax({
		url: API,
		type: 'post',
		data: 'payroll=' + params,
		processData: false
	})
		.done(data => {
			$.unblockUI()
			
			responseJSON = $.parseJSON(data)

			if(!responseJSON.error){
				$("#gross_pay").val(parseFloat(responseJSON.gross_pay))
				$("#total_deductions").val(parseFloat(responseJSON.total_deductions))
				$("#net_pay").val(parseFloat(responseJSON.net_pay))

				if(action == "save"){
					myTable.ajax.reload(null, false)
					viewEmployeePayrollTable($("#payroll_id").val())

					new PNotify({"type" : "success",
								"title" : "Successful!",
								"text" : "Employee Payroll added succesfully!"})
					
					$("#empPayrollForm").trigger("reset")
				}
			
			}else{
				alert(responseJSON.message)
			}
			
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Delete Post Error: ', errorThrown)
			return false
		})
}

function viewAddedEmp(id){
	$.ajax({
		url: API + '?viewAddedEmployee='+id,
		processData: false
	})
		.done(data => {
			console.log(data)
			var json = $.parseJSON(data)
			
			populateForm($('#empPayrollForm'), json)
			viewEmployeeWithAttendance(json.emp_id)
		})
		.fail(errorThrown => {
			console.log('Get Error: ', errorThrown)
			return false
		})
}

function deleteAddedEmp(id){
	if(!confirm("Are you sure you want to delete employee payroll ?")){
		return
	}

	$.ajax({
		url: API + '?deleteEmployeePayroll='+id,
		processData: false
	})
		.done(data => {
			console.log(data)
			var json = $.parseJSON(data)
			
			viewEmployeePayrollTable($("#payroll_id").val())
			new PNotify(json)
		})
		.fail(errorThrown => {
			console.log('Get Error: ', errorThrown)
			return false
		})
}

//select employees
$.ajax({
	url: EMP_API + '?get',
	processData: false
})
	.done(data => {
		var json = $.parseJSON(data)
		var newVal = '<option selected disabled value="">Select Employee</option>'

		newVal += json.map(value => {
			var name = value.first_name + " "+value.middle_name + " "+value.last_name
			return `<option value="${value.id}">${name}</option>`
		})

		$(`#emp_id`).html(newVal)
	})
	.fail(errorThrown => {
		console.log('Get Error: ', errorThrown)
		return false
	})


	function viewAll(){
		myTable.ajax.url(API + '?get').load()
	}
	
	function viewApproved(){
		myTable.ajax.url(API + '?get&status=APPROVED').load()
	}
	
	function viewPending(){
		myTable.ajax.url(API + '?get&status=PENDING').load()
	}
	
	function viewRejected(){
		myTable.ajax.url(API + '?get&status=REJECTED').load()
	}
