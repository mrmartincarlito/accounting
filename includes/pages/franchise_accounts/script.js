const FRANCHISE_API = './api/franchise_accounts.php';
const FRANCHISEE_ENTRIES_API = './api/franchise_entries.php';


$(document).ready(function() {

	// get franchisee owner name
	$.ajax({
		url: FRANCHISEE_ENTRIES_API + '?get',
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)
			var newVal = '<option selected disabled>Select Franchisee Owner</option>'
			
			newVal += json.map(value => {
				return `<option value="${value.id}">${value.name}</option>`
			})
			$(`#franchisee_id`).html(newVal)
		})
		.fail(errorThrown => {
			console.log('Get Error: ', errorThrown)
			return false
	})

	var myTable = $('#datatable').DataTable({
		processing: true,
		ajax: {
			url: FRANCHISE_API + '?get',
			dataSrc: function(json) {
				var return_data = new Array()
				for (var i = 0; i < json.length; i++) {
					return_data.push({
						id: json[i].id,
						franchisee_owner: json[i].name,
						complete_name: json[i].last_name + ', ' + json[i].first_name + ' ' + json[i].middle_name + '.',
						email: json[i].email,
						username: json[i].username,
					})
				}
				return return_data
			},

			complete: function() {
				$('#datatable tbody').on('dblclick', 'tr', function() {
					$('form').trigger('reset')
					$('input[type=checkbox]').prop('checked', false)

					var data = $('#datatable')
						.DataTable()
						.row(this)
						.data().id
					$('input[name*="modifyId"]').val(data)
					$('input[name*="formAction"]').val('edit')

					$.blockUI()

					$.ajax({
						url: FRANCHISE_API + '?getDetails=' + data, //employee id
						processData: false
					})
						.done(data => {
							var json = $.parseJSON(data)

							$.unblockUI()
							populateForm($('form'), json)
							$('#formModal').modal('show')
						})
						.fail(errorThrown => {
							$.unblockUI()
							console.log('Details Get Error', errorThrown)
							return false
						})
				})

				$('#addModal').on('click', function() {
					$('form').trigger('reset')
					$('input[name*="modifyId"]').val('')
					$('input[name*="formAction"]').val('add')
					$('#formModal').modal('show')
				})
			}
		},
		columns: [
			{ data: 'id' },
			{ data: 'franchisee_owner' },
			{ data: 'complete_name' },
			{ data: 'email' },
			{ data: 'username' }
		],
		order: [[0, 'desc']],
		rowReorder: {
			selector: 'td:nth-child(2)'
		},
		responsive: true,
	})

	$('form').on('submit', function(e) {
		e.preventDefault()

		$.blockUI({
			baseZ: 2000
		})

		var data = $('form').serializeArray()
		var params = postParams('', data)

		$.ajax({
			url: FRANCHISE_API,
			type: 'post',
			data: 'data=' + params,
			processData: false
		})
			.done(data => {
				$.unblockUI()
				console.log('Save Changes Response: ', data)

				responseJSON = $.parseJSON(data)

				new PNotify(responseJSON)

				if (responseJSON.type == 'success') {
					myTable.ajax.reload(null, false)
					$('#formModal').modal('hide')
				}
			})
			.fail(errorThrown => {
				$.unblockUI()
				console.log('Save Changes Post Error: ', errorThrown)
				return false
			})
	})

	//delete button
	$('#deleteButton').on('click', function() {
		if (!confirm('Are you sure you want to remove this account?')) {
			return false
		}

		$.blockUI({
			baseZ: 2000
		})

		$('input[name*="formAction"]').val('delete')

		var data = $('form').serializeArray()
		var params = postParams('', data)

		$.ajax({
			url: FRANCHISE_API,
			type: 'post',
			data: 'data=' + params,
			processData: false
		})
			.done(data => {
				$.unblockUI()
				console.log('Delete Response: ', data)

				responseJSON = $.parseJSON(data)
				new PNotify(responseJSON)
				if (responseJSON.type == 'success') {
					myTable.ajax.reload(null, false)
					$('#formModal').modal('hide')
				}
			})
			.fail(errorThrown => {
				$.unblockUI()
				console.log('Delete Post Error: ', errorThrown)
				return false
			})

		return false
	})
})
