const ORDER_API = './api/order.php'
const RETURN_ORDER_API = './api/return_order.php'

var returnRequestTable = null
returnRequestTable = $('#datatable').DataTable({
	processing: true,
	ajax: {
		url: RETURN_ORDER_API + '?getOverview',
		dataSrc: function (json) {
			var return_data = new Array()

			for (var i = 0; i < json.length; i++) {
				return_data.push({
					id: json[i].id,
					order_ref_no: json[i].order_ref_no,
					return_ref_no: json[i].return_ref_no,
					status: json[i].status,
					date_time: json[i].date_time,
					action : "<button class='btn-danger' onclick='cancel("+json[i].id+")'>CANCEL</button>"
				})
			}

			return return_data
		},

		complete: function () {
			//do something soon
		}
	},
	columns: [
		{ data: 'id' },
		{ data: 'order_ref_no' },
		{ data: 'return_ref_no' },
		{ data: 'status' },
		{ data: 'date_time' },
		{ data: 'action' }
	],
	order: [[0, 'desc']],
	rowReorder: {
		selector: 'td:nth-child(4)'
	},
	responsive: true,
})

var poItemsDataTable = null
var selectedItemId = []


function loadOrderItems() {
	var action = "receiving"
	$("#poModal").modal("show")
	$('#poItemsTable').DataTable().destroy()
	poItemsDataTable = $('#poItemsTable').DataTable({
		processing: true,
		ajax: {
			url: ORDER_API + '?viewOrderedItems&order_ref=' + $("#order_ref_no").val(),
			dataSrc: function (json) {
				var return_data = new Array()
				var total_amount = 0;

				if (action == "receiving") {
					for (var i = 0; i < json.length; i++) {
						$("#owner").html(json[i].franchisee_owner)
						$("#franchisee_id").val(json[i].franchisee_id)
						selectedItemId.push(json[i].id)
						$("#order_ref_no").html(json[i].order_ref_no)

						total_amount = total_amount + parseFloat(json[i].total_amount)
						return_data.push({
							id: json[i].id,
							product_id: json[i].description,
							qty: "<input class='form-control' readonly id='qty_" + json[i].id + "' type='number' style='text-align:right' value='" + json[i].qty + "'/>",
							ret_qty: "<input class='form-control' onkeyup='calculatePrice("+json[i].id+")' id='ret_qty_" + json[i].id + "' type='number' style='text-align:right' value='" + json[i].qty + "'/>",
							cost: "<input class='form-control' id='price_" + json[i].id + "' type='text' style='text-align:right' readonly value='" + json[i].price + "'>",
							total_amount: "<input class='form-control' id='total_amount_" + json[i].id + "' type='text' style='text-align:right' readonly value='" + json[i].total_amount + "'>",
							action: '<input type="checkbox" name="returnthis[]" id="return_' + json[i].id + '" value="'+json[i].id+'"> RETURN THIS'
						})
					}
				}

				$("#overall_total_amount").html(formatNumber(total_amount))
				return return_data
			},

			complete: function () {
				//do something soon
			}
		},
		columns: [
			{ data: 'id' },
			{ data: 'product_id' },
			{ data: 'qty' },
			{ data: 'ret_qty' },
			{ data: 'cost' },
			{ data: 'total_amount' },
			{ data: 'action' },
		],
		order: [[0, 'desc']],
		rowReorder: {
			selector: 'td:nth-child(4)'
		},
		responsive: true,
	})
}

function calculatePrice(item_id){

	var price = $("#price_"+item_id).val()
	var qty = $("#ret_qty_"+item_id).val()

	if(qty > parseFloat($("#qty_"+item_id).val())){
		$("#ret_qty_"+item_id).val($("#qty_"+item_id).val())
	}
	
	if(qty == "" || qty == "null" || qty == "undefined" || qty == "0"){
		$("#total_amount_"+item_id).val(1 * price)
	}else{
		$("#total_amount_"+item_id).val(qty * price)
	}

}
function proceedReturn() {
	if (!confirm('Are you sure you want to place this return goods?')) {
		return false
	}

	var selectedReturns = [];
	$('input[name="returnthis[]"]:checked').each(function () {
		selectedReturns[selectedReturns.length] = (this.checked ? $(this).val() : "");
	});

	if(selectedReturns.length == 0){
		alert("No selected item to return")
		return
	}

	var orderInfo = {
		order_ref_no : $("#order_ref_no").val(),
		remarks : $("#remarks").val(),
		franchisee_id : $("#franchisee_id").val()
	}

	var orders = []
	for(var i=0; i<selectedReturns.length; i++){
		var order = {			
			id : selectedReturns[i],
			price : $("#price_"+selectedReturns[i]).val(),
			qty : $("#ret_qty_"+selectedReturns[i]).val(),
			total_amount : $("#total_amount_"+selectedReturns[i]).val(),			
		}

		orders.push(order)
	}

	var orderParams = {
		info : orderInfo,
		orders : orders
	}

	$.blockUI({
		baseZ: 2000
	})

	$.ajax({
		url: RETURN_ORDER_API,
		type: 'post',
		data: 'proceedReturn=' + JSON.stringify(orderParams),
		processData: false
	})
		.done(data => {
			$.unblockUI()
			
			responseJSON = $.parseJSON(data)
			new PNotify(responseJSON)
			if (responseJSON.type == 'success') {
				returnRequestTable.ajax.reload(null, false)
			}
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Delete Post Error: ', errorThrown)
			return false
		})
}

function cancel(id) {
	if (!confirm('Are you sure you want to this return ?')) {
		return false
	}

	$.ajax({
		url: RETURN_ORDER_API,
		type: 'post',
		data: 'cancelReturn=' + id,
		processData: false
	})
		.done(data => {
			$.unblockUI()
			
			responseJSON = $.parseJSON(data)
			new PNotify(responseJSON)
			if (responseJSON.type == 'success') {
				returnRequestTable.ajax.reload(null, false)
			}
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Delete Post Error: ', errorThrown)
			return false
		})

}