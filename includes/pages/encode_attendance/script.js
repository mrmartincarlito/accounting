const API = './api/encode_attendance.php'
const EMP_API = './api/employee.php';

$('')
var myTable = null;
  $.fn.dataTable.ext.search.push(
	  function (settings, data, dataIndex) {
		  var startDate = Date.parse($('#start-date').val(), 10);
		  var endDate = Date.parse($('#end-date').val(), 10);
		  var columnDate = Date.parse(data[4]) || 0; // use data for the age column
		  if ((isNaN(startDate) && isNaN(endDate)) ||
			  (isNaN(startDate) && columnDate <= endDate) ||
			  (startDate <= columnDate && isNaN(endDate)) ||
			  (startDate <= columnDate && columnDate <= endDate)) {
			  return true;
		  }
		  return false;
	  }
  );

  $('.date-range-filter').change(function () {
	  myTable.draw();
  });

  myTable = $('#datatable').DataTable({
	  processing: true,
	  serverSide: true,
	  rowReorder: {
		  selector: 'td:nth-child(3)'
	  },
	  responsive: true,
	  order: [
		  [0, 'desc']
	  ],
	  buttons: [
		  {
			  extend: 'excel',
			  text: 'Export to Excel'
		  },
		  {

			  extend: 'pdf',
			  text: 'Export to Pdf',
			  orientation: 'portrait',
			  filename: 'Attendance Information',
			  paging: true,
			  customize: function (doc) {
				  doc.content.splice(0, 1);
				  var now = new Date();
				  var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();
				  doc.pageMargins = [20, 60, 20, 30];
				  doc.defaultStyle.fontSize = 8;
				  doc.styles.tableHeader.fontSize = 8;
				  doc['header'] = (function () {
					  return {
						  columns: [{
							  alignment: 'left',
							  text: 'Attendance Information',
							  fontSize: 20,
							  margin: [20, 20]
						  }]
					  }
				  });
				  doc['footer'] = (function () {
					  return {
						  columns: [{
							  alignment: 'left',
							  text: 'Created on: ' + jsDate.toString(),
							  margin: [10, 10]
						  }]
					  }
				  })
			  }
		  }
	  ],
	  dom: 'lBfrtip',
	  "language": {
		  "lengthMenu": 'Display <select>' +
			  '<option value="10">10</option>' +
			  '<option value="50">50</option>' +
			  '<option value="70">70</option>' +
			  '<option value="80">80</option>' +
			  '<option value="100">100</option>' +
			  '<option value="-1">All</option>' +
			  '</select> records'
	  },
	  ajax: {
		  url: API + '?get',
		  complete: function () {

			  $('#addModal').on('click', function () {
				  $('form').trigger('reset')
				  $('input[type=checkbox]').prop('checked', false)
				  $(`input[name*="modifyId"]`).val('')
				  $(`input[name*="formAction"]`).val('add')
				  $('#formModal').modal('show')
			  })
		  }
	  }
  })

  $('#viewModal').on('click', function () {
	  $('form').trigger('reset')
	  $('input[type=checkbox]').prop('checked', false)
	  $(`input[name*="modifyId"]`).val('')
	  $(`input[name*="formAction"]`).val('filter')
	  $('#viewFormModal').modal('show')
  })

$('form').on('submit', function(e) {
	e.preventDefault()

	$.blockUI({
		baseZ: 2000
	})

	var data = $('form').serializeArray()
	var params = postParams('', data)

	$.ajax({
		url: API,
		type: 'post',
		data: 'data=' + params,
		processData: false
	})
		.done(data => {
			$.unblockUI()
			console.log('Save Changes Response: ', data)
			responseJSON = $.parseJSON(data)

			new PNotify(responseJSON)

			if (responseJSON.type == 'success') {
				myTable.ajax.reload(null, false)
				$('#formModal').modal('hide')
			}
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Save Changes Post Error: ', errorThrown)
			return false
		})
})

//delete button
$('#deleteButton').on('click', function() {
	if (!confirm('Are you sure you want to remove?')) {
		return false
	}

	$.blockUI({
		baseZ: 2000
	})

	$(`input[name*="formAction"]`).val('delete')

	var data = $('form').serializeArray()
	var params = postParams('', data)

	$.ajax({
		url: API,
		type: 'post',
		data: 'data=' + params,
		processData: false
	})
		.done(data => {
			$.unblockUI()
			console.log('Delete Response: ', data)

			responseJSON = $.parseJSON(data)
			new PNotify(responseJSON)
			if (responseJSON.type == 'success') {
				myTable.ajax.reload(null, false)
				$('#formModal').modal('hide')
			}
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Delete Post Error: ', errorThrown)
			return false
		})

	return false
})

//select employees
$.ajax({
	url: EMP_API + '?get',
	processData: false
})
	.done(data => {
		var json = $.parseJSON(data)
		var newVal = '<option selected disabled>Select Employee</option>'

		newVal += json.map(value => {
			var name = value.first_name + " "+value.middle_name + " "+value.last_name
			return `<option value="${value.id}">${name}</option>`
		})

		$(`#emp_id`).html(newVal)
		$(`#emp_id_filter`).html(newVal)
	})
	.fail(errorThrown => {
		console.log('Get Error: ', errorThrown)
		return false
	})

function modify(data){
  $(`input[name*="modifyId"]`).val(data)
  $(`input[name*="formAction"]`).val('edit')

  $.ajax({
	  url: API + '?getDetails=' + data,
	  processData: false
  })
	  .done(data => {
		  var json = $.parseJSON(data)

		  $.unblockUI()
		  populateForm($('form'), json)
		  $('#formModal').modal('show')
	  })
	  .fail(errorThrown => {
		  $.unblockUI()
		  console.log('Account Details Get Error', errorThrown)
		  return false
	  })
}

function resetTime(){
	$(`input[name*="total_minutes"]`).val("0")
	$(`input[name*="regular_legal_ot"]`).val("0")
	$(`input[name*="special_ot"]`).val("0")
	$(`input[name*="special_holiday"]`).val("0")
}

function filterData(){
	//2021-10-01 -> 20211001
	var emp_id = $('#emp_id_filter').val();
	var startDate = $(`input[name*="startDate"]`).val();
	var endDate = $(`input[name*="endDate"]`).val();
	myTable.ajax.url(API + '?get&emp='+emp_id+'&startDate='+startDate+'&endDate='+endDate).load()
	$('#viewFormModal').modal('hide')
}