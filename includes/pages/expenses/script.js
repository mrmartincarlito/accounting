
  const API = './api/expenses.php'
  const CATEGORY_API = './api/category.php'
  const SUB_CATEGORY_API = './api/sub_category.php'
  // On change of Expense Type
  $('.expense_type').change(function () {
	var type =  $('.expense_type').val()
		// get non-operartional expense
		$.ajax({
			url: CATEGORY_API + '?get&type='+type,
			processData: false
		})
			.done(data => {
				var json = $.parseJSON(data)
				var newVal = '<option selected disabled>Select Category</option>'
				
				newVal += json.map(value => {
					return `<option value="${value.category_name}">${value.category_name}</option>`
				})
				$(`#category`).html(newVal)
			})
			.fail(errorThrown => {
				console.log('Get Error: ', errorThrown)
				return false
		})
  });
 // On change of Category
  $('.category').change(function () {
	var category_name =  $('.category').val();
		$.ajax({
			url: SUB_CATEGORY_API + '?get&category='+category_name,
			processData: false
		})
			
			.done(data => {
				var json = $.parseJSON(data)
				var newVal = '<option selected disabled>Select Sub-Category</option>'
				
				newVal += json.map(value => {
					return `<option value="${value.sub_category_name}">${value.sub_category_name}</option>`
				})
				$(`#sub_category`).html(newVal)
			})
			.fail(errorThrown => {
				console.log('Get Error: ', errorThrown)
				return false
		})
  });

  var myTable = null;
	$.fn.dataTable.ext.search.push(
		function (settings, data, dataIndex) {
			var startDate = Date.parse($('#start-date').val(), 10);
			var endDate = Date.parse($('#end-date').val(), 10);
			var columnDate = Date.parse(data[4]) || 0; // use data for the age column
			if ((isNaN(startDate) && isNaN(endDate)) ||
				(isNaN(startDate) && columnDate <= endDate) ||		
				(startDate <= columnDate && isNaN(endDate)) ||
				(startDate <= columnDate && columnDate <= endDate)) {
				return true;
			}
			return false;
		}
	);

	$('.date-range-filter').change(function () {
		myTable.draw();
	});

	myTable = $('#datatable').DataTable({
		processing: true,
		serverSide: true,
		rowReorder: {
			selector: 'td:nth-child(3)'
		},
		responsive: true,
		order: [
			[0, 'desc']
		],
		buttons: [
			{
				extend: 'excel',
				text: 'Export to Excel'
			},
			{

				extend: 'pdf',
				text: 'Export to Pdf',
				orientation: 'portrait',
				filename: 'Expense Information',
				paging: true,
				customize: function (doc) {
					doc.content.splice(0, 1);
					var now = new Date();
					var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();
					doc.pageMargins = [20, 60, 20, 30];
					doc.defaultStyle.fontSize = 8;
					doc.styles.tableHeader.fontSize = 8;
					doc['header'] = (function () {
						return {
							columns: [{
								alignment: 'left',
								text: 'Expense Information',
								fontSize: 20,
								margin: [20, 20]
							}]
						}
					});
					doc['footer'] = (function () {
						return {
							columns: [{
								alignment: 'left',
								text: 'Created on: ' + jsDate.toString(),
								margin: [10, 10]
							}]
						}
					})
				}
			}
		],
		dom: 'lBfrtip',
		"language": {
			"lengthMenu": 'Display <select>' +
				'<option value="10">10</option>' +
				'<option value="50">50</option>' +
				'<option value="70">70</option>' +
				'<option value="80">80</option>' +
				'<option value="100">100</option>' +
				'<option value="-1">All</option>' +
				'</select> records'
		},
		ajax: {
			url: API + '?get',
			complete: function () {

				$('#addModal').on('click', function () {
					$('form').trigger('reset')
					$('input[type=checkbox]').prop('checked', false)
					$(`input[name*="modifyId"]`).val('')
					$(`input[name*="formAction"]`).val('add')
					$('#formModal').modal('show')
				})
			}
		}
	})

  $('form').on('submit', function(e) {
	  e.preventDefault()

	  $.blockUI({
		  baseZ: 2000
	  })

	  var data = $('form').serializeArray()
	  var params = postParams('', data)

	  $.ajax({
		  url: API,
		  type: 'post',
		  data: 'data=' + params,
		  processData: false
	  })
		  .done(data => {
			  $.unblockUI()
			  console.log('Save Changes Response: ', data)

			  responseJSON = $.parseJSON(data)

			  new PNotify(responseJSON)

			  if (responseJSON.type == 'success') {
				  myTable.ajax.reload(null, false)
				  $('#formModal').modal('hide')
			  }
		  })
		  .fail(errorThrown => {
			  $.unblockUI()
			  console.log('Save Changes Post Error: ', errorThrown)
			  return false
		  })
  })

  //delete button
  $('#deleteButton').on('click', function() {
	  if (!confirm('Are you sure you want to remove this Expense?')) {
		  return false
	  }

	  $.blockUI({
		  baseZ: 2000
	  })

	  $(`input[name*="formAction"]`).val('delete')

	  var data = $('form').serializeArray()
	  var params = postParams('', data)

	  $.ajax({
		  url: API,
		  type: 'post',
		  data: 'data=' + params,
		  processData: false
	  })
		  .done(data => {
			  $.unblockUI()
			  console.log('Delete Response: ', data)

			  responseJSON = $.parseJSON(data)
			  new PNotify(responseJSON)
			  if (responseJSON.type == 'success') {
				  myTable.ajax.reload(null, false)
				  $('#formModal').modal('hide')
			  }
		  })
		  .fail(errorThrown => {
			  $.unblockUI()
			  console.log('Delete Post Error: ', errorThrown)
			  return false
		  })

	  return false
  })

function modify(data){
	$(`input[name*="modifyId"]`).val(data)
	$(`input[name*="formAction"]`).val('edit')

	$.ajax({
		url: API + '?getDetails=' + data,
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)
			var type = json.expense_type;
			var category = json.category;
			var sub_category = json.sub_category;
			// get categories
			$.ajax({
				url: CATEGORY_API + '?get&type='+type,
				processData: false
			})
				.done(data => {
					var json_category = $.parseJSON(data)
					var categoryVal = '<option selected disabled>Select Category</option>'
					categoryVal += json_category.map(value => {
						if(value.category_name == category){
							selected = "selected";
						}
						return '<option value="' + value.category_name + '"' + (value.category_name === category ? 'selected="selected"' : '') +
       '>' + value.category_name+ '</option>';
					})
					$(`#category`).html(categoryVal)

				})
				.fail(errorThrown => {
					console.log('Get Error: ', errorThrown)
					return false
			})

				// get sub_categories
				$.ajax({
					url: SUB_CATEGORY_API + '?get&category='+category,
					processData: false
				})
					.done(data => {
						var json_sub_category = $.parseJSON(data)
						var subCategoryVal = '<option selected disabled>Select Sub - Category</option>'
						subCategoryVal += json_sub_category.map(value => {
							if(value.sub_category_name == sub_category){
								selected = "selected";
							}
							return '<option value="' + value.sub_category_name + '"' + (value.sub_category_name === sub_category ? 'selected="selected"' : '') +
		   '>' + value.sub_category_name+ '</option>';
						})
						$(`#sub_category`).html(subCategoryVal)
	
					})
					.fail(errorThrown => {
						console.log('Get Error: ', errorThrown)
						return false
				})
	

			
			$.unblockUI()
			populateForm($('form'), json)
			$('#formModal').modal('show')
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Expense Details Get Error', errorThrown)
			return false
		})
}


function viewAll(){
	myTable.ajax.url(API + '?get').load()
}
function viewOperational(){
	myTable.ajax.url(API + '?get&type=Operational').load()
}
function viewNonOperational(){
	myTable.ajax.url(API + '?get&type=Non-Operational').load()
}
function getNonOperational(){
	

}
function getOperational(){

}