const FRANCHISEE_ENTRIES_API = './api/franchise_entries.php';
const SUPPLIER_API = './api/supplier.php'
const PRODUCT_API = './api/product_information.php'
const REPORTS_API = './api/reports.php'
const BRANCH_API = './api/franchise_branch.php'

var selectedReport = ""


function statementOfAccount(){
	$("#no_content").css("display", "none")
	$("#daterange").css("display", "")
	$("#franchisee").css("display", "")
	$("#sales").css("display", "none")
	$("#purchase_order").css("display", "none")
	$("#returns").css("display", "none")
	selectedReport = "statement_of_account"
}

function sales() {
	$("#no_content").css("display", "none")
	$("#daterange").css("display", "")
	$("#returns").css("display", "none")
	$("#franchisee").css("display", "none")
	$("#sales").css("display", "")
	$("#purchase_order").css("display", "none")
	selectedReport = "sales"
}

function deliveryCharge(){
	$("#no_content").css("display", "none")
	$("#daterange").css("display", "")
	$("#returns").css("display", "none")
	$("#franchisee").css("display", "none")
	$("#sales").css("display", "none")
	$("#purchase_order").css("display", "none")
	selectedReport = "deliveryCharge"
}

function returnGoods() {
	$("#no_content").css("display", "none")
	$("#daterange").css("display", "")
	$("#returns").css("display", "")
	$("#franchisee").css("display", "none")
	$("#sales").css("display", "none")
	$("#purchase_order").css("display", "none")
	selectedReport = "return_goods"
}

function viewPayrollForm() {
	$("#no_content").css("display", "none")
	$("#daterange").css("display", "")
	$("#purchase_order").css("display", "none")
	$("#franchisee").css("display", "none")
	$("#sales").css("display", "none")
	$("#returns").css("display", "none")
	selectedReport = "payroll"
}

function viewPurchaseOrder() {
	$("#no_content").css("display", "none")
	$("#daterange").css("display", "")
	$("#purchase_order").css("display", "")
	$("#franchisee").css("display", "none")
	$("#sales").css("display", "none")
	$("#returns").css("display", "none")
	selectedReport = "purchase_order"
}

function viewBilling() {
	$("#no_content").css("display", "none")
	$("#daterange").css("display", "")
	$("#purchase_order").css("display", "none")
	$("#franchisee").css("display", "")
	$("#sales").css("display", "none")
	$("#returns").css("display", "none")
	selectedReport = "billing"
}

function viewOrders() {
	$("#no_content").css("display", "none")
	$("#daterange").css("display", "")
	$("#purchase_order").css("display", "none")
	$("#franchisee").css("display", "")
	$("#sales").css("display", "none")
	$("#returns").css("display", "none")
	selectedReport = "order"
}

function viewExpenses() {
	$("#no_content").css("display", "none")
	$("#daterange").css("display", "")
	$("#purchase_order").css("display", "none")
	$("#franchisee").css("display", "none")
	$("#sales").css("display", "none")
	$("#returns").css("display", "none")
	selectedReport = "expenses"
}

function viewExpensesDetailed() {
	$("#no_content").css("display", "none")
	$("#daterange").css("display", "")
	$("#purchase_order").css("display", "none")
	$("#franchisee").css("display", "none")
	$("#sales").css("display", "none")
	$("#returns").css("display", "none")
	selectedReport = "expenses_detailed"
}

function viewFranchise() {
	$("#no_content").css("display", "none")
	$("#daterange").css("display", "none")
	$("#purchase_order").css("display", "none")
	$("#franchisee").css("display", "none")
	$("#franchisee").css("display", "")
	$("#sales").css("display", "none")
	$("#returns").css("display", "none")
	selectedReport = "franchise"
}

function preview(){
	var data = {
		"report" : selectedReport,
		"data" : {
			"dateFrom" : $("#date_from").val(),
			"dateTo" : $("#date_to").val(),
			"franchisee" : $("#franchisee_id").val(),
			"supplier_id" : $("#supplier_id").val(),
			"product_id" : $("#product_id").val(),
			"branch_id" : $("#branch_id").val(),
			"sales_category" : $("#category").val(),
			"item_condition" : $("#item_condition").val()
		}
	}

	$.ajax({
		url: REPORTS_API + '?' + selectedReport + '=' + JSON.stringify(data),
		processData: false
	})
		.done(data => {
			var json = JSON.parse(data)

			$("#caption").html(json.caption)

			//get thead
			var th = "<tr>"
			$.each(json.columns, function (i, item) {
				th = th + "<th style='text-align:center'>"+item+"</th>"
			});
			th = th + "</tr>"
			$("#thead").html(th)

			var tr = ""

			if (json.rows.length == 0) {
				tr = tr + "<tr><td style='text-align:center' colspan='"+json.columns.length+"'>No Record Found</td></tr>"
			}

			//get tbody for payroll
			if(selectedReport == "payroll"){
				$.each(json.rows, function (i, item) {
					tr = tr + "<tr>"+
						"<td>"+item.payroll_no+"</td>"+
						"<td>"+item.cut_off_date+"</td>"+
						"<td>"+item.release_date+"</td>"+
						"<td>"+item.added_by+"</td>"+
						"<td>"+item.status+"</td>"+
						"<td>"+item.remarks+"</td>"+
						"<td style='text-align:right'>"+formatNumber(item.total_amount)+"</td>"+
					"</tr>"
				});
			}

			//get tbody for purchase order
			if(selectedReport == "purchase_order"){
				$.each(json.rows, function (i, item) {
					tr = tr + "<tr>"+
						"<td>"+item.po_number+"</td>"+
						"<td>"+item.date_po+"</td>"+
						"<td>"+item.supplier+"</td>"+
						"<td>"+item.product+"</td>"+
						"<td>"+item.cost+"</td>"+
						"<td>"+item.qty+"</td>"+
						"<td style='text-align:right'>"+formatNumber(item.item_total)+"</td>"+
						"<td>"+item.status+"</td>"+
						"<td>"+item.added_by+"</td>"+
					"</tr>"
				});
			}

			//get tbody for billing
			if(selectedReport == "billing"){
				$.each(json.rows, function (i, item) {
					tr = tr + "<tr>"+
						"<td>"+item.franchise+"</td>"+
						"<td>"+item.description+"</td>"+
						"<td>"+item.date_paid+"</td>"+
						"<td>"+item.check_no+"</td>"+
						"<td>"+item.bank+"</td>"+
						"<td>"+item.remarks+"</td>"+
						"<td>"+item.status+"</td>"+
						"<td>"+item.approved_by+"</td>"+
						"<td style='text-align:right'>"+formatNumber(item.total_amount)+"</td>"+
					"</tr>"
				});
			}

				//get tbody for order
				if(selectedReport == "order"){
					$.each(json.rows, function (i, item) {
						tr = tr + "<tr>"+
							"<td>"+item.franchise+"</td>"+
							"<td>"+item.date_paid+"</td>"+
							"<td>"+item.check_no+"</td>"+
							"<td>"+item.bank+"</td>"+
							"<td>"+item.remarks+"</td>"+
							"<td>"+item.status+"</td>"+
							"<td>"+item.approved_by+"</td>"+
							"<td style='text-align:right'>"+formatNumber(item.total_amount)+"</td>"+
						"</tr>"
					});
				}

			//get tbody for expenses
			if(selectedReport == "expenses"){
				$.each(json.rows, function (i, item) {
					tr = tr + "<tr>"+
						// "<td>"+item.date_paid+"</td>"+
						// "<td>"+item.voucher_number+"</td>"+
						"<td>"+item.expense_type+"</td>"+
						// "<td>"+item.expense_type+"</td>"+
						"<td>"+item.category+"</td>"+
						// "<td>"+item.added_by+"</td>"+
						"<td style='text-align:right'>"+formatNumber(item.total_amount)+"</td>"+
					"</tr>"
				});
			}

			//get tbody for expenses
			if(selectedReport == "expenses_detailed"){
				$.each(json.rows, function (i, item) {
					tr = tr + "<tr>"+
						// "<td>"+item.date_paid+"</td>"+
						// "<td>"+item.voucher_number+"</td>"+
						"<td style='text-align:center'>"+item.expense_type+"</td>"+
						// "<td>"+item.expense_type+"</td>"+
						"<td style='text-align:center'>"+item.category+"</td>"+
						"<td style='text-align:center'>"+item.sub_category+"</td>"+
						// "<td>"+item.added_by+"</td>"+
						"<td style='text-align:right'>"+formatNumber(item.total_amount)+"</td>"+
					"</tr>"
				});
			}

			//get tbody for expenses
			if(selectedReport == "franchise"){
				$.each(json.rows, function (i, item) {
					tr = tr + "<tr>"+
						"<td>"+item.franchise+"</td>"+
						"<td>"+item.expiry_date+"</td>"+
						"<td>"+item.renewed_date+"</td>"+
						"<td>"+item.new_expiry_date+"</td>"+
						"<td>"+item.approved_by+"</td>"+
						"<td>"+item.date_time+"</td>"+
					"</tr>"
				});
			}

			//get tbody for statement of account
			if(selectedReport == "statement_of_account"){
				$.each(json.rows, function (i, item) {
					tr = tr + "<tr>"+
						"<td style='text-align:center'>"+item.date+"</td>"+
						"<td style='text-align:center'>"+item.ref_no+"</td>"+
						"<td style='text-align:right'>"+item.order_amount+"</td>"+
						"<td style='text-align:right'>"+item.order_payments+"</td>"+
						"<td style='text-align:right'>"+item.billing_amount+"</td>"+
						"<td style='text-align:right'>"+item.billing_payments+"</td>"+
					"</tr>"
				});
			}

			var category = $("#category").val()

			//get tbody sales per branch
			if(selectedReport == "sales" && category == 1){
				$.each(json.rows, function (i, item) {
					tr = tr + "<tr>"+
						"<td style='text-align:center'>"+item.branch+"</td>"+
						"<td style='text-align:right'>"+item.total_amount+"</td>"+
					"</tr>"
				});
			}

			//get tbody sales per franchise
			if(selectedReport == "sales" && category == 2){
				$.each(json.rows, function (i, item) {
					tr = tr + "<tr>"+
						"<td style='text-align:center'>"+item.franchisee+"</td>"+
						"<td style='text-align:center'>"+item.date+"</td>"+
						"<td style='text-align:center'>"+item.or_invoice+"</td>"+
						"<td style='text-align:right'>"+item.amount+"</td>"+
					"</tr>"
				});
			}

			//get tbody sales per batch distribution
			if(selectedReport == "sales" && category == 3){
				$.each(json.rows, function (i, item) {
					tr = tr + "<tr>"+
						"<td style='text-align:center'>"+item.batch_no+"</td>"+
						"<td style='text-align:center'>"+item.product+"</td>"+
						"<td style='text-align:center'>"+item.order_ref+"</td>"+
						"<td style='text-align:center'>"+item.qty+"</td>"+
						"<td style='text-align:right'>"+item.price+"</td>"+
						"<td style='text-align:right'>"+item.sales+"</td>"+
					"</tr>"
				});
			}

			//get tbody sales per delivery fee
			if(selectedReport == "sales" && category == 4){
				$.each(json.rows, function (i, item) {
					tr = tr + "<tr>"+
						"<td style='text-align:center'>"+item.franchisee+"</td>"+
						"<td style='text-align:center'>"+item.order_ref+"</td>"+
						"<td style='text-align:right'>"+item.amount+"</td>"+
					"</tr>"
				});
			}

			//get tbody sales per product
			if(selectedReport == "sales" && category == 5){
				$.each(json.rows, function (i, item) {
					tr = tr + "<tr>"+
						"<td style='text-align:center'>"+item.product+"</td>"+
						"<td style='text-align:center'>"+item.qty+"</td>"+
						"<td style='text-align:right'>"+item.sales+"</td>"+
					"</tr>"
				});
			}

			//get tbody return goods
			if(selectedReport == "return_goods"){
				$.each(json.rows, function (i, item) {
					tr = tr + "<tr>"+
						"<td style='text-align:center'>"+item.franchisee+"</td>"+
						"<td style='text-align:center'>"+item.order_ref+"</td>"+
						"<td style='text-align:center'>"+item.return_ref+"</td>"+
						"<td style='text-align:center'>"+item.condition+"</td>"+
						"<td style='text-align:center'>"+item.item+"</td>"+
						"<td style='text-align:center'>"+item.qty+"</td>"+
						"<td style='text-align:right'>"+item.price+"</td>"+
						"<td style='text-align:right'>"+item.amount+"</td>"+
					"</tr>"
				});
			}

			//get tbody deliverycharge
			if(selectedReport == "deliveryCharge"){
				$.each(json.rows, function (i, item) {
					tr = tr + "<tr>"+
						"<td style='text-align:center'>"+item.order_ref+"</td>"+
						"<td style='text-align:center'>"+item.date_ordered+"</td>"+
						"<td style='text-align:right'>"+item.delivery_charge+"</td>"+
					"</tr>"
				});
			}

			$("#tbody").html(tr)

			var others = "<table class='table table-bordered'>"
			//other data on report
			$.each(json.others, function (i, item) {
				var value = item.value
				
				if(item.is_numeric == "true"){
					value = formatNumber(item.value)
				}
				others = others + "<tr><td style='text-align:center'>"+item.name+"</td><td style='text-align:right'><b>"+  value +"</b></td></tr>"
			})

			others = others + "</table>"

			$("#others").html(others)

		})
		.fail(errorThrown => {
			console.log('Get Error: ', errorThrown)
			return false
		})
}

function printReport(printpage){
	var headstr = "<html><head><title></title></head><body>";
	var footstr = "</body>";
	var newstr = document.all.item(printpage).innerHTML;
	var oldstr = document.body.innerHTML;
	document.body.innerHTML = headstr + newstr + footstr;
	window.print();
	document.body.innerHTML = oldstr;
	return false;
}

getFranchise()
function getFranchise() {
	// get franchisee owner name
	$.ajax({
		url: FRANCHISEE_ENTRIES_API + '?get',
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)
			var newVal = '<option selected value="">All Franchisee</option>'

			newVal += json.map(value => {
				return `<option value="${value.id}">${value.name}</option>`
			})
			$(`#franchisee_id`).html(newVal)
		})
		.fail(errorThrown => {
			console.log('Get Error: ', errorThrown)
			return false
		})
}

getSupplier()
function getSupplier(){
	  // get supplier list
	  $.ajax({
		url: SUPPLIER_API + '?get',
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)
			var newVal = '<option selected  value="">Select Supplier</option>'
			
			newVal += json.map(value => {
				return `<option value="${value.id}">${value.supplier_name}</option>`
			})
			$(`#supplier_id`).html(newVal)
		})
		.fail(errorThrown => {
			console.log('Get Error: ', errorThrown)
			return false
	})
}

getProducts()
function getProducts(){
	$.ajax({
		url: PRODUCT_API + '?getAll',
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)
			var newVal = '<option selected value="">All Products</option>'
			
			newVal += json.map(value => {
				return `<option value="${value.id}">${value.description}</option>`
			})
			$(`#product_id`).html(newVal)
			$.unblockUI()
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Get Error: ', errorThrown)
			return false
	})
}

getBranch()
function getBranch(){
	$.ajax({
		url: BRANCH_API + '?get',
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)
			var newVal = '<option selected value="">All Branches</option>'
			
			newVal += json.map(value => {
				return `<option value="${value.id}">[${value.branch_code}] ${value.branch_name}</option>`
			})
			$(`#branch_id`).html(newVal)
			$.unblockUI()
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Get Error: ', errorThrown)
			return false
	})
}

function filterByBranch(branch_id){
	// get franchisee owner name
	$.ajax({
		url: FRANCHISEE_ENTRIES_API + '?filterByBranch=' + branch_id,
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)
			var newVal = '<option selected value="">All Branch Franchise</option>'

			newVal += json.map(value => {
				return `<option value="${value.id}">${value.name}</option>`
			})
			$(`#franchisee_id`).html(newVal)
		})
		.fail(errorThrown => {
			console.log('Get Error: ', errorThrown)
			return false
		})
}

function salesCategory(category) {
	if (category == 2) { //Per Franchisee
		$("#franchisee").show()
		$("#branch_div").hide()

		$("#purchase_order").hide()
		$("#supplier_div").show()
	}

	else if (category == 5) { //Per Product
		$("#franchisee").show()
		$("#branch_div").hide()

		$("#purchase_order").show()
		$("#supplier_div").hide()
	}

	else {
		$("#franchisee").hide()
		$("#branch_div").show()

		$("#purchase_order").hide()
		$("#supplier_div").show()
	}
}