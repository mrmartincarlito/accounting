const ORDER_API = './api/order.php'

var myTable = null;
$.fn.dataTable.ext.search.push(
	function (settings, data, dataIndex) {
		var startDate = Date.parse($('#start-date').val(), 10);
		var endDate = Date.parse($('#end-date').val(), 10);
		var columnDate = Date.parse(data[4]) || 0; // use data for the age column
		if ((isNaN(startDate) && isNaN(endDate)) ||
			(isNaN(startDate) && columnDate <= endDate) ||
			(startDate <= columnDate && isNaN(endDate)) ||
			(startDate <= columnDate && columnDate <= endDate)) {
			return true;
		}
		return false;
	}
);

$('.date-range-filter').change(function () {
	myTable.draw();
});

myTable = $('#datatable').DataTable({
	processing: true,
	serverSide: true,
	rowReorder: {
		selector: 'td:nth-child(3)'
	},
	responsive: true,
	order: [
		[0, 'desc']
	],
	buttons: [
		{
			extend: 'excel',
			text: 'Export to Excel'
		},
		{

			extend: 'pdf',
			text: 'Export to Pdf',
			orientation: 'portrait',
			filename: 'Order Information',
			paging: true,
			customize: function (doc) {
				doc.content.splice(0, 1);
				var now = new Date();
				var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();
				doc.pageMargins = [20, 60, 20, 30];
				doc.defaultStyle.fontSize = 8;
				doc.styles.tableHeader.fontSize = 8;
				doc['header'] = (function () {
					return {
						columns: [{
							alignment: 'left',
							text: 'Order Information',
							fontSize: 20,
							margin: [20, 20]
						}]
					}
				});
				doc['footer'] = (function () {
					return {
						columns: [{
							alignment: 'left',
							text: 'Created on: ' + jsDate.toString(),
							margin: [10, 10]
						}]
					}
				})
			}
		}
	],
	"language": {
		"lengthMenu": 'Display <select>' +
			'<option value="10">10</option>' +
			'<option value="50">50</option>' +
			'<option value="70">70</option>' +
			'<option value="80">80</option>' +
			'<option value="100">100</option>' +
			'<option value="-1">All</option>' +
			'</select> records'
	},
	ajax: {
		url: ORDER_API + '?get',
		complete: function () {

		}
	}
})

function cancelOrder() {
	var order_ref = $("#order_ref_no").html()

	if ($("#remarks").val() == "") {
		alert("Please input remarks");
		return
	}

	if (!confirm('Are you sure you want to cancel the order?')) {
		return false
	}

	$.blockUI({
		baseZ: 2000
	})

	$.ajax({
		url: ORDER_API + '?cancelOrder=' + order_ref + '&remarks=' + $("#remarks").val(),
		processData: false
	})
		.done(data => {

			$.unblockUI()

			responseJSON = $.parseJSON(data)

			new PNotify(responseJSON)
			if (responseJSON.type == 'success') {
				myTable.ajax.reload(null, false)
				$('#poModal').modal('hide')
			}
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Get Error: ', errorThrown)
			return false
		})
}

function receiveItems(id){
	loadOrderItems(id, "receiving")
}

var selectedItemId = []
var selectedOrderId = 0;
var selectedDeliveryCharge = 0;
function loadOrderItems(id, action = "view") {
	$("#poModal").modal("show")
	$('#poItemsTable').DataTable().destroy()
	selectedItemId = []
	poItemsDataTable = $('#poItemsTable').DataTable({
		processing: true,
		ajax: {
			url: ORDER_API + '?viewOrderedItems=' + id,
			dataSrc: function (json) {
				var return_data = new Array()
				var total_amount = 0;

				if(action == "view"){
					$("#receiveOrderButton").hide()
					$("#printDrButton").show()
					$("#receivingForm").hide()
					$("#orderAgainButton").show()
					$("#cancelButton").show()
					var discount = 0
					for (var i = 0; i < json.length; i++) {
						selectedItemId.push(json[i].id)
						$("#order_ref_no").html(json[i].order_ref_no)
						total_amount = total_amount + parseFloat(json[i].total_amount)
						
						//var readonly = json[i].special_price_tag == "YES" ? "" : "readonly"
						var readonly = "readonly"

						if (json[i].status == "DISPOSED") {
							$("#approveButton").hide();
							$("#printDrButton").hide()
						}

						$("#remarks").val(json[i].remarks)
						discount = parseFloat(json[i].discount_amount)
						selectedOrderId = json[i].order_id
						selectedDeliveryCharge = json[i].delivery_charge
						$("#delivery_charge").html(json[i].delivery_charge)
						$("#discount_voucher").html(`<input type="text" class="form-control" id="discount_voucher_`+json[i].order_id+`" value="`+json[i].discount_voucher+`">`)
						$("#discount_amount").html(`<input type="number" style="text-align: right;" class="form-control" id="discount_amount_`+json[i].order_id+`" onkeyup="calculateOverAllTotal(`+json[i].order_id+`)" value="`+json[i].discount_amount+`">`)
						return_data.push({
							id: json[i].id,
							product_id: json[i].description,
							qty: "<input class='form-control' readonly onkeyup='calculatePrice("+json[i].id+")' id='qty_"+json[i].id+"' type='number' style='text-align:right' value='"+json[i].qty+"'/>",
							cost: "<input class='form-control' onkeyup='calculatePrice("+json[i].id+", "+json[i].order_id+")' id='price_"+json[i].id+"' type='text' style='text-align:right' "+readonly+" value='"+json[i].price+"'>",
							total_amount: "<input class='form-control' id='total_amount_"+json[i].id+"' type='text' style='text-align:right' readonly value='"+json[i].total_amount+"'>",
						})
					}
				}
				
				$("#overall_total_amount").html(formatNumber((total_amount - discount) + selectedDeliveryCharge))
				return return_data
			},

			complete: function () {
				//do something soon
			}
		},
		columns: [
			{ data: 'id' },
			{ data: 'product_id' },
			{ data: 'qty' },
			{ data: 'cost' },
			{ data: 'total_amount' },
		],
		order: [[0, 'desc']],
		rowReorder: {
			selector: 'td:nth-child(4)'
		},
		responsive: true,
	})
}

function calculatePrice(item_id, order_id){
	var price = $("#price_"+item_id).val()
	var qty = parseFloat($("#qty_"+item_id).val())
	
	if(qty == "" || qty == "null" || qty == "undefined" || qty == "0"){
		$("#total_amount_"+item_id).val(1 * price)
	}else{
		$("#total_amount_"+item_id).val(qty * price)
	}

	calculateOverAllTotal(order_id)
}

function calculateOverAllTotal(order_id = 0){
	var total_amount = 0;

	for(var i=0; i<selectedItemId.length; i++){
		total_amount = total_amount + parseFloat($("#total_amount_"+selectedItemId[i]).val())
	}

	if($("#discount_amount_"+order_id).val() !== ""){
		total_amount = total_amount - parseFloat($("#discount_amount_"+order_id).val())
	}

	$("#overall_total_amount").html(total_amount + selectedDeliveryCharge)

	if($("#overall_total_amount").html() == "NaN"){
		calculateOverAllTotal()
	}
}


function approveOrder(){
	var order_ref = $("#order_ref_no").html()

	var orders = []
	var selectedItems = selectedItemId

	for (var i = 0; i < selectedItems.length; i++) {
		var order = {
			item_id: selectedItems[i],
			price: $("#price_" + selectedItems[i]).val(),
			qty: $("#qty_" + selectedItems[i]).val(),
			total_amount: $("#total_amount_" + selectedItems[i]).val(),
		}

		orders.push(order)
	}

	var orderParams = {
		order_ref : order_ref,
		discount_voucher : $("#discount_voucher_"+selectedOrderId).val(),
		discount_amount : $("#discount_amount_"+selectedOrderId).val(),
		overall_total: $("#overall_total_amount").html(),
		orders  : orders
	}

	if (!confirm('Are you sure you want to approve this order?')) {
		return false
	}

	$.blockUI({
		baseZ: 2000
	})

	$.ajax({
		url: ORDER_API + '?approveOrder=' + JSON.stringify(orderParams),
		processData: false
	})
		.done(data => {

			$.unblockUI()

			responseJSON = $.parseJSON(data)

			new PNotify(responseJSON)
			if (responseJSON.type == 'success') {
				myTable.ajax.reload(null, false)
				$('#poModal').modal('hide')
			}
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Get Error: ', errorThrown)
			return false
		})
}

function viewAll() {
	myTable.ajax.url(ORDER_API + '?get').load()
}

function viewApproved() {
	myTable.ajax.url(ORDER_API + '?get&status=APPROVED').load()
}

function viewPending() {
	myTable.ajax.url(ORDER_API + '?get&status=PENDING').load()
}

function viewRejected() {
	myTable.ajax.url(ORDER_API + '?get&status=REJECTED').load()
}

function viewReceived() {
	myTable.ajax.url(ORDER_API + '?get&status=RECEIVED').load()
}

function printDR() {
	window.open('reports/dr.php?or=' + $("#order_ref_no").html(), '_blank')
}

function printSI() {
	window.open('reports/si.php?or=' + $("#order_ref_no").html(), '_blank')
}

function printCR() {
	window.open('reports/cr.php?or=' + $("#order_ref_no").html(), '_blank')
}

function printAR() {
	window.open('reports/ar.php?or=' + $("#order_ref_no").html(), '_blank')
}

function printOR() {
	window.open('reports/or.php?or=' + $("#order_ref_no").html(), '_blank')
}