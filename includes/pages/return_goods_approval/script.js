const ORDER_API = './api/order.php'
const RETURN_ORDER_API = './api/return_order.php'

var myTable = null;
$.fn.dataTable.ext.search.push(
	function (settings, data, dataIndex) {
		var startDate = Date.parse($('#start-date').val(), 10);
		var endDate = Date.parse($('#end-date').val(), 10);
		var columnDate = Date.parse(data[4]) || 0; // use data for the age column
		if ((isNaN(startDate) && isNaN(endDate)) ||
			(isNaN(startDate) && columnDate <= endDate) ||
			(startDate <= columnDate && isNaN(endDate)) ||
			(startDate <= columnDate && columnDate <= endDate)) {
			return true;
		}
		return false;
	}
);

$('.date-range-filter').change(function () {
	myTable.draw();
});

myTable = $('#datatable').DataTable({
	processing: true,
	serverSide: true,
	rowReorder: {
		selector: 'td:nth-child(3)'
	},
	responsive: true,
	order: [
		[0, 'desc']
	],
	buttons: [
		{
			extend: 'excel',
			text: 'Export to Excel'
		},
		{

			extend: 'pdf',
			text: 'Export to Pdf',
			orientation: 'portrait',
			filename: 'Order Information',
			paging: true,
			customize: function (doc) {
				doc.content.splice(0, 1);
				var now = new Date();
				var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();
				doc.pageMargins = [20, 60, 20, 30];
				doc.defaultStyle.fontSize = 8;
				doc.styles.tableHeader.fontSize = 8;
				doc['header'] = (function () {
					return {
						columns: [{
							alignment: 'left',
							text: 'Order Information',
							fontSize: 20,
							margin: [20, 20]
						}]
					}
				});
				doc['footer'] = (function () {
					return {
						columns: [{
							alignment: 'left',
							text: 'Created on: ' + jsDate.toString(),
							margin: [10, 10]
						}]
					}
				})
			}
		}
	],
	"language": {
		"lengthMenu": 'Display <select>' +
			'<option value="10">10</option>' +
			'<option value="50">50</option>' +
			'<option value="70">70</option>' +
			'<option value="80">80</option>' +
			'<option value="100">100</option>' +
			'<option value="-1">All</option>' +
			'</select> records'
	},
	ajax: {
		url: RETURN_ORDER_API + '?get',
		complete: function () {

		}
	}
})

function viewReturnGoods(return_ref){
	$("#returnPOModal").modal('show')

	$('#returnItemsTable').DataTable().destroy()
	$('#returnItemsTable').DataTable({
		processing: true,
		ajax: {
			url: RETURN_ORDER_API + '?getReturnItems=' +return_ref,
			dataSrc: function (json) {
				var return_data = new Array()
				for (var i = 0; i < json.length; i++) {
					$("#return_ref_no").html(json[i].return_ref)
					return_data.push({
						id: json[i].return_ref,
						item: json[i].item,
						return_qty: json[i].return_qty,
						cost: json[i].cost,
						total_amount: json[i].total_amount
					})
				}
				return return_data
			},

			complete: function () {
				$('#poModal').modal('show')
			}
		},
		columns: [
			{ data: 'item' },
			{ data: 'return_qty' },
			{ data: 'cost' },
			{ data: 'total_amount' }
		],
		order: [[0, 'desc']],
		rowReorder: {
			selector: 'td:nth-child(4)'
		},
		responsive: true,
	})
}

function approveReturn(status){
	if (!confirm("Are you sure you want to "+status + " ?")) {
		return
	}

	$.blockUI({
		baseZ: 2000
	})

	$.ajax({
		url: RETURN_ORDER_API,
		type: 'post',
		data: 'approveStatus=' + status + '&return_ref=' + $("#return_ref_no").html(),
		processData: false
	})
		.done(data => {
			$.unblockUI()

			responseJSON = $.parseJSON(data)

			new PNotify(responseJSON)
			$("#returnPOModal").modal('hide')
			myTable.ajax.reload(null, false)

		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Post Error: ', errorThrown)
			return false
		})
}

function printDR() {
	window.open('reports/return receipt.php?or=' + $("#return_ref_no").html(), '_blank')
}