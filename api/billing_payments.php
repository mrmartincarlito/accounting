<?php
require_once("config.php");
require_once("logs.php");
require_once("payments-movements.php");

$loggedUser = json_decode(getLoggedUserDetails($database));

if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    $approved_by = "";
    $approved_date = null;

    $checkApprovalAccess = checkApproval($database, $data->status);

    if($checkApprovalAccess == "NO ACCESS"){
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => "You do not have approval or reject power, Please change your status to PENDING to proceed"
        ));

        return;
    }

    if(!isset($data->description)){
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => "Please check the current billing balance is selected properly"
        ));

        return;
    }

    $or_number = "";
    $invoice = "";
    $billing = explode("|", $data->description);

    if(isset($data->status) && $data->status == "APPROVED"){
        $approved_by = $loggedUser->username;
        $approved_date = $database->now();

        $or_number = isset($data->or_number) ? $data->or_number : "";
        $invoice = isset($data->invoice_number) ? $data->invoice_number : "";

        setBillingToPaid($database, $billing[0], $or_number, $invoice);
    }

    if($data->formAction == "add"){

        $insertData = Array (
            "franchise_id" => $data->franchise_id,
            "bill_id" => $billing[0],
            "description" => $billing[1],
            "total_amount" => $data->total_amount,
            "date_paid" => $data->date_paid,
            "check_no" => isset($data->check_no) ? $data->check_no : "",
            "check_details" => isset($data->check_details) ? $data->check_details : "",
            "bank" => isset($data->bank) ? $data->bank : "",
            "remarks" => isset($data->remarks) ? $data->remarks : "",
            "status" => $data->status,
            "approved_by" => $approved_by,
            "approved_date" => $approved_date,
            "or_number" => $or_number,
            "invoice_number" => $invoice
        );

        $id = $database->insert (BILLING_PAYMENTS, $insertData);
        if($id){
            $_SESSION['image-billing-payment-id'] = $id;

            calculateBillingBalance($database, $insertData);

            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Billing Payment Added successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "edit"){

        $updateData = Array (
            "franchise_id" => $data->franchise_id,
            "bill_id" => $billing[0],
            "description" => $billing[1],
            "total_amount" => $data->total_amount,
            "date_paid" => $data->date_paid,
            "check_no" => isset($data->check_no) ? $data->check_no : "",
            "check_details" => isset($data->check_details) ? $data->check_details : "",
            "bank" => isset($data->bank) ? $data->bank : "",
            "remarks" => isset($data->remarks) ? $data->remarks : "",
            "status" => $data->status,
            "approved_by" => $approved_by,
            "approved_date" => $approved_date,
            "or_number" => $or_number,
            "invoice_number" => $invoice
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (BILLING_PAYMENTS, $updateData);
        if($id){

            $_SESSION['image-billing-payment-id'] = $data->modifyId;

            calculateBillingBalance($database, $updateData);

            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Billing Payment Details modified successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "delete"){
        $updateData = Array (
            "is_deleted" => 1
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (BILLING_PAYMENTS, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Billing Payment Deleted succesfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "add"){
        saveLog($database,"{$data->formAction} Billing Payment: {$data->description}");
    }else{
        saveLog($database,"{$data->formAction} Billing Payment ID {$data->modifyId}");
    }
}

//GET METHODS
if(isset($_GET["get"])){
    $primaryKey = 'id'; //primary key
    $columns = array(
        //db = column name
        //dt = 0
        //formatter =
        array( 'db' => 'id', 'dt' => 0 ),
        array(  'db' => 'franchise_id',  
                'dt' => 1,
                'formatter' => function ($data, $row) {
                    global $database;

                    $database->where("id", $data);
                    $emp = $database->getOne(FRANCHISEE);

                    return empty($emp) ? "" : $emp['name'];
                }
        ),
        array( 'db' => 'description',  'dt' => 2 ),
        array(  'db' => 'total_amount',   
                'dt' => 3 , 
                'formatter' => function ($data, $row){
                return number_format($data);
            }
        ),
        array(  'db' => 'date_paid',   
                'dt' => 4,
                'formatter' => function ($data, $row){
                    return date("F d, Y", strtotime($data));
                }
            ),
        array( 'db' => 'remarks',   'dt' => 5 ),
        array(  'db' => 'status',   
                'dt' => 6,
                'formatter' => function ($data, $row){

                    return convertStatusColor($data);
                }
            ),
        array( 'db' => 'approved_by',   'dt' => 7 ),
        array( 'db' => 'approved_date',   'dt' => 8 ),
        array( 'db' => 'or_number',   'dt' => 9 ),
        array( 'db' => 'invoice_number',   'dt' => 10 ),
        array(  'db' => 'id',   
                'dt' => 11 ,
                'formatter' => function($data ,$row) {

                    if($row["status"] == "PENDING"){
                        return ' <div class="btn-group dropdown">
                                <button style="color:white;" aria-expanded="false" data-toggle="dropdown" class="btn bg-theme dropdown-toggle waves-effect waves-light" type="button">Take Action <span class="caret"></span></button>
                                <ul role="menu" class="dropdown-menu animated">
                                    <li><a href="#" onclick="modify('.$data.')">Modify</a></li>
                                    <li><a href="#" onclick="viewImage('.$data.')">View Image Receipt</a></li>
                                </ul>
                            </div>';
                    }else{
                        return ' <div class="btn-group dropdown">
                                <button style="color:white;" aria-expanded="false" data-toggle="dropdown" class="btn bg-theme dropdown-toggle waves-effect waves-light" type="button">Take Action <span class="caret"></span></button>
                                <ul role="menu" class="dropdown-menu animated">
                                    <li><a href="#" onclick="viewImage('.$data.')">View Image Receipt</a></li>
                                </ul>
                            </div>';
                    } 
                }
            ),
    );
    
    $condition = "is_deleted = 0";

    if(isset($_GET['status'])){
        $condition .= " and status = '{$_GET['status']}'";
    }

    echo json_encode(
        SSPCustom::simpleCustom( $_GET, $sqlSSPDetails, BILLING_PAYMENTS , $primaryKey, $columns, $condition )
    );
}

if(isset($_GET["getDetails"])){
    $id = $_GET["getDetails"];

    $database->where ("id", $id);
    $userDB = $database->getOne(BILLING_PAYMENTS);
    echo json_encode($userDB);
}

if(isset($_GET['changeStatus'])){
    $data = json_decode($_GET['changeStatus']);

    $checkApprovalAccess = checkApproval($database, $data->status);

    if($checkApprovalAccess == "NO ACCESS"){
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => "You do not have approval or reject power"
        ));

        return;
    }
    $database->where("id", $data->id);
    $billing = $database->getOne(BILLING_PAYMENTS);

    if($billing["status"] != "PENDING" ){
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => "This is already ".$billing["status"]
        ));

        return;
    }

    $database->where("id", $data->id);
    $update = $database->update(BILLING_PAYMENTS, array(
        "status" => $data->status,
        "approved_by" => $loggedUser->username,
        "approved_date" => $database->now()
    ));

    if($update){

        if($data->status == "APPROVED") {
            $database->where("id", $data->id);
            $billing = $database->getOne(BILLING_PAYMENTS);

            setBillingToPaid($database, $billing["bill_id"], "", "");
            calculateBillingBalance($database, $billing);
        }

        echo json_encode(Array (
            "type" => "success",
            "title" => "Successful!",
            "text" => "Billing Payment Updated succesfully!"
        ));
    }else{
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => $database->getLastError()
        ));
    }
}

?>