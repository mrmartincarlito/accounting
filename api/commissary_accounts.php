<?php
require_once("config.php");
require_once("logs.php");

if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    if($data->formAction == "add"){
        $insertData = Array (
            "first_name" => $data->first_name,
            "middle_name" => $data->middle_name,
            "last_name" => $data->last_name,
            "email" => $data->email,
            "username" => $data->username,
            "password" => password_hash($data->username,PASSWORD_DEFAULT),
            "is_deleted" => 0
        );

        $id = $database->insert (COMMISSARY, $insertData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Commissary Account added successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "edit"){
        // if($data->password != $data->confirm_password){
        //     echo json_encode(Array (
        //         "type" => "error",
        //         "title" => "Error!",
        //         "text" => "Passwords must match!"
        //     ));
        //     exit;
        // }

        $updateData = Array (
            "first_name" => $data->first_name,
            "middle_name" => $data->middle_name,
            "last_name" => $data->last_name,
            "email" => $data->email,
            "username" => $data->username,
            "password" => password_hash($data->username,PASSWORD_DEFAULT),
            "is_deleted" => 0
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (COMMISSARY, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Commissary details modified successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "delete"){
        $updateData = Array (
            "is_deleted" => 1
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (COMMISSARY, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Commissary Account deleted succesfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "add"){
        saveLog($database,"{$data->formAction} ACCOUNT: {$data->first_name}");
    }else{
        saveLog($database,"{$data->formAction} ACCOUNT ID {$data->modifyId}");
    }
}

//GET METHODS
if(isset($_GET["get"])){
    //SELECT *
    $database->where("is_deleted", 0);
    $commissaryData = $database->get(COMMISSARY);
    echo json_encode($commissaryData);
}

if(isset($_GET["getDetails"])){
    $id = $_GET["getDetails"];
    //SELECT WHERE
    $database->where ("id", $id);
    $commissaryData = $database->getOne(COMMISSARY);
    echo json_encode($commissaryData);
}

?>