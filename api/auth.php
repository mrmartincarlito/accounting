<?php
require_once("config.php");
require_once("logs.php");

function getLoggedUserDetails($database){
    $username = @$_SESSION["username"];

    $cols = Array ( "s.id as acc_id", 
                    "s.first_name",
                    "s.middle_name",
                    "s.last_name", 
                    "s.username", 
                    "s.role",  
                    "s.password",
                    "s.image_file",
                    "a.*",);
    $database->where ("username", $username);
    $database->join (ACCESS_LEVELS." a", "s.role=a.ID", "LEFT");
    $userDB = $database->getOne(ACCOUNTS." s", $cols);
    return json_encode($userDB);
}

function setLoggedUser($username){
    $_SESSION["username"] = $username;
}

if(isset($_GET["getLoggedUserDetails"])){
    echo getLoggedUserDetails($database);
}

if(isset($_POST["data"]) && preg_match('/auth.php/', $actual_link)) {
    $postData = json_decode($_POST["data"]);
    $action = $postData->action;
    $data = json_decode($postData->data);

    if($action == "login"){
        $database->where("is_deleted", 0);
        $database->where ("username", $data->username);
        $userDB = $database->getOne (ACCOUNTS);

        if (empty($userDB)) {
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => "You do not have an account to access this system"
            ));
            return;
        } 

        if($userDB['username'] == $data->username && password_verify($data->password,$userDB['password'])){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Logged in succesfully!"
            ));
            setLoggedUser($userDB['username']);
            saveLog($database,"Logged in");
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => "Wrong username or password!"
            ));
        }
    }
}



?>