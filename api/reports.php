<?php
require_once("config.php");
require_once("logs.php");

$loggedUser = json_decode(getLoggedUserDetails($database));
$checkAccess = checkAccessReport($loggedUser->role);

if (isset($_GET['payroll'])) {

    if ($checkAccess["access_report_payroll"] == "0") {
        $response["caption"] = "<h1 class='text-danger'>You do not have permission to access Payroll Report</h1>";
        $response["columns"] = array();
        $response["rows"] = array();
        $response["others"] = array();

        echo json_encode($response);
        return;
    }

    $data = json_decode($_GET['payroll']);
    $response = array();
    $response["caption"] = "<h4>Payroll Reports</h4><h5>Date From : {$data->data->dateFrom} - {$data->data->dateTo}</h5>";
    //th columns
    $columns = array(
        "PAYROLL NO", "CUT OFF DATE", "RELEASE DATE", "ADDED BY", "STATUS", "REMARKS", "TOTAL AMOUNT"
    );

    $response["columns"] = $columns;

    $database->where("is_deleted", 0);
    $database->where("cut_off_date", array($data->data->dateFrom, $data->data->dateTo), "BETWEEN");
    $payroll = $database->get(PAYROLL);

    $response["rows"] = $payroll;

    $overAll = 0;
    foreach ($payroll as $p) {
        $overAll = $overAll + $p["total_amount"];
    }

    $response["others"] = array(
        array(
            "name" => "OVERALL TOTAL AMOUNT",
            "value" => $overAll,
            "is_numeric" => "true"
        )
    );

    echo json_encode($response);
}

if (isset($_GET["purchase_order"])) {

    if ($checkAccess["access_report_po"] == "0") {
        $response["caption"] = "<h1 class='text-danger'>You do not have permission to access Purchase Order Report</h1>";
        $response["columns"] = array();
        $response["rows"] = array();
        $response["others"] = array();

        echo json_encode($response);
        return;
    }


    $data = json_decode($_GET['purchase_order']);
    $response = array();
    $response["caption"] = "<h4>Purchase Order Reports</h4><h5>Date From : {$data->data->dateFrom} - {$data->data->dateTo}</h5>";
    //th columns
    $columns = array(
        "PO #", "DATE PO", "SUPPLIER", "PRODUCT", "COST", "QTY", "TOTAL AMOUNT", "STATUS", "ADDED BY"
    );
    $response["columns"] = $columns;

    if (!(empty($data->data->dateFrom) && empty($data->data->dateTo))) {
        $database->where("date_po", array($data->data->dateFrom, $data->data->dateTo), "BETWEEN");
    }

    if (!empty($data->data->supplier_id)) {
        $database->where("supplier_id", $data->data->supplier_id);
    }

    $database->where("is_deleted", 0);
    $rows = $database->get(PURCHASE_ORDER);

    $poWithItems = array();

    foreach ($rows as $row) {

        $database->where("purchase_order_id", $row["id"]);
        $items = $database->get(PURCHASE_ORDER_ITEMS);

        foreach ($items as $item) {
            $poWithItem = array(
                "id" => $row["id"],
                "po_number" => $row["po_number"],
                "date_po" => $row["date_po"],
                "supplier" => getSupplier($row["supplier_id"])["supplier_name"],
                "status" => $row["status"],
                "added_by" => $row["added_by"],
                "product_id" => $item["product_id"],
                "product" => getProduct($item["product_id"])["description"],
                "cost" => $item["cost"],
                "qty" => $item["qty"],
                "item_total" => $item["total_amount"]
            );

            array_push($poWithItems, $poWithItem);
        }
    }

    $responseWithProductCondition = array();

    foreach ($poWithItems as $poWithItem) {
        if ($data->data->product_id == $poWithItem["product_id"]) {
            array_push($responseWithProductCondition, $poWithItem);
        }
    }

    $response["rows"] = (!empty($data->data->product_id)) ? $responseWithProductCondition : $poWithItems;

    $overAll = 0;
    foreach ((!empty($data->data->product_id)) ? $responseWithProductCondition : $poWithItems as $compute) {
        $overAll = $overAll + $compute["item_total"];
    }

    $response["others"] =  array(
        array(
            "name" => "OVERALL TOTAL AMOUNT",
            "value" => $overAll,
            "is_numeric" => "true"
        )
    );

    echo json_encode($response);
}

if (isset($_GET['billing'])) {

    if ($checkAccess["access_report_billing"] == "0") {
        $response["caption"] = "<h1 class='text-danger'>You do not have permission to access Franchise Billing Report</h1>";
        $response["columns"] = array();
        $response["rows"] = array();
        $response["others"] = array();

        echo json_encode($response);
        return;
    }

    $data = json_decode($_GET['billing']);
    $response = array();
    $response["caption"] = "<h4>Billing Payment Reports</h4><h5>Date From : {$data->data->dateFrom} - {$data->data->dateTo}</h5>";
    //th columns
    $columns = array(
        "FRANCHISEE", "DESCRIPTION", "DATE PAID", "CHECK NO", "BANK", "REMARKS", "STATUS", "APPROVED BY", "TOTAL AMOUNT"
    );

    if (!empty($data->data->franchisee)) {
        $database->where("franchise_id", $data->data->franchisee);
    }

    $database->where("status", "APPROVED");
    $database->where("is_deleted", 0);
    $database->where("date_paid", array($data->data->dateFrom, $data->data->dateTo), "BETWEEN");
    $billing = $database->get(BILLING_PAYMENTS);

    $parsedBills = array();

    foreach ($billing as $bill) {
        $parsedBill = array(
            "franchise" => getFranchise($bill["franchise_id"])["name"],
            "description" => $bill["description"],
            "date_paid" => $bill["date_paid"],
            "bank" => $bill["bank"],
            "remarks" => $bill["remarks"],
            "status" => $bill["status"],
            "approved_by" => $bill["approved_by"],
            "total_amount" => $bill["total_amount"]
        );

        array_push($parsedBills, $parsedBill);
    }

    $response["rows"] = $parsedBills;

    $response["columns"] = $columns;

    $overAll = 0;
    foreach ($billing as $p) {
        $overAll = $overAll + $p["total_amount"];
    }


    $response["others"] = array(

        // array(
        //     "name" => "CURRENT BILLING BALANCE",
        //     "value" => $overAll,
        //     "is_numeric" => "true"
        // ),
        array(
            "name" => "OVERALL TOTAL PAYMENTS",
            "value" => $overAll,
            "is_numeric" => "true"
        ),
        // array(
        //     "name" => "OVERALL EXISTING BALANCE",
        //     "value" => $overAll,
        //     "is_numeric" => "true"
        // ),
    );

    echo json_encode($response);
}

if (isset($_GET['order'])) {

    if ($checkAccess["access_report_orders"] == "0") {
        $response["caption"] = "<h1 class='text-danger'>You do not have permission to access Franchise Order Report</h1>";
        $response["columns"] = array();
        $response["rows"] = array();
        $response["others"] = array();

        echo json_encode($response);
        return;
    }

    $data = json_decode($_GET['order']);
    $response = array();
    $response["caption"] = "<h4>Order Reports</h4><h5>Date From : {$data->data->dateFrom} - {$data->data->dateTo}</h5>";
    //th columns
    $columns = array(
        "FRANCHISEE", "DATE PAID", "CHECK NO", "BANK", "REMARKS", "STATUS", "APPROVED BY", "TOTAL AMOUNT"
    );

    if (!empty($data->data->franchisee)) {
        $database->where("franchise_id", $data->data->franchisee);
    }

    $database->where("status", "APPROVED");
    $database->where("is_deleted", 0);
    $database->where("date_paid", array($data->data->dateFrom, $data->data->dateTo), "BETWEEN");
    $orders = $database->get(ORDER_PAYMENTS);

    $parsedOrders = array();

    foreach ($orders as $order) {
        $parsedOrder = array(
            "franchise" => getFranchise($order["franchise_id"])["name"],
            "date_paid" => $order["date_paid"],
            "check_no" => $order["check_no"],
            "bank" => $order["bank"],
            "remarks" => $order["remarks"],
            "status" => $order["status"],
            "approved_by" => $order["approved_by"],
            "total_amount" => $order["total_amount"]
        );

        array_push($parsedOrders, $parsedOrder);
    }


    $response["rows"] = $parsedOrders;

    $response["columns"] = $columns;

    $overAll = 0;
    foreach ($orders as $p) {
        $overAll = $overAll + $p["total_amount"];
    }

    $response["others"] = array(
        // array(
        //     "name" => "CURRENT BILLING BALANCE",
        //     "value" => $overAll,
        //     "is_numeric" => "true"
        // ),
        array(
            "name" => "OVERALL TOTAL PAYMENTS",
            "value" => $overAll,
            "is_numeric" => "true"
        ),
        // array(
        //     "name" => "OVERALL EXISTING BALANCE",
        //     "value" => $overAll,
        //     "is_numeric" => "true"
        // ),
    );

    echo json_encode($response);
}

if (isset($_GET['expenses_detailed'])) {

    if ($checkAccess["access_report_detailed_expense"] == "0") {
        $response["caption"] = "<h1 class='text-danger'>You do not have permission to access Detailed Expense Report</h1>";
        $response["columns"] = array();
        $response["rows"] = array();
        $response["others"] = array();

        echo json_encode($response);
        return;
    }

    $data = json_decode($_GET['expenses_detailed']);
    $response = array();
    $response["caption"] = "<h4>Expenses Detailed Report</h4><h5>Date From : {$data->data->dateFrom} - {$data->data->dateTo}</h5>";
    //th columns
    $columns = array(
        "TYPE", "CATEGORY", "SUB CATEGORY" , "TOTAL AMOUNT"
    );

    $response["columns"] = $columns;

    $expenses = $database->rawQuery("
        SELECT *, SUM(total_amount) as amount from " .EXPENSES. "
        WHERE is_deleted = 0 and date_paid BETWEEN '{$data->data->dateFrom}' and '{$data->data->dateTo}'
        group by expense_type, category, sub_category
        order by expense_type asc
    ");

        //disposal expenses
    $disposalExpenses = $database->rawQuery("
        SELECT * from " . PRODUCT_DISPOSAL . " pd 
        JOIN  " . PRODUCT_DISPOSAL_ITEMS . " pdi on pd.id = pdi.disposal_id
        WHERE is_deleted = 0 and DATE(date_time) BETWEEN '{$data->data->dateFrom}' and '{$data->data->dateTo}'
    ");

    if (!empty($disposalExpenses)) {

        foreach ($disposalExpenses as $disposalExpense) {
            array_push($expenses, array(
                "amount" => $disposalExpense['total_amount'],
                "total_amount" => $disposalExpense['total_amount'],
                "sub_category" => $disposalExpense["code"],
                "category" => "PRODUCT DISPOSAL",
                "expense_type" => "Operational"
            ));
        }
    }

    $response["rows"] = $expenses;

    $overAll = 0;
    foreach ($expenses as $p) {
        $overAll = $overAll + $p["amount"];
    }

    $response["others"] = array(
        array(
            "name" => "OVERALL TOTAL AMOUNT",
            "value" => $overAll,
            "is_numeric" => "true"
        )
    );

    echo json_encode($response);
}

if (isset($_GET['expenses'])) {

    if ($checkAccess["access_report_expense_summary"] == "0") {
        $response["caption"] = "<h1 class='text-danger'>You do not have permission to access Expense Summary Report</h1>";
        $response["columns"] = array();
        $response["rows"] = array();
        $response["others"] = array();

        echo json_encode($response);
        return;
    }

    $data = json_decode($_GET['expenses']);
    $response = array();
    $response["caption"] = "<h4>Expenses Summary Report</h4><h5>Date From : {$data->data->dateFrom} - {$data->data->dateTo}</h5>";
    //th columns
    $columns = array(
        "TYPE", "CATEGORY", "TOTAL AMOUNT"
    );

    $response["columns"] = $columns;

    $expenses = $database->rawQuery("
        SELECT SUM(total_amount) as total_amount, category, expense_type, date_paid from " .EXPENSES. "
        WHERE is_deleted = 0 and date_paid BETWEEN '{$data->data->dateFrom}' and '{$data->data->dateTo}'
        group by category
        order by expense_type asc
    ");

    //disposal expenses
    $disposalExpense = $database->rawQuery("
        SELECT SUM(pdi.total_amount) as total_amount from " . PRODUCT_DISPOSAL . " pd 
        JOIN  " . PRODUCT_DISPOSAL_ITEMS . " pdi on pd.id = pdi.disposal_id
        WHERE is_deleted = 0 and DATE(date_time) BETWEEN '{$data->data->dateFrom}' and '{$data->data->dateTo}'
    ");

    if (!empty($disposalExpense)) {
        array_push($expenses, array(
            "total_amount" => $disposalExpense[0]['total_amount'],
            "category" => "PRODUCT DISPOSAL",
            "expense_type" => "Operational"
        ));
    }

    $response["rows"] = $expenses;

    $overAll = 0;
    foreach ($expenses as $p) {
        $overAll = $overAll + $p["total_amount"];
    }

    $response["others"] = array(
        array(
            "name" => "OVERALL TOTAL AMOUNT",
            "value" => $overAll,
            "is_numeric" => "true"
        )
    );

    echo json_encode($response);
}

if (isset($_GET['franchise'])) {

    if ($checkAccess["access_report_renewal_logs"] == "0") {
        $response["caption"] = "<h1 class='text-danger'>You do not have permission to access Franchisee Renewal Log Report</h1>";
        $response["columns"] = array();
        $response["rows"] = array();
        $response["others"] = array();

        echo json_encode($response);
        return;
    }

    $data = json_decode($_GET['franchise']);
    $response = array();
    $response["caption"] = "<h4>Franchise Renewal Reports</h4><h5>Date From : {$data->data->dateFrom} - {$data->data->dateTo}</h5>";
    //th columns
    $columns = array(
        "FRANCHISEE", "EXPIRY DATE", "RENEWED DATE", "NEW EXPIRY DATE", "APPROVED BY", "DATE TIME"
    );

    $response["columns"] = $columns;

    if (!empty($data->data->franchisee)) {
        $database->where("franchise_id", $data->data->franchisee);
    }

    $renewals = $database->get(RENEWAL_LOG);

    $parsedRenews = array();

    foreach ($renewals as $renewal) {
        $parsedRenew = array(
            "franchise" => getFranchise($renewal["franchise_id"])["name"],
            "expiry_date" => $renewal["expiry_date"],
            "renewed_date" => $renewal["renewed_date"],
            "new_expiry_date" => $renewal["new_expiry_date"],
            "approved_by" => $renewal["approved_by"],
            "date_time" => $renewal["date_time"]
        );

        array_push($parsedRenews, $parsedRenew);
    }

    $response["rows"] = $parsedRenews;

    $response["others"] = array();

    echo json_encode($response);
}

if (isset($_GET['statement_of_account'])) {

    if ($checkAccess["access_report_soa"] == "0") {
        $response["caption"] = "<h1 class='text-danger'>You do not have permission to access Statement of Account Report</h1>";
        $response["columns"] = array();
        $response["rows"] = array();
        $response["others"] = array();

        echo json_encode($response);
        return;
    }

    $data = json_decode($_GET['statement_of_account']);
    $response = array();
    $from = date("F j, Y", strtotime($data->data->dateFrom));
    $to = date("F j, Y", strtotime($data->data->dateTo));

    //th columns
    $columns = array(
        "DATE", "REF / OR", "ORDER AMOUNT", "ORDER PAYMENTS", "BILLING AMOUNT", "BILLING PAYMENTS"
    );
    $response["columns"] = $columns;

    //get order first
    if (!empty($data->data->franchisee)) {
        $database->where("franchisee_id", $data->data->franchisee);
    }
    $database->where("status", array("PENDING", "REJECTED"), "NOT IN");
    $database->where("date_ordered", array($data->data->dateFrom, $data->data->dateTo), "BETWEEN");
    $orders = $database->get(ORDERS);

    $order_amount = 0;
    $values = array();

    foreach ($orders as $order) {
        $order_amount = $order_amount + $order["total_amount"];

        array_push($values, array(
            "date" => date("F j, Y", strtotime($order["date_ordered"])),
            "ref_no" => $order["order_ref"],
            "order_amount" =>  number_format($order["total_amount"], 2, ".", ","),
            "order_payments" => "",
            "billing_amount" => "",
            "billing_payments" => ""
        ));
    }

    $franchisee = getFranchise($data->data->franchisee)['name'];
    $response["caption"] = "<h3>Statement of Account</h3><h4>Franchisee : $franchisee</h4><h5>Date From : {$from} - {$to}</h5>";

    //get order payments
    if (!empty($data->data->franchisee)) {
        $database->where("franchise_id", $data->data->franchisee);
    }
    $database->where("status", "APPROVED");
    $database->where("date_paid", array($data->data->dateFrom, $data->data->dateTo), "BETWEEN");
    $orders = $database->get(ORDER_PAYMENTS);
    $order_payment = 0;

    foreach ($orders as $order) {
        $order_payment = $order_payment + $order["total_amount"];
        array_push($values, array(
            "date" => date("F j, Y", strtotime($order["date_paid"])),
            "ref_no" => $order["or_number"],
            "order_amount" => "",
            "order_payments" =>  number_format($order["total_amount"], 2, ".", ","),
            "billing_amount" => "",
            "billing_payments" => ""
        ));
    }

    //get billing 
    if (!empty($data->data->franchisee)) {
        $database->where("franchise_id", $data->data->franchisee);
    }
    $database->where("is_deleted", 0);
    $database->where("date_time", array($data->data->dateFrom, $data->data->dateTo), "BETWEEN");
    $orders = $database->get(BILLING_FRANCHISE);
    $billing = 0;
    foreach ($orders as $order) {
        $billing = $billing + $order["total_amount"];
        array_push($values, array(
            "date" => date("F j, Y", strtotime($order["date_time"])),
            "ref_no" => $order["or_number"],
            "order_amount" => "",
            "order_payments" => "",
            "billing_amount" =>  number_format($order["total_amount"], 2, ".", ","),
            "billing_payments" => ""
        ));
    }

    //get billing payments
    if (!empty($data->data->franchisee)) {
        $database->where("franchise_id", $data->data->franchisee);
    }
    $database->where("date_paid", array($data->data->dateFrom, $data->data->dateTo), "BETWEEN");
    $database->where("status", "APPROVED");
    $orders = $database->get(BILLING_PAYMENTS);
    $billing_payment = 0;
    foreach ($orders as $order) {
        $billing_payment = $billing_payment + $order["total_amount"];
        array_push($values, array(
            "date" => date("F j, Y", strtotime($order["date_paid"])),
            "ref_no" => $order["or_number"],
            "order_amount" => "",
            "order_payments" => "",
            "billing_amount" => "",
            "billing_payments" => number_format($order["total_amount"], 2, ".", ",")
        ));
    }

    //sort by dates
    usort($values, 'comparedates');

    //add all column total
    array_push($values, array(
        "date" => "",
        "ref_no" => "",
        "order_amount" => number_format($order_amount, 2, ".", ","),
        "order_payments" => number_format($order_payment, 2, ".", ","),
        "billing_amount" => number_format($billing, 2, ".", ","),
        "billing_payments" => number_format($billing_payment, 2, ".", ",")
    ));

    $response["rows"] = $values;

    $orderBalance = $order_amount - $order_payment;
    $billingBalance = $billing - $billing_payment;
    $totalBalance = $orderBalance + $billingBalance;

    $response["others"] = array(
        array(
            "name" => "Order Balance",
            "value" => number_format($orderBalance, 2, ".", ","),
            "is_numeric" => "true"
        ),
        array(
            "name" => "Billing Balance",
            "value" => number_format($billingBalance, 2, ".", ","),
            "is_numeric" => "true"
        ),
        array(
            "name" => "Over all Balance Due",
            "value" => number_format($totalBalance, 2, ".", ","),
            "is_numeric" => "true"
        )
    );

    echo json_encode($response);
}

if (isset($_GET['sales'])) {

    if ($checkAccess["access_report_sales"] == "0") {
        $response["caption"] = "<h1 class='text-danger'>You do not have permission to access Sales Report</h1>";
        $response["columns"] = array();
        $response["rows"] = array();
        $response["others"] = array();

        echo json_encode($response);
        return;
    }

    $data = json_decode($_GET['sales']);
    $response = array();
    $from = date("F j, Y", strtotime($data->data->dateFrom));
    $to = date("F j, Y", strtotime($data->data->dateTo));

    if ($data->data->sales_category == "1") { //1 Per Branch
        $response["caption"] = "<h3>Report of Sales</h3><h4>Per Branch</h4><h5>Date From : {$from} - {$to}</h5>";
        //th columns
        $columns = array(
            "BRANCH NAME", "SALES"
        );
        $response["columns"] = $columns;

        $row = array();
        $overAll = 0;
        foreach (getBranches() as $branch) {
            $database->where("branch_id", $branch["id"]);
            $franchisees = $database->get(FRANCHISEE, null, array("id"));

            $franchisee = array();
            if (empty($franchisees)) {
                continue;
            } else {
                foreach ($franchisees as $f) {
                    array_push($franchisee, $f["id"]);
                }
            }

            $database->where("date_ordered", array($data->data->dateFrom, $data->data->dateTo), "BETWEEN");
            $database->where("status", array("PENDING", "REJECTED"), "NOT IN");
            $database->where("franchisee_id", $franchisee, "IN");
            $orders = $database->get(ORDERS);

            $totalAmount = 0;
            foreach ($orders as $order) {
                $totalAmount = $totalAmount + $order["total_amount"];
            }

            $overAll = $overAll + $totalAmount;
            array_push($row, array(
                "branch" => $branch["branch_code"] . " - " . $branch["branch_name"],
                "total_amount" => number_format($totalAmount, 2, ".", ","),
            ));
        }

        $response["rows"] = $row;
        $response["others"] = array(
            array(
                "name" => "Total Sales",
                "value" => number_format($overAll, 2, ".", ","),
                "is_numeric" => "true"
            )
        );
    }

    if ($data->data->sales_category == "2") { //2 Per Franchise with Invoices
        $response["caption"] = "<h3>Report of Sales</h3><h4>Per Franchise</h4><h5>Date From : {$from} - {$to}</h5>";
        //th columns
        $columns = array(
            "FRANCHISEE", "DATE", "OR / INVOICE", "SALES"
        );
        $response["columns"] = $columns;

        $values = array();
        //get order payments

        if (!empty($data->data->franchisee)) { 
            $response["caption"] = "<h3>Report of Sales</h3><h4>Franchisee : <b>".getFranchise($data->data->franchisee)["name"]."</b></h4><h5>Date From : {$from} - {$to}</h5>";
            
            $database->where("franchise_id", $data->data->franchisee);
        }

        $database->where("status", "APPROVED");
        $database->where("date_paid", array($data->data->dateFrom, $data->data->dateTo), "BETWEEN");
        $orders = $database->get(ORDER_PAYMENTS);
        $order_payment = 0;

        foreach ($orders as $order) {
            $order_payment = $order_payment + $order["total_amount"];
            array_push($values, array(
                "franchisee" => getFranchise($order['franchise_id'])["name"],
                "date" => date("F j, Y", strtotime($order["date_paid"])),
                "or_invoice" => $order["or_number"] . "/" . $order["invoice_number"],
                "amount" =>  number_format($order["total_amount"], 2, ".", ","),
            ));
        }

        $response["rows"] = $values;

        $response["others"] = array(
            array(
                "name" => "Total Sales",
                "value" => number_format($order_payment, 2, ".", ","),
                "is_numeric" => "true"
            )
        );
    }

    if ($data->data->sales_category == "3") { //3 Per Batch No Distribution
        $response["caption"] = "<h3>Report of Sales</h3><h4>Per Batch Distribution</h4><h5>Date From : {$from} - {$to}</h5>";
        //th columns
        $columns = array(
            "BATCH NO", "PRODUCT NAME", "ORDER REF", "QTY SOLD", "PRICE", "TOTAL"
        );
        $response["columns"] = $columns;

        $values = array();

        $database->where("date_ordered", array($data->data->dateFrom, $data->data->dateTo), "BETWEEN");
        $database->where("status", array("PENDING", "REJECTED"), "NOT IN");
        $orders = $database->get(ORDERS);
        $totalAmount = 0;
        foreach ($orders as $order) {

            $products = $database->rawQuery("SELECT *, count(item_id) as qty 
                                            FROM " . PRODUCT_ITEMIZE . "
                                            WHERE order_ref = '" . $order['order_ref'] . "' 
                                            group by batch_no order by batch_no asc");

            foreach ($products as $product) {

                $orderItem = getOrderItemsByRefnoAndItemId($product["order_ref"], $product["item_id"]);
                $totalAmount = $totalAmount + $orderItem["total_amount"];
                array_push($values, array(
                    "batch_no" => $product["ref_no"],
                    "product" => getProduct($product["item_id"])["description"],
                    "order_ref" => $product["order_ref"],
                    "qty" => $product["qty"],
                    "price" => number_format($orderItem["price"], 2, ".", ","),
                    "sales" => number_format($orderItem["total_amount"], 2, ".", ",")
                ));
            }
        }
        usort($values, 'comparebatchno');
        $response["rows"] = $values;

        $response["others"] = array(
            array(
                "name" => "Total Sales",
                "value" => number_format($totalAmount, 2, ".", ","),
                "is_numeric" => "true"
            )
        );
    }

    if ($data->data->sales_category == "4") { //4 Delivery Fee
        $response["caption"] = "<h3>Report of Sales</h3><h4>Per Delivery Fee</h4><h5>Date From : {$from} - {$to}</h5>";
        //th columns
        $columns = array(
            "FRANCHISEE", "ORDER REF", "DELIVERY FEE"
        );
        $response["columns"] = $columns;

        $values = array();

        $database->where("date_ordered", array($data->data->dateFrom, $data->data->dateTo), "BETWEEN");
        $database->where("status", array("PENDING", "REJECTED"), "NOT IN");
        $orders = $database->get(ORDERS);

        $totalAmount = 0;
        foreach ($orders as $order) {
            if ($order['delivery_charge'] > 0) {
                $totalAmount = $totalAmount + $order["delivery_charge"];
                array_push($values, array(
                    "franchisee" => getFranchise($order['franchisee_id'])['name'],
                    "order_ref" => $order['order_ref'],
                    "amount" => number_format($order["delivery_charge"], 2, ".", ",")
                ));
            }
        }

        $response["rows"] = $values;

        $response["others"] = array(
            array(
                "name" => "Total Delivery Fees",
                "value" => number_format($totalAmount, 2, ".", ","),
                "is_numeric" => "true"
            )
        );

    }

    if ($data->data->sales_category == "5") { //4 Per Product
        $response["caption"] = "<h3>Report of Sales</h3><h4>Per Product</h4><h5>Date From : {$from} - {$to}</h5>";
        //th columns
        $columns = array(
            "PRODUCT NAME", "QTY SOLD", "TOTAL"
        );
        $response["columns"] = $columns;

        $franchiseeId = "";

        if (!empty($data->data->franchisee)) { 
            $response["caption"] = "<h3>Report of Sales</h3><h4>Franchisee : <b>".getFranchise($data->data->franchisee)["name"]."</b></h4><h5>Date From : {$from} - {$to}</h5>";
            $franchiseeId = $data->data->franchisee;
        }

        $values = array();

        $products = getAllProducts();
        $totalAmount = 0;
        foreach ($products as $product) {

            if ($franchiseeId != "") {
                $database->where("franchisee_id", $franchiseeId);
            }

            $database->where("date_ordered", array($data->data->dateFrom, $data->data->dateTo), "BETWEEN");
            $database->where("status", array("PENDING", "REJECTED"), "NOT IN");
            $orders = $database->get(ORDERS);

            $orderIds = array("0");
            foreach ($orders as $order) {
                array_push($orderIds, $order["id"]);
            }
            
            //with product id selected
            $whereProductId = "";
            if (!empty($data->data->product_id)) {
                $whereProductId = "and item_id = " . $data->data->product_id;
            }

            $orderItems = $database->rawQuery("SELECT 
                        sum(rec_qty + qty) as qty, 
                        sum(total_amount) as total_amount 
                        FROM " . ORDER_ITEMS . " WHERE item_id = " . $product["id"] . " 
                        and order_id in(" . implode(",", $orderIds) . ") $whereProductId");

            foreach ($orderItems as $order) {
                if ($order["qty"] != null) {
                    $totalAmount = $totalAmount + $order["total_amount"];
                    array_push($values, array(
                        "product" => $product["description"],
                        "qty" => $order["qty"],
                        "sales" => number_format($order["total_amount"], 2, ".", ",")
                    ));
                }
            }
        }

        $response["rows"] = $values;
        $response["others"] = array(
            array(
                "name" => "Total Sales",
                "value" => number_format($totalAmount, 2, ".", ","),
                "is_numeric" => "true"
            )
        );
    }

    echo json_encode($response);
}

if (isset($_GET['return_goods'])) {
    if ($checkAccess["access_report_returns"] == "0") {
        $response["caption"] = "<h1 class='text-danger'>You do not have permission to access Return Goods Report</h1>";
        $response["columns"] = array();
        $response["rows"] = array();
        $response["others"] = array();

        echo json_encode($response);
        return;
    }

    $data = json_decode($_GET['return_goods']);
    $response = array();
    $from = date("F j, Y", strtotime($data->data->dateFrom));
    $to = date("F j, Y", strtotime($data->data->dateTo));
    $condition = $data->data->item_condition;

    $response["caption"] = "<h3>Return Goods Report</h3><h4>Item Condition: $condition</h4><h5>Date From : {$from} - {$to}</h5>";
    //th columns
    $columns = array(
        "FRANCHISEE", "ORDER REF", "RETURN REF" , "CONDITION" , "ITEM", "RETURN QTY" , "COST" , "AMOUNT"
    );
    $response["columns"] = $columns;

    $values = array();

    $database->where("DATE(date_time)", array($data->data->dateFrom, $data->data->dateTo), "BETWEEN");
    $database->where("status", array("PENDING", "REJECTED"), "NOT IN");
    $database->where("item_condition", $condition);
    $returns = $database->get(ORDER_RETURNS);

    $totalAmount = 0;
    foreach ($returns as $return) {
        $database->where("return_ref", $return["return_ref_no"]);
        $items = $database->get(ORDER_RETURN_ITEMS);

        foreach ($items as $item ) {
            $totalAmount = $totalAmount + $item["total_amount"];
            array_push($values, array(
                "franchisee" => getFranchise($return["franchisee_id"])["name"],
                "order_ref" => $return["order_ref_no"],
                "return_ref" => $item["return_ref"],
                "condition" => $return["item_condition"],
                "item" => getProduct($item["item_id"])["description"],
                "qty" => $item["return_qty"],
                "price" => number_format($item["cost"], 2, ".", ","),
                "amount" => number_format($item["total_amount"], 2, ".", ",")
            ));
        }
    }


    $response["rows"] = $values;
    $response["others"] = array(
        array(
            "name" => "Total Return Amount",
            "value" => number_format($totalAmount, 2, ".", ","),
            "is_numeric" => "true"
        )
    );

    echo json_encode($response);

}

if (isset($_GET['deliveryCharge'])) {
    $data = json_decode($_GET['deliveryCharge']);
    $response = array();
    $from = date("F j, Y", strtotime($data->data->dateFrom));
    $to = date("F j, Y", strtotime($data->data->dateTo));
    $condition = $data->data->item_condition;

    $response["caption"] = "<h3>Delivery Charge Report</h3><h4>Date From : {$from} - {$to}</h4>";
    //th columns
    $columns = array(
        "ORDER REF","DATE ORDERED",  "DELIVERY CHARGE"
    );
    $response["columns"] = $columns;

    $values = array();

    $database->where("DATE(date_ordered)", array($data->data->dateFrom, $data->data->dateTo), "BETWEEN");
    $database->where("status", array("PENDING", "REJECTED", "CANCELLED"), "NOT IN");
    $orders = $database->get(ORDERS);

    $totalDeliveryCharge = 0;

    foreach($orders as $order) {
        $totalDeliveryCharge = $totalDeliveryCharge + $order["delivery_charge"];
        array_push($values, array(
            "order_ref" => $order['order_ref'],
            "date_ordered" => $order["date_ordered"],
            "delivery_charge" => number_format($order["delivery_charge"], 2, ".", ",")
        ));
    }

    $response["rows"] = $values;
    $response["others"] = array(
        array(
            "name" => "Total Delivery Charge",
            "value" => number_format($totalDeliveryCharge, 2, ".", ","),
            "is_numeric" => "true"
        )
    );

    echo json_encode($response);
}

function comparedates($object1, $object2)
{
    return strtotime($object1['date']) > strtotime($object2['date']);
}

function comparebatchno($object1, $object2)
{
    return $object1['batch_no'] > $object2['batch_no'];
}
