<?php
require_once("config.php");
require_once("logs.php");

$loggedUser = json_decode(getLoggedUserDetails($database));

if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    $checkApprovalAccess = checkApproval($database, $data->status);

    if($checkApprovalAccess == "NO ACCESS"){
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => "You do not have approval or reject power, Please change your status to PENDING to proceed"
        ));

        return;
    }

    if($data->formAction == "add"){

        $payrollNo = generateRefno($database, "1", PAYROLL, "payroll_no", "P-");

        $insertData = Array (
            "payroll_no" => $payrollNo,
            "cut_off_date" => $data->cut_off_date,
            "release_date" => $data->release_date,
            "added_by" =>  $loggedUser->username,
            "status" => $data->status,
            "remarks" => isset($data->remarks) ? $data->remarks : "",
            "total_amount" => $data->total_amount
        );

        $id = $database->insert (PAYROLL, $insertData);
        if($id){
            $_SESSION['image-payroll-id'] = $id;
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Payroll Added successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "edit"){

        $updateData = Array (
            "cut_off_date" => $data->cut_off_date,
            "release_date" => $data->release_date,
            "added_by" =>  $loggedUser->username,
            "status" => $data->status,
            "remarks" => isset($data->remarks) ? $data->remarks : "",
            "total_amount" => $data->total_amount
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (PAYROLL, $updateData);
        if($id){
            $_SESSION['image-payroll-id'] = $data->modifyId;
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Payroll Details modified successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "delete"){
        $updateData = Array (
            "is_deleted" => 1
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (PAYROLL, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Payroll Deleted succesfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "add"){
        saveLog($database,"{$data->formAction} Payroll: {$payrollNo}");
    }else{
        saveLog($database,"{$data->formAction} Payroll ID {$data->modifyId}");
    }
}

if(isset($_POST['getAttendanceLogsByEmployee'])){
    $data = json_decode($_POST['getAttendanceLogsByEmployee']);

    $response = array();

    //get payroll information
    $database->where("id", $data->payroll_id);
    $payrollInfo = $database->getOne(PAYROLL);

    //get employee information
    $database->where("id", $data->emp_id);
    $empInfo = $database->getOne(EMP_TABLE);

    //get employee attendance
    $database->where("emp_id", $data->emp_id);
    $database->where("work_date", array($payrollInfo["cut_off_date"], $payrollInfo["release_date"]), "BETWEEN");
    $attendanceLogs = $database->get(ATTENDANCE_TABLE);

    $totalLogs = array(
        "total_reg_min" => 0,
        "leg_hol_ot" => 0,
        "spc_hol_ot" => 0,
        "spc_hol" => 0,
        "leg_hol" => 0,
        "total_hr" => 0
    );

    //compute for total number of logs
    foreach($attendanceLogs as $attendanceLog){
        $totalLogs["total_reg_min"] = $totalLogs["total_reg_min"] + $attendanceLog['total_minutes'];
        $totalLogs["leg_hol_ot"] = $totalLogs["leg_hol_ot"] + $attendanceLog['regular_legal_ot'];
        $totalLogs["spc_hol_ot"] = $totalLogs["spc_hol_ot"] + $attendanceLog['special_ot'];
        $totalLogs["spc_hol"] = $totalLogs["spc_hol"] + $attendanceLog['special_holiday'];
        $totalLogs["leg_hol"] = $totalLogs["leg_hol"] + $attendanceLog['legal_holiday'];
        $totalLogs["total_hr"] = $totalLogs["total_hr"] + $attendanceLog['total_hours'];
    }

    //iterate response json
    $response["emp_id"] = $empInfo["id"];
    $response["employment_type"] = $empInfo["employment_type"];
    $response["rate"] = $empInfo["rate"];
    $response["payroll_id"] = $payrollInfo["id"];
    $response["payroll_no"] = $payrollInfo["payroll_no"];
    $response["cut_off_date"] = $payrollInfo["cut_off_date"];
    $response["release_date"] = $payrollInfo["release_date"];
    $response["attendance_logs"] = $totalLogs;

    echo json_encode($response);
}

if(isset($_POST['payroll'])){
    $postData = json_decode($_POST["payroll"]);
    $data = json_decode($postData->data);

    $grossPay = (empty($data->base_rate) ? 0 : $data->base_rate) + 
                (empty($data->allowance) ? 0 : $data->allowance) +
                (empty($data->comm_allowance) ? 0 : $data->comm_allowance) + 
                (empty($data->ot_reg_day) ? 0 : $data->ot_reg_day) + 
                (empty($data->ot_leg_day) ? 0 : $data->ot_leg_day) + 
                (empty($data->ot_spcl_day) ? 0 : $data->ot_spcl_day) + 
                (empty($data->special_holiday) ? 0 : $data->special_holiday) +
                (empty($data->regular_holiday) ? 0 : $data->regular_holiday);

    $totalDeductions =  (empty($data->sss) ? 0 : $data->sss) + 
                        (empty($data->pag_ibig) ? 0 : $data->pag_ibig) + 
                        (empty($data->philhealth) ? 0 : $data->philhealth) + 
                        (empty($data->lates) ? 0 : $data->lates) + 
                        (empty($data->absent) ? 0 : $data->absent) +
                        (empty($data->tax_deduct) ? 0 : $data->tax_deduct) +
                        (empty($data->sss_loan) ? 0 : $data->sss_loan) +
                        (empty($data->pag_ibig_loan) ? 0 : $data->pag_ibig_loan) +
                        (empty($data->cash_advance) ? 0 : $data->cash_advance) +
                        (empty($data->short_remittance) ? 0 : $data->short_remittance);

    $response = array();
    $response["error"] = false;
    $response["message"] = "";
    $response["gross_pay"] = $grossPay;
    $response["total_deductions"] = $totalDeductions;
    $response["net_pay"] = $response["gross_pay"] - $response["total_deductions"];

    if($postData->action == "save"){
        $insertData = array();

        $insertData["gross_pay"] = $response["gross_pay"];
        $insertData["total_deductions"] = $response["total_deductions"];
        $insertData["net_pay"] = $response["net_pay"];
        $insertData["added_by"] = $loggedUser->username;

        foreach($data as $key => $value){
            //escape data not present on column
            if(($key != "formAction") && 
                ($key != "modifyId") && 
                ($key != "position") && 
                ($key != "employee_rate") &&
                ($key != "payrollEmpTable_length") &&
                ($key != "gross_pay") &&
                ($key != "total_deductions") &&
                ($key != "net_pay")){

                    if(!empty($value)){
                        $insertData[$key] = $value;
                    }
            }
        }

        //check if emp payroll already existed with this payroll id
        $database->where("payroll_id", $data->payroll_id);
        $database->where("emp_id", $data->emp_id);
        $isPresent = $database->getOne(EMP_PAYROLL);

        if(empty($isPresent)){
            //insert when not present
            $id = $database->insert(EMP_PAYROLL, $insertData);
            if(!$id){
                $response["error"] = true;
                $response["message"] = $database->getLastError();
            }
        }else{
            //update when present
            $database->where("payroll_id", $data->payroll_id);
            $database->where("emp_id", $data->emp_id);
            $id = $database->update(EMP_PAYROLL, $insertData);
            if(!$id){
                $response["error"] = true;
                $response["message"] = $database->getLastError();
            }
        }

        //update payroll table
        $database->where("payroll_id", $data->payroll_id);
        $sumEmpNetPay = $database->getOne(EMP_PAYROLL, "SUM(net_pay) as total_net_pay");

        $database->where("id", $data->payroll_id);
        $database->update(PAYROLL, array("total_amount" => $sumEmpNetPay["total_net_pay"]));
    }
    
    echo json_encode($response);
}

//GET METHODS
if(isset($_GET["get"])){
    $primaryKey = 'id';
    $columns = array(
        array( 'db' => 'id', 'dt' => 0 ),
        array( 'db' => 'payroll_no', 'dt' => 1 ),
        array( 'db' => 'cut_off_date',  'dt' => 2 ),
        array( 'db' => 'release_date',   'dt' => 3 ),
        array(  'db' => 'total_amount',   
                'dt' => 4 ,
                'formatter' => function($data, $row){
                    return number_format($data);
                }
        ),
        array( 'db' => 'added_by',   'dt' => 5 ),
        array(  'db' => 'status',   
                'dt' => 6 ,
                'formatter' => function($data, $row){
                    return convertStatusColor($data);
                }
        ),
        array(  'db' => 'image_file',   
                'dt' => 7 ,
                'formatter' => function($data, $row){
                    if(empty($data)){
                        return "NO FILE UPLOADED";
                    }else{
                        return '<a href="./api/'.$data.'" download="'.$row["payroll_no"].'">DOWNLOAD FILE</a>';
                    }
                }
        ),
        array( 'db' => 'remarks',   'dt' => 8 ),
        array(  'db' => 'id',   
                'dt' => 9 ,
                'formatter' => function($data ,$row) {

                    if($row["status"] == "PENDING"){
                        return ' <div class="btn-group dropdown">
                                <button style="color:white;" aria-expanded="false" data-toggle="dropdown" class="btn bg-theme dropdown-toggle waves-effect waves-light" type="button">Take Action <span class="caret"></span></button>
                                <ul role="menu" class="dropdown-menu animated">
                                    <li><a href="#" onclick="modify('.$data.')">Modify</a></li>
                                    <!--<li><a href="#" onclick="addEmployee('.$data.')">Add Employee Payroll</a></li>-->
                                </ul>
                            </div>';
                    }else{
                        return "";
                    }
                    
                }
            ),
    );
    
    $condition = "is_deleted = 0";
    if(isset($_GET['status'])){
        $condition .= " and status = '{$_GET['status']}'";
    }

    echo json_encode(
        SSPCustom::simpleCustom( $_GET, $sqlSSPDetails, PAYROLL , $primaryKey, $columns, $condition )
    );
}

if(isset($_GET["getDetails"])){
    $id = $_GET["getDetails"];

    $database->where ("id", $id);
    $userDB = $database->getOne(PAYROLL);
    echo json_encode($userDB);
}

if(isset($_GET["getAddedEmployeesToPayroll"])){
    $id = $_GET["getAddedEmployeesToPayroll"];

    $values = $database->rawQuery("Select e.first_name, e.middle_name, e.last_name, p.* from 
    ".EMP_PAYROLL." p right join ".EMP_TABLE." e on p.emp_id = e.id where p.payroll_id = $id ");

    echo json_encode($values);
}

if(isset($_GET['viewAddedEmployee'])){
    $id = $_GET['viewAddedEmployee'];

    $database->where("id", $id);
    $emp = $database->getOne(EMP_PAYROLL);

    echo json_encode($emp);
}

if(isset($_GET['deleteEmployeePayroll'])){
    $id = $_GET['deleteEmployeePayroll'];

    $database->where("id", $id);
    $id = $database->delete(EMP_PAYROLL);

    if($id){
        echo json_encode(Array (
            "type" => "success",
            "title" => "Successful!",
            "text" => "Employee Payroll Deleted succesfully!"
        ));
    }else{
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => $database->getLastError()
        ));
    }

}

?>