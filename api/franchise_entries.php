<?php
require_once("config.php");
require_once("logs.php");

if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    if(!isset($data->branch_id)){
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => "Please select branch"
        ));

        return;
    }

    if(!isset($data->package_type)){
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => "Please select package type"
        ));

        return;
    }

    if($data->formAction == "add"){
  
        $insertData = Array (
            "branch_id" => $data->branch_id,
            "mode_of_transaction" => $data->mode_of_transaction,
            "name" => $data->name,
            "address" => $data->address,
            "contactno" => $data->contactno,
            "birthday" => $data->birthday,
            "tin_number" => $data->tin_number,
            "package_type" => $data->package_type,
            "date_contract_signing" => $data->date_contract_signing,
            "date_branch_opening" => $data->date_branch_opening,
            "date_contract_expiry" => $data->date_contract_expiry,
            // "billing_balance" => isset($data->billing_balance) ? $data->billing_balance : 0,
            // "order_balance" => isset($data->order_balance) ? $data->order_balance : 0,
            "total_amount" => isset($data->billing_balance) && isset($data->order_balance) ? $data->billing_balance + $data->order_balance : 0,
            "last_payment_amount" => 0,
            "delivery_charge" => $data->delivery_charge,
            "special_price_tag" => $data->special_price_tag
        );

        $id = $database->insert (FRANCHISEE, $insertData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Franchisee Entry added successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "edit"){

        $updateData = Array (
            "branch_id" => $data->branch_id,
            "mode_of_transaction" => $data->mode_of_transaction,
            "name" => $data->name,
            "address" => $data->address,
            "contactno" => $data->contactno,
            "birthday" => $data->birthday,
            "tin_number" => $data->tin_number,
            "package_type" => isset($data->package_type) ? $data->package_type : "",
            "date_contract_signing" => $data->date_contract_signing,
            "date_branch_opening" => $data->date_branch_opening,
            "date_contract_expiry" => $data->date_contract_expiry,
            // "billing_balance" => isset($data->billing_balance) ? $data->billing_balance : 0,
            // "order_balance" => isset($data->order_balance) ? $data->order_balance : 0,
            "total_amount" => isset($data->billing_balance) && isset($data->order_balance) ? $data->billing_balance + $data->order_balance : 0,
            "last_payment_amount" => 0,
            "delivery_charge" => isset($data->delivery_charge) ? $data->delivery_charge : 0,
            "special_price_tag" => $data->special_price_tag
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (FRANCHISEE, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Franchisee Entry details modified successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "delete"){
        $updateData = Array (
            "is_deleted" => 1
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (FRANCHISEE, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Franchisee Entry Account deleted succesfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "add"){
        saveLog($database,"{$data->formAction} FRANCHISEE: {$data->name}");
    }else{
        saveLog($database,"{$data->formAction} FRANCHISEE ID {$data->modifyId}");
    }
}

//GET METHODS
if(isset($_GET["get"])){
    $database->where ('f.is_deleted', 0);
    $database->join ("franchise_branch b", "f.branch_id=b.id", "LEFT");
    $database->join ("cart c", "f.package_type=c.id", "LEFT");
    $franchiseData = $database->get("franchisee f", null, "f.*, b.branch_code, c.description");
    echo json_encode($franchiseData);
}

if(isset($_GET["getDetails"])){
    $id = $_GET["getDetails"];
    //SELECT WHERE
    $database->where ("id", $id);
    $franchiseData = $database->getOne(FRANCHISEE);

    echo json_encode($franchiseData);
}

if(isset($_GET['filterByBranch'])){
    $id = $_GET["getDetails"];
    //SELECT WHERE
    $database->where ("branch_id", $id);
    $franchiseData = $database->getOne(FRANCHISEE);

    echo json_encode($franchiseData);
}

?>