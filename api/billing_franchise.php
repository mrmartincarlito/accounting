<?php
require_once("config.php");
require_once("logs.php");
require_once("payments-movements.php");

$loggedUser = json_decode(getLoggedUserDetails($database));

if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    if($data->formAction == "add"){

        $insertData = Array (
            "franchise_id" => $data->franchise_id,
            "description" => $data->description,
            "total_amount" => $data->total_amount,
            "remarks" => isset($data->remarks) ? $data->remarks : "",
            "added_by" => $loggedUser->username,
            "or_number" => isset($data->or_number) ? $data->or_number : "",
            "invoice_number" => isset($data->invoice_number) ? $data->invoice_number : "",
            "date_time" => $database->now()
        );

        $id = $database->insert (BILLING_FRANCHISE, $insertData);
        if($id){
           
            addBilling($database, $id, $insertData, 'new');

            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Billing Payment Added successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "edit"){

        $updateData = Array (
            "franchise_id" => $data->franchise_id,
            "description" => $data->description,
            "total_amount" => $data->total_amount,
            "remarks" => isset($data->remarks) ? $data->remarks : "",
            "added_by" => $loggedUser->username,
            "or_number" => isset($data->or_number) ? $data->or_number : "",
            "invoice_number" => isset($data->invoice_number) ? $data->invoice_number : ""
        );

        addBilling($database, $data->modifyId, $updateData, 'update');

        $database->where ('id', $data->modifyId);
        $id = $database->update (BILLING_FRANCHISE, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Billing Payment Details modified successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "delete"){
        $updateData = Array (
            "is_deleted" => 1
        );

        addBilling($database, $data->modifyId, array("franchise_id" => $data->franchise_id), 'delete');

        $database->where ('id', $data->modifyId);
        $id = $database->update (BILLING_FRANCHISE, $updateData);
        if($id){
            
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Billing Payment Deleted succesfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "add"){
        saveLog($database,"{$data->formAction} Bill to Franchise: {$data->description}");
    }else{
        saveLog($database,"{$data->formAction} Bill to Franchise {$data->modifyId}");
    }
}

//GET METHODS
if(isset($_GET["get"])){
    $primaryKey = 'id'; //primary key
    $columns = array(
        array( 'db' => 'id', 'dt' => 0 ),
        array(  'db' => 'franchise_id',  
                'dt' => 1,
                'formatter' => function ($data, $row) {
                    global $database;

                    $database->where("id", $data);
                    $emp = $database->getOne(FRANCHISEE);

                    return empty($emp) ? "" : $emp['name'];
                }
        ),
        array( 'db' => 'description',  'dt' => 2 ),
        array( 'db' => 'total_amount',   'dt' => 3 ),
        array(  'db' => 'status',   
                'dt' => 4,
                'formatter' => function ($data, $row){

                    return convertStatusColor($data);
                }
            ),
        array( 'db' => 'or_number',   'dt' => 5 ),
        array( 'db' => 'invoice_number',   'dt' => 6 ),
        array( 'db' => 'added_by',   'dt' => 7 ),
        array( 'db' => 'date_time',   'dt' => 8 ),
        array( 'db' => 'remarks',   'dt' => 9 ),
        array(  'db' => 'id',   
                'dt' => 10 ,
                'formatter' => function($data ,$row) {

                    return ' <div class="btn-group dropdown">
                            <button style="color:white;" aria-expanded="false" data-toggle="dropdown" class="btn bg-theme dropdown-toggle waves-effect waves-light" type="button">Take Action <span class="caret"></span></button>
                            <ul role="menu" class="dropdown-menu animated">
                                <li><a href="#" onclick="modify('.$data.')">Modify</a></li>
                            </ul>
                        </div>';
                    
                }
            ),
    );
    
    $condition = "is_deleted = 0";

    if(isset($_GET['status'])){
        $condition .= " and status = '{$_GET['status']}'";
    }

    echo json_encode(
        SSPCustom::simpleCustom( $_GET, $sqlSSPDetails, BILLING_FRANCHISE , $primaryKey, $columns, $condition )
    );
}

if(isset($_GET["getDetails"])){
    $id = $_GET["getDetails"];

    $database->where ("id", $id);
    $userDB = $database->getOne(BILLING_FRANCHISE);
    echo json_encode($userDB);
}

if(isset($_GET["getCurrentBilling"])){
    $id = $_GET["getCurrentBilling"];

    $database->where ("franchise_id", $id);
    $database->where("status", "UNPAID");
    $database->where("is_deleted", 0);
    $userDB = $database->get(BILLING_FRANCHISE);
    echo json_encode($userDB);
}

?>