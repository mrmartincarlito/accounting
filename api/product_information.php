<?php
require_once("config.php");
require_once("logs.php");

if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    if($data->formAction == "add"){

        $insertData = Array (
            "product_code" => generateRefno($database, "1", PRODUCT_TABLE, "product_code", "I-"),
            "supplier_id" => $data->supplier_id,
            "description" => $data->description,
            "cost" => $data->cost,
            "price" => $data->price,
            "category" => $data->category,
            "order_count" => $data->order_count,
            "uom" => $data->uom
        );

        $id = $database->insert (PRODUCT_TABLE, $insertData);
        if($id){
            $_SESSION['image-product-id'] = $id;
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Product Added successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "edit"){

        $updateData = Array (
            //"product_code" => time(), //to be finalized
            "supplier_id" => $data->supplier_id,
            "description" => $data->description,
            "cost" => $data->cost,
            "price" => $data->price,
            "category" => $data->category,
            "order_count" => $data->order_count,
            "uom" => $data->uom
        );
        $_SESSION['image-product-id'] = $data->modifyId;
        $database->where ('id', $data->modifyId);
        $id = $database->update (PRODUCT_TABLE, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Product Details modified successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "delete"){
        $updateData = Array (
            "is_deleted" => 1
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (PRODUCT_TABLE, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Product Deleted succesfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "add"){
        saveLog($database,"{$data->formAction} Product: {$data->description}");
    }else{
        saveLog($database,"{$data->formAction} Product ID {$data->modifyId}");
    }
}

//GET METHODS
if(isset($_GET["get"])){
    $primaryKey = 'id';
    $columns = array(
        array( 'db' => 'id', 'dt' => 0 ),
        array( 'db' => 'product_code',  'dt' => 1 ),
        array(  'db' => 
                'image_file',  'dt' => 2 ,
                'formatter' => function ($data, $row){
                    $image = empty($data) ? "plugins/images/no-item.png" : "api/$data";
                    return '<img src="'.$image.'" alt="NO IMAGE" class="img-thumbnail">';

                }
        ),
        array(  'db' => 'supplier_id',  
                'dt' => 3,
                'formatter' => function ($data, $row){
                    global $database;

                    $database->where("id", $data);
                    $supplier = $database->getOne(SUPPLIER_TABLE);
                    return empty($supplier) ? "" : $supplier["supplier_name"];
                }
            ),
        array( 'db' => 'description',   'dt' => 4 ),
        array( 'db' => 'cost',   'dt' => 5 ),
        array( 'db' => 'price',   'dt' => 6 ),
        array( 'db' => 'category',   'dt' => 7 ),
        array( 'db' => 'stocks',   'dt' => 8 ),
        array(  'db' => 'id',   
                'dt' => 9 ,
                'formatter' => function($data ,$row) {
                    return ' <div class="btn-group dropdown">
                                <button style="color:white;" aria-expanded="false" data-toggle="dropdown" class="btn bg-theme dropdown-toggle waves-effect waves-light" type="button">Take Action <span class="caret"></span></button>
                                <ul role="menu" class="dropdown-menu animated">
                                    <li><a href="#" onclick="modify('.$data.')">Modify</a></li>
                                    <li><a href="#" onclick="viewProductJournal('.$data.')">Product Journal</a></li>
                                    <!--<li><a href="#" onclick="actualCount('.$data.')">Edit Actual Count</a></li>-->
                                </ul>
                            </div>';
                }
            ),
    );
    
    $condition = "is_deleted = 0";

    if(isset($_GET['food'])){
        $condition .= " and category = 'FOOD'";
    }

    if(isset($_GET['nonfood'])){
        $condition .= " and category = 'NON FOOD'";
    }

    if(isset($_GET['others'])){
        $condition .= " and category = 'OTHERS'";
    }

    echo json_encode(
        SSPCustom::simpleCustom( $_GET, $sqlSSPDetails, PRODUCT_TABLE , $primaryKey, $columns, $condition )
    );
}

if(isset($_GET['viewProductJournal'])){
    $id = $_GET['viewProductJournal'];
    
    $database->orderBy("date_time", "desc");
    $database->where("product_id", $id);
    $journals = $database->get(PRODUCT_JOURNAL);

    $response = array();

    foreach($journals as $journal){
        array_push($response, array(
            "item" => getProduct($journal["product_id"])["description"],
            "transaction" => $journal["transaction"],
            "ref_no" => $journal["ref_no"],
            "qty" => $journal["qty"],
            "date_time" => $journal["date_time"]
        ));
    }

    echo json_encode($response);
}

if(isset($_GET["getItemsChoices"])){
    $primaryKey = 'id';
    $columns = array(
        array(  'db' => 
                'image_file',  'dt' => 0 ,
                'formatter' => function ($data, $row){
                    $image = empty($data) ? "plugins/images/no-item.png" : "api/$data";
                    return '<img src="'.$image.'" alt="NO IMAGE" class="img-thumbnail">';

                }
        ),
        array(  'db' => 'description',   
                'dt' => 1 ,
                'formatter' => function ($data, $row){ 
                   
                    return "<h3>".$data."</h3>";
                } 
        ),
        array( 'db' => 'id',   
                'dt' => 2, 
                'formatter' => function ($data, $row){ 
                    $product = getProduct($data);
                    
                    $select = "<select id='qty_choice_".$data."'>";

                    for ($i = 1; $i<=1000; $i++) {
                        $select .= "<option>".($i * $product["order_count"])."</option>";
                    }

                    $select .= "</select>";

                    $cart = '<button type="button" id="cart_'.$data.'" class="btn btn-danger" onclick="addToCart('.$data.')"><i class="fa fa-cart-plus m-r-5"></i> <span>ADD TO CART</span></button>';

                    return "<h3>Qty: $select</h3>
                    <h3>Price: <b>Php ".number_format($product["price"])."</b></h3>
                    $cart"; 
                } 
        )
    );
    
    $condition = "is_deleted = 0 and NOT(category = 'OTHERS')";

    echo json_encode(
        SSPCustom::simpleCustom( $_GET, $sqlSSPDetails, PRODUCT_TABLE , $primaryKey, $columns, $condition )
    );
}

if(isset($_GET["getAll"])){
    $database->where("is_deleted" , 0);
    $userDB = $database->get(PRODUCT_TABLE);
    echo json_encode($userDB);
}

if(isset($_GET['getProduct'])){
    $database->where("is_deleted" , 0);
    $database->where("category", $_GET['getProduct']);
    $userDB = $database->get(PRODUCT_TABLE);
    echo json_encode($userDB);
}

if(isset($_GET["getDetails"])){
    $id = $_GET["getDetails"];

    $database->where ("id", $id);
    $userDB = $database->getOne(PRODUCT_TABLE);
    echo json_encode($userDB);
}


if(isset($_GET['getProductBySupplierId'])){
    $supplierId = $_GET['getProductBySupplierId'];

    $database->where("supplier_id", $supplierId);
    $database->where("is_deleted", 0);
    $products = $database->get(PRODUCT_TABLE);

    echo json_encode($products);
}

?>