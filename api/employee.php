<?php
require_once("config.php");
require_once("logs.php");

if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    if($data->formAction == "add"){
        //insert
        $insertData = Array (
            "first_name" => $data->first_name,
            "middle_name" => isset($data->middle_name) ? $data->middle_name : "",
            "last_name" => $data->last_name,
            "city" => $data->city,
            "province" => $data->province,
            "employment_date" => $data->employment_date,
            "regularization_date" => $data->regularization_date,
            "rate" => $data->rate,
            "employment_type" => $data->employment_type
        );

        $id = $database->insert (EMP_TABLE, $insertData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Employee added successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "edit"){

        $updateData = Array (
            "first_name" => $data->first_name,
            "middle_name" => isset($data->middle_name) ? $data->middle_name : "",
            "last_name" => $data->last_name,
            "city" => $data->city,
            "province" => $data->province,
            "employment_date" => $data->employment_date,
            "regularization_date" => $data->regularization_date,
            "rate" => $data->rate,
            "employment_type" => $data->employment_type
        );

        //update table set fname = fname etc 
        //where id = modifyId
        $database->where ('id', $data->modifyId);
        $id = $database->update (EMP_TABLE, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Employee details modified successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "delete"){
        $updateData = Array (
            "is_deleted" => 1
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (EMP_TABLE, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Employee deleted succesfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "add"){
        saveLog($database,"{$data->formAction} ACCOUNT: {$data->first_name}");
    }else{
        saveLog($database,"{$data->formAction} ACCOUNT ID {$data->modifyId}");
    }
}

//GET METHODS
if(isset($_GET["get"])){
    //SELECT *
    $database->where("is_deleted", 0);
    $employeeData = $database->get(EMP_TABLE);
    echo json_encode($employeeData);
}

if(isset($_GET["getDetails"])){
    $id = $_GET["getDetails"];
    //SELECT WHERE
    $database->where ("id", $id);
    $employeeData = $database->getOne(EMP_TABLE);
    echo json_encode($employeeData);
}

?>