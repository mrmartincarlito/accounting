<?php
require_once("config.php");
require_once("logs.php");

$loggedUser = json_decode(getLoggedUserDetails($database));

if(isset($_GET["getOverview"])){

    $database->where("is_deleted", 0);
    $userDB = $database->get(ORDER_RETURNS);
    echo json_encode($userDB);
}



if(isset($_POST['proceedReturn'])){
    $data = json_decode($_POST['proceedReturn']);

    $refNo = generateRefno($database, "1", ORDER_RETURNS, "return_ref_no", "R-");

    $database->where("is_deleted", 0);
    $database->where("order_ref", $data->info->order_ref_no);
    $order = $database->getOne(ORDERS);

    if($order["status"] == "APPROVED" || $order["status"] == "PENDING"){
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => "This order is not yet received by the franchisee"
        ));

        return;
    }

    $id = $database->insert(ORDER_RETURNS, array(
        "order_ref_no" => $data->info->order_ref_no,
        "return_ref_no" => $refNo,
        "franchisee_id" => $data->info->franchisee_id,
        "status" => "PENDING",
        "added_by" => $loggedUser->username,
        "remarks" => $data->info->remarks
    ));

    if($id){
        foreach($data->orders as $order){
            $database->insert(ORDER_RETURN_ITEMS, array(
                "return_ref" => $refNo,
                "item_id" => getOrderItems($order->id)["item_id"],
                "return_qty" => $order->qty,
                "cost" => $order->price,
                "total_amount" => $order->total_amount
            ));
        }

        echo json_encode(Array (
            "type" => "success",
            "title" => "Successful!",
            "text" => "Successfully Placed Your Return Goods!"
        ));
    }else{
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => "Something went wrong please try again later"
        ));
    }
}

if(isset($_POST['cancelReturn'])){
    $id = $_POST['cancelReturn'];

    $database->where("id", $id);
    $return = $database->getOne(ORDER_RETURNS);

    if($return["status"] == "APPROVED"){
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => "This is already APPROVED by the commissary, please contact them to cancel"
        ));

        return;
    }

    
    if($return["status"] == "REJECTED"){
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => "You cannot cancel a REJECTED return request"
        ));

        return;
    }

    $database->where("id", $id);
    $id = $database->update(ORDER_RETURNS, array("is_deleted" => 1, "status" => "CANCELLED"));

    if($id){
        echo json_encode(Array (
            "type" => "success",
            "title" => "Successful!",
            "text" => "You just cancelled your return request"
        ));
    }else{
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => "Something went wrong please try again later"
        ));
    }

}

if(isset($_GET["get"])){

    $primaryKey = 'id';
    $columns = array(
        array( 'db' => 'id', 'dt' => 0 ),
        array( 'db' => 'order_ref_no',  'dt' => 1 ),
        array( 'db' => 'return_ref_no',  'dt' => 2 ),
        array(  'db' => 'order_ref_no',  
                'dt' => 3,
                'formatter' => function ($data, $row){

                    global $database;

                    $database->where("order_ref", $data);
                    $order = $database->getOne(ORDERS);

                    $franchisee = getFranchise($order["franchisee_id"]);

                    return $franchisee["name"];
                }
        ),
        array(  'db' => 'status',   
                'dt' => 4,
                'formatter' => function ($data, $row){

                    return convertStatusColor($data);
                }
            ),
        array( 'db' => 'date_time',   'dt' => 5 ),
        array( 'db' => 'remarks',   'dt' => 6 ),
        array(  'db' => 'return_ref_no',   
                'dt' => 7 ,
                'formatter' => function($data ,$row) {

                    $poNumber = "'".$data."'";

                    // $button = "";
                    // if($row["status"] == "PENDING"){   
                        $button = '<button class="btn btn-primary" onclick="viewReturnGoods('.$poNumber.')"><i class="ti-eye"></i> VIEW RETURN GOODS</button>';   
                    //}
                    
                    return $button;
                }
            ),
    );
    
    $condition = "is_deleted = 0";

    if(isset($_GET['status'])){
        $condition .= " and status = '{$_GET['status']}'";
    }

    echo json_encode(
        SSPCustom::simpleCustom( $_GET, $sqlSSPDetails, ORDER_RETURNS , $primaryKey, $columns, $condition )
    );
}

if(isset($_GET['getReturnItems'])){
    $database->where("return_ref", $_GET['getReturnItems']);
    $items = $database->get(ORDER_RETURN_ITEMS);

    $response = array();

    foreach($items as $item){
        array_push($response, array(
            "return_ref" => $item['return_ref'],
            "item_id" => $item['item_id'],
            "return_qty" => $item['return_qty'],
            "cost" => number_format($item['cost']),
            "total_amount" => number_format($item['total_amount']),
            "item" => getProduct($item['item_id'])["description"]
        ));
    }

    echo json_encode($response);
}


if(isset($_POST['approveStatus'])){
    $status = $_POST['approveStatus'];
    $returnRef = $_POST['return_ref'];

    $database->where("return_ref_no", $returnRef);
    $database->update(ORDER_RETURNS, array("status" => $status));

    echo json_encode(Array (
        "type" => "success",
        "title" => "Successful!",
        "text" => "Successfully ".$status . " return"
    ));
}