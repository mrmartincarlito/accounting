<?php
require_once("config.php");
require_once("logs.php");
require_once("payments-movements.php");

$loggedUser = json_decode(getLoggedUserDetails($database));

if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    $approved_by = "";
    $approved_date = null;

    $checkApprovalAccess = checkApproval($database, $data->status);

    if($checkApprovalAccess == "NO ACCESS"){
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => "You do not have approval or reject power, Please change your status to PENDING to proceed"
        ));

        return;
    }

    $or_number = "";
    $invoice = "";
    if(isset($data->status) && $data->status == "APPROVED"){
        $approved_by = $loggedUser->username;
        $approved_date = $database->now();

        $or_number = isset($data->or_number) ? $data->or_number : "";
        $invoice = isset($data->invoice_number) ? $data->invoice_number : "";

        $database->where("order_ref", $invoice);
        $database->where("franchisee_id", $data->franchise_id);
        $getFranchiseOrder = $database->getOne(ORDERS);

        if (empty($getFranchiseOrder)) {
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => "DR # does not exist on Franchisee"
            ));
    
            return;
        }
    }

    if($data->formAction == "add"){

        $insertData = Array (
            "franchise_id" => $data->franchise_id,
            "total_amount" => $data->total_amount,
            "date_paid" => $data->date_paid,
            "check_no" => isset($data->check_no) ? $data->check_no : "",
            "check_details" => isset($data->check_details) ? $data->check_details : "",
            "bank" => isset($data->bank) ? $data->bank : "",
            "remarks" => isset($data->remarks) ? $data->remarks : "",
            "status" => $data->status,
            "approved_by" => $approved_by,
            "approved_date" => $approved_date,
            "or_number" => $or_number,
            "invoice_number" => $invoice
        );

        $id = $database->insert (ORDER_PAYMENTS, $insertData);
        if($id){
            $_SESSION['image-order-payment-id'] = $id;

            calculateOrderBalance($database, $insertData);

            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Order Payment Added successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "edit"){

        $checkIfAlreadyApproved = false;

        $database->where ('id', $data->modifyId);
        $orderPayment = $database->getOne(ORDER_PAYMENTS);

        if ($orderPayment["status"] == "APPROVED" || $orderPayment["status"] == "REJECTED"){
            $checkIfAlreadyApproved = true;
        }

        $updateData = Array (
            "franchise_id" => $data->franchise_id,
            "total_amount" => $data->total_amount,
            "date_paid" => $data->date_paid,
            "check_no" => isset($data->check_no) ? $data->check_no : "",
            "check_details" => isset($data->check_details) ? $data->check_details : "",
            "bank" => isset($data->bank) ? $data->bank : "",
            "remarks" => isset($data->remarks) ? $data->remarks : "",
            "status" => $data->status,
            "approved_by" => $approved_by,
            "approved_date" => $approved_date,
            "or_number" => $or_number,
            "invoice_number" => $invoice
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (ORDER_PAYMENTS, $updateData);
        if($id){

            $_SESSION['image-order-payment-id'] = $data->modifyId;

            if(!$checkIfAlreadyApproved){
                $database->where ('id', $data->modifyId);
                $orderPayment = $database->getOne(ORDER_PAYMENTS);
                calculateOrderBalance($database, $orderPayment);
            }

            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Order Payment Details modified successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "delete"){
        $updateData = Array (
            "is_deleted" => 1
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (ORDER_PAYMENTS, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Order Payment Deleted succesfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "add"){
        saveLog($database,"{$data->formAction} Order Payment: {$data->franchise_id}");
    }else{
        saveLog($database,"{$data->formAction} Order Payment ID {$data->modifyId}");
    }
}

//GET METHODS
if(isset($_GET["get"])){
    $primaryKey = 'id';
    $columns = array(
        array( 'db' => 'id', 'dt' => 0 ),
        array(  'db' => 'franchise_id',  
                'dt' => 1,
                'formatter' => function ($data, $row) {
                    global $database;

                    $database->where("id", $data);
                    $emp = $database->getOne(FRANCHISEE);

                    return empty($emp) ? "" : $emp['name'];
                }
        ),
        array( 'db' => 'total_amount',   'dt' => 2 ),
        array(  'db' => 'date_paid',   
                'dt' => 3,
                'formatter' => function ($data, $row){
                    return date("F d, Y", strtotime($data));
                }
            ),
        array( 'db' => 'remarks',   'dt' => 4 ),
        array(  'db' => 'status',   
                'dt' => 5,
                'formatter' => function ($data, $row){

                    return convertStatusColor($data);
                }
            ),
        array( 'db' => 'approved_by',   'dt' => 6 ),
        array( 'db' => 'approved_date',   'dt' => 7 ),
        array( 'db' => 'or_number',   'dt' => 8 ),
        array( 'db' => 'invoice_number',   'dt' => 9 ),
        array(  'db' => 'id',   
                'dt' => 10 ,
                'formatter' => function($data ,$row) {

                    if($row["status"] == "PENDING"){
                        return ' <div class="btn-group dropdown">
                                <button style="color:white;" aria-expanded="false" data-toggle="dropdown" class="btn bg-theme dropdown-toggle waves-effect waves-light" type="button">Take Action <span class="caret"></span></button>
                                <ul role="menu" class="dropdown-menu animated">
                                    <li><a href="#" onclick="modify('.$data.')">Modify</a></li>
                                    <li><a href="#" onclick="viewImage('.$data.')">View Image Receipt</a></li>
                                </ul>
                            </div>';
                    }else{
                        // return ' <div class="btn-group dropdown">
                        //         <button style="color:white;" aria-expanded="false" data-toggle="dropdown" class="btn bg-theme dropdown-toggle waves-effect waves-light" type="button">Take Action <span class="caret"></span></button>
                        //         <ul role="menu" class="dropdown-menu animated">
                        //             <li><a href="#" onclick="viewImage('.$data.')">View Image Receipt</a></li>
                        //         </ul>
                        //     </div>';

                        return '<button type="button" class="btn btn-info btn-circle" onclick="viewImage('.$data.')" title="View Receipt"><i class="fa fa-eye"></i> </button>
                        <button type="button" class="btn btn-success btn-circle" onclick="printOR('.$data.')" title="Official Receipt"><i class="fa  fa-book"></i> </button>
                        <button type="button" class="btn btn-warning btn-circle" onclick="printAR('.$data.')" title="Acknowledgement Receipt"><i class="fa  fa-pencil-square-o"></i> </button>
                        
                        ';
                    }
                    
                }
            ),
    );
    
    $condition = "is_deleted = 0";

    if(isset($_GET['status'])){
        $condition .= " and status = '{$_GET['status']}'";
    }

    echo json_encode(
        SSPCustom::simpleCustom( $_GET, $sqlSSPDetails, ORDER_PAYMENTS , $primaryKey, $columns, $condition )
    );
}

if(isset($_GET["getDetails"])){
    $id = $_GET["getDetails"];

    $database->where ("id", $id);
    $userDB = $database->getOne(ORDER_PAYMENTS);
    echo json_encode($userDB);
}

if(isset($_GET['changeStatus'])){
    $data = json_decode($_GET['changeStatus']);

    $checkApprovalAccess = checkApproval($database, $data->status);

    if($checkApprovalAccess == "NO ACCESS"){
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => "You do not have approval or reject power"
        ));

        return;
    }
    $database->where("id", $data->id);
    $billing = $database->getOne(ORDER_PAYMENTS);

    if($billing["status"] != "PENDING" ){
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => "This is already ".$billing["status"]
        ));

        return;
    }

    $database->where("id", $data->id);
    $update = $database->update(ORDER_PAYMENTS, array(
        "status" => $data->status,
        "approved_by" => $loggedUser->username,
        "approved_date" => $database->now()
    ));

    if($update){

        if($data->status == "APPROVED") {
            $database->where("id", $data->id);
            $billing = $database->getOne(ORDER_PAYMENTS);
            calculateOrderBalance($database, $billing);
        }

        echo json_encode(Array (
            "type" => "success",
            "title" => "Successful!",
            "text" => "Order Payment Updated succesfully!"
        ));
    }else{
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => $database->getLastError()
        ));
    }
}
?>