<?php
require_once("config.php");
require_once("logs.php");

$loggedUser = json_decode(getLoggedUserDetails($database));

if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);
    $poNumber = $_POST['po'];

    $database->where("po_number", $poNumber);
    $po = $database->get(PURCHASE_ORDER);

    if (empty($po)) {
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => "PO Number is invalid"
        ));
        return;
    }

    if($data->formAction == "add"){

        $insertData = Array (
            "po_number" => $poNumber,
            "amount" => $data->amount,
            "date_paid" => $data->date_paid,
            "check_no" => isset($data->check_no) ? $data->check_no : "",
            "check_details" => isset($data->check_details) ? $data->check_details : "",
            "bank" => isset($data->bank) ? $data->bank : "",
            "remarks" => isset($data->remarks) ? $data->remarks : "",
            "added_by" => $loggedUser->username
        );

        $id = $database->insert (PURCHASE_ORDER_PAYMENTS, $insertData);
        if($id){
        
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "PO Payment Added successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "edit"){


        $updateData = Array (
            "amount" => $data->amount,
            "date_paid" => $data->date_paid,
            "check_no" => isset($data->check_no) ? $data->check_no : "",
            "check_details" => isset($data->check_details) ? $data->check_details : "",
            "bank" => isset($data->bank) ? $data->bank : "",
            "remarks" => isset($data->remarks) ? $data->remarks : "",
            "added_by" => $loggedUser->username
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (PURCHASE_ORDER_PAYMENTS, $updateData);
        if($id){

            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Purchase Order Payment Details modified successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "delete"){
        $updateData = Array (
            "is_deleted" => 1
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (PURCHASE_ORDER_PAYMENTS, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Purchase Order Payment Deleted succesfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "add"){
        saveLog($database,"{$data->formAction} Purchase Order Payment: {$poNumber}");
    }else{
        saveLog($database,"{$data->formAction} Purchase Order Payment ID {$data->modifyId}");
    }
}

//GET METHODS
if(isset($_GET["get"])){
    $primaryKey = 'id';
    $columns = array(
        array( 'db' => 'id', 'dt' => 0 ),
        array( 'db' => 'po_number',   'dt' => 1 ),
        array( 'db' => 'amount',   'dt' => 2 ),
        array( 'db' => 'remarks',   'dt' => 3 ),
        array(  'db' => 'date_paid',   
                'dt' => 4,
                'formatter' => function ($data, $row){
                    return date("F d, Y", strtotime($data));
                }
            ),
        array( 'db' => 'added_by',   'dt' => 5 ),
        array(  'db' => 'id',   
        'dt' => 6 ,
        'formatter' => function($data ,$row) {

            return ' <button class="btn btn-info" onclick="modify('.$data.')">VIEW</button>
            ';
            
        }
    )
    );
    
    $po = "0";
    $condition = "is_deleted = 0";

    if(isset($_GET['q'])){
        $po = $_GET['q'];
    }

    $condition .= " and po_number = '$po'";

    echo json_encode(
        SSPCustom::simpleCustom( $_GET, $sqlSSPDetails, PURCHASE_ORDER_PAYMENTS , $primaryKey, $columns, $condition )
    );
}

if(isset($_GET["getDetails"])){
    $id = $_GET["getDetails"];

    $database->where ("id", $id);
    $userDB = $database->getOne(PURCHASE_ORDER_PAYMENTS);
    echo json_encode($userDB);
}

if (isset($_GET['getSumAmount'])) {
    $po = $_GET['getSumAmount'];

    $sum = $database->rawQuery("Select SUM(amount) as amount from " . PURCHASE_ORDER_PAYMENTS . " where po_number = '$po' and is_deleted = 0");

    echo json_encode($sum);
}

?>