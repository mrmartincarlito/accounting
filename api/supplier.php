<?php
require_once("config.php");
require_once("logs.php");

if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    if($data->formAction == "add"){
        $insertData = Array (
            "supplier_name" => $data->supplier_name,
            "supplier_address" => $data->supplier_address,
            "contact_number" => $data->contact_number
        );  

        $id = $database->insert (SUPPLIER_TABLE, $insertData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Supplier added successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "edit"){

        $updateData = Array (
            "supplier_name" => $data->supplier_name,
            "supplier_address" => $data->supplier_address,
            "contact_number" => $data->contact_number
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (SUPPLIER_TABLE, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Supplier details modified successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "delete"){

        $updateData = Array (
            "is_deleted" => 1
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (SUPPLIER_TABLE, $updateData);

        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Supplier deleted succesfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "add"){
        saveLog($database,"{$data->formAction} SUPPLIER NAME: {$data->supplier_name}");
    }else{
        saveLog($database,"{$data->formAction} SUPPLIER ID {$data->modifyId}");
    }
}

//GET METHODS
if(isset($_GET["get"])){
    $database->where("is_deleted", 0);
    $supplier = $database->get(SUPPLIER_TABLE);
    echo json_encode($supplier);
}

if(isset($_GET["getDetails"])){
    $id = $_GET["getDetails"];

    $database->where ("id", $id);
    $supplier = $database->getOne(SUPPLIER_TABLE);
    echo json_encode($supplier);
}
