<?php
require_once("config.php");
require_once("logs.php");

$loggedUser = json_decode(getLoggedUserDetails($database));

if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);
    $added_by = $loggedUser->username;

    if($data->formAction == "add"){
        $insertData = Array (
            "date_paid" => $data->date_paid,
            "expense_type" => $data->expense_type,
            "voucher_number" => $data->voucher_number,
            "category" => $data->category,
            "sub_category" => isset($data->sub_category) ? $data->sub_category : "",
            "total_amount" => $data->total_amount,
            "added_by" => $added_by,
            "remarks" => $data->remarks
        );

        $id = $database->insert (EXPENSES, $insertData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Expense Added successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "edit"){

        $updateData = Array (
            "date_paid" => $data->date_paid,
            "expense_type" => $data->expense_type,
            "voucher_number" => $data->voucher_number,
            "category" => $data->category,
            "sub_category" => isset($data->sub_category) ? $data->sub_category : "",
            "total_amount" => $data->total_amount,
            "added_by" => $added_by,
            "remarks" => $data->remarks
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (EXPENSES, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Expense Details modified successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "delete"){
        $updateData = Array (
            "is_deleted" => 1
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (EXPENSES, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Expense Deleted succesfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "add"){
        saveLog($database,"{$data->formAction} Expense: {$data->expense_type}");
    }else{
        saveLog($database,"{$data->formAction} Expense ID {$data->modifyId}");
    }
}

//GET METHODS
if(isset($_GET["get"])){
    $primaryKey = 'id';
    $columns = array(
        array( 'db' => 'id', 'dt' => 0 ),
        array( 'db' => 'date_paid',   
                'dt' => 1,
                'formatter' => function ($data, $row){
                    return date("F d, Y", strtotime($data));
                }
        ),
        array( 'db' => 'expense_type', 'dt' => 2 ),
        array( 'db' => 'voucher_number', 'dt' => 3 ),
        array( 'db' => 'category', 'dt' => 4 ),
        array( 'db' => 'sub_category', 'dt' => 5 ),
        array( 'db' => 'total_amount', 'dt' => 6 ),
        array( 'db' => 'added_by', 'dt' => 7 ),
        array( 'db' => 'remarks', 'dt' => 8 ),
        array(  'db' => 'id',   
        'dt' => 9 ,
        'formatter' => function($data ,$row) {
            return ' <div class="btn-group dropdown">
                        <button style="color:white;" aria-expanded="false" data-toggle="dropdown" class="btn bg-theme dropdown-toggle waves-effect waves-light" type="button">Take Action <span class="caret"></span></button>
                        <ul role="menu" class="dropdown-menu animated">
                            <li><a href="#" onclick="modify('.$data.')">Modify</a></li>
                        </ul>
                    </div>';
        }
    ),
    );
    $condition = "is_deleted = 0";

    if(isset($_GET['type'])){
        $condition .= " and expense_type = '{$_GET['type']}'";
    }

    echo json_encode(
        SSPCustom::simpleCustom( $_GET, $sqlSSPDetails, EXPENSES , $primaryKey, $columns, $condition)
    );
    
}

if(isset($_GET["getDetails"])){
    $id = $_GET["getDetails"];

    $database->where ("id", $id);
    $userDB = $database->getOne(EXPENSES);
    echo json_encode($userDB);
}

?>