<?php

function updateProductStocks($database, $productId, $qty, $transaction, $refno, $movement = "+"){
    $database->rawQuery("Update ".PRODUCT_TABLE." set stocks = stocks $movement $qty where id = $productId");

    addProductJournal($database,
        array(
            "product_id" => $productId,
            "transaction" => $transaction,
            "qty" => $qty,
            "ref_no" => $refno
        )
    );
}   

function addProductJournal($database, $insertData){
    $database->insert(PRODUCT_JOURNAL, $insertData);
}