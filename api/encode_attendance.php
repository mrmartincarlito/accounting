<?php
require_once("config.php");
require_once("logs.php");

define("TABLE_NAME", "attendance");
define("EMPLOYEES", "employees");

$loggedUser = json_decode(getLoggedUserDetails($database));

if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    if($data->formAction == "add"){

        $insertData = Array (
            "emp_id" => $data->emp_id,
            "total_minutes" => $data->total_minutes,
            "regular_legal_ot" => $data->regular_legal_ot,
            "special_ot" => $data->special_ot,
            "special_holiday" => $data->special_holiday,
            "legal_holiday" => $data->legal_holiday,
            "total_hours" => computeTotalHours($data),
            "legend" => $data->legend,
            "work_date" => $data->work_date,
            "added_by" => $loggedUser->username
        );

        //check if emp workday already existed
        $database->where("emp_id", $data->emp_id);
        $database->where("work_date", $data->work_date);
        $workLogExisting = $database->getOne(TABLE_NAME);

        if(!empty($workLogExisting)){
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => "Attendance work log for this employee already existed"
            ));

            return;
        }


        $id = $database->insert (TABLE_NAME, $insertData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Attendance Added successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "edit"){

        $updateData = Array (
            "emp_id" => $data->emp_id,
            "total_minutes" => $data->total_minutes,
            "regular_legal_ot" => $data->regular_legal_ot,
            "special_ot" => $data->special_ot,
            "special_holiday" => $data->special_holiday,
            "legal_holiday" => $data->legal_holiday,
            "total_hours" => computeTotalHours($data),
            "legend" => $data->legend,
            //"work_date" => $data->work_date,
            "added_by" => $loggedUser->username
        );
       
        $database->where ('id', $data->modifyId);
        $id = $database->update (TABLE_NAME, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Attendance Details modified successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "delete"){
        $updateData = Array (
            "is_deleted" => 1
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (TABLE_NAME, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Attendance Deleted succesfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "add"){
        saveLog($database,"{$data->formAction} Attendance to Employee ID: {$data->emp_id}");
    }else{
        saveLog($database,"{$data->formAction} Attendance to Employee ID {$data->modifyId}");
    }
}

//GET METHODS
if(isset($_GET["get"])){
    $primaryKey = 'id';
    $columns = array(
        array( 'db' => 'id', 'dt' => 0 ),
        array(  'db' => 'emp_id',  
                'dt' => 1,
                'formatter' => function ($data, $row) {
                    global $database;

                    $database->where("id", $data);
                    $emp = $database->getOne(EMPLOYEES);

                    return empty($emp) ? "" : $emp['first_name']. " ".$emp['middle_name']. " ".$emp['last_name'];
                }
            ),
        array(  'db' => 'work_date',   
                'dt' => 2,
                'formatter' => function ($data, $row){
                    return date("l F d, Y", strtotime($data));
                }
            ),
        array( 'db' => 'legend',   'dt' => 3 ),
        array( 'db' => 'total_minutes',   'dt' => 4 ),
        array( 'db' => 'regular_legal_ot',   'dt' => 5 ),
        array( 'db' => 'special_ot',   'dt' => 6 ),
        array( 'db' => 'special_holiday',   'dt' => 7 ),
        array( 'db' => 'legal_holiday',   'dt' => 8 ),
        array( 'db' => 'total_hours',   'dt' => 9 ),
        array( 'db' => 'added_by',   'dt' => 10 ),
        array(  'db' => 'id',   
                'dt' => 11 ,
                'formatter' => function($data ,$row) {
                    return ' <div class="btn-group dropdown">
                                <button style="color:white;" aria-expanded="false" data-toggle="dropdown" class="btn bg-theme dropdown-toggle waves-effect waves-light" type="button">Take Action <span class="caret"></span></button>
                                <ul role="menu" class="dropdown-menu animated">
                                    <li><a href="#" onclick="modify('.$data.')">Modify</a></li>
                                    <!-- <li><a href="#" onclick="">Override Total Hours</a></li>-->
                                </ul>
                            </div>';
                }
            ),
    );
    
    $condition = "is_deleted = 0";

    if(isset($_GET['emp'])){
        $condition .= " and emp_id = '{$_GET['emp']}' AND work_date BETWEEN '{$_GET['startDate']}' AND '{$_GET['endDate']}'";
    }
    echo json_encode(
        SSPCustom::simpleCustom( $_GET, $sqlSSPDetails, TABLE_NAME , $primaryKey, $columns, $condition )
    );
}

if(isset($_GET["getDetails"])){
    $id = $_GET["getDetails"];

    $database->where ("id", $id);
    $userDB = $database->getOne(TABLE_NAME);
    echo json_encode($userDB);
}

function computeTotalHours($data){
    $minutes = $data->total_minutes + $data->regular_legal_ot + $data->special_ot + $data->special_holiday;

    $hours = floor($minutes / 60);
    $minutes = ($minutes % 60);

    return "$hours.$minutes";
}

?>