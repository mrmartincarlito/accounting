<?php
require_once("config.php");
require_once("logs.php");

$loggedUser = json_decode(getLoggedUserDetails($database));

if(isset($_GET['getFranchiseExpiry'])){
    
    $franchisee = $database->rawQuery("Select * from ".FRANCHISEE." where is_deleted = 0 and 
                                        date_contract_expiry BETWEEN date_sub(Now(), INTERVAL 3 MONTH)
                                        AND 
                                        Now()
                                        ORDER BY date_contract_expiry DESC");

    echo json_encode($franchisee);
}

if(isset($_GET['getFranchiseBalances'])){
   
    $database->where("is_deleted", 0);
    $database->orderBy("total_amount","ASC");
    $franchisee = $database->get(FRANCHISEE);

    echo json_encode($franchisee);
}

if(isset($_GET['getCards'])){
    $response = array();

    $database->where("is_deleted", 0);
    $database->where("status", "PENDING");
    $po = $database->get(PURCHASE_ORDER);

    $response["no_of_po"] = count($po);

    $database->where("is_deleted", 0);
    $database->where("status", "PENDING");
    $po = $database->get(ORDERS);

    $response["no_of_orders"] = count($po);

    $database->where("is_deleted", 0);
    $database->where("status", "PENDING");
    $po = $database->get(BILLING_PAYMENTS);

    $response["no_of_billing_payments"] = count($po);

    $database->where("is_deleted", 0);
    $database->where("status", "PENDING");
    $po = $database->get(ORDER_PAYMENTS);

    $response["no_of_order_payments"] = count($po);

    $database->where("is_deleted", 0);
    $po = $database->get(FRANCHISEE);

    $response["no_of_franchise"] = count($po);

    echo json_encode($response);
}

if(isset($_POST['renew'])){
    $postData = json_decode($_POST["renew"]);
    $data = json_decode($postData->data);

    $checkApprovalAccess = checkApproval($database, $data->status);

    if($checkApprovalAccess == "NO ACCESS"){
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => "You do not have APPROVAL POWER for this"
        ));

        return;
    }

    $database->where("id", $data->id);
    $franchise = $database->getOne(FRANCHISEE);

    $insertRenewal = array(
        "franchise_id" => $data->id,
        "expiry_date" => $franchise['date_contract_expiry'],
        "renewed_date" => $data->renewed_date,
        "new_expiry_date" => $data->new_expiry_date,
        "approved_by" => $loggedUser->username
    );

    $id = $database->insert(FRANCHISE_RENEWAL_TABLE, $insertRenewal);

    if($id){
        //update franchise contract expiry
        $update = array("date_contract_expiry" => $data->new_expiry_date);
        $database->where("id", $franchise["id"]);
        $database->update(FRANCHISEE, $update);

        echo json_encode(Array (
            "type" => "success",
            "title" => "Successful!",
            "text" => "Franchise renewed successfully"
        ));
    }else{
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => $database->getLastError()
        ));
    }
}