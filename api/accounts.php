<?php
require_once("config.php");
require_once("logs.php");

if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    if($data->formAction == "add"){
        $insertData = Array (
            "first_name" => $data->first_name,
            "middle_name" => $data->middle_name,
            "last_name" => $data->last_name,
            "email" => $data->email,
            "username" => $data->username,      
            "password" => password_hash($data->username,PASSWORD_DEFAULT),
            "role" => $data->role,
            "is_deleted" => 0
        );

        $id = $database->insert (ACCOUNTS, $insertData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Account added successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "edit"){
        $updateData = Array (
            "first_name" => $data->first_name,
            "middle_name" => $data->middle_name,
            "last_name" => $data->last_name,
            "email" => $data->email,
            "username" => $data->username,
            "password" => password_hash($data->username,PASSWORD_DEFAULT),
            "role" => $data->role,
            "is_deleted" => 0
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (ACCOUNTS, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Account details modified successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "delete"){
        $updateData = Array (
            "is_deleted" => 1
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (ACCOUNTS, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Account deleted succesfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "add"){
        saveLog($database,"{$data->formAction} ACCOUNT: {$data->first_name}");
    }else{
        saveLog($database,"{$data->formAction} ACCOUNT ID {$data->modifyId}");
    }
}

//GET METHODS
if(isset($_GET["get"])){
    $database->where ('s.is_deleted', 0);
    $database->join (ACCESS_LEVELS." a", "s.role=a.id", "LEFT");
    $userDB = $database->get(ACCOUNTS." s", null, "s.*, a.description");
    echo json_encode($userDB);
}

if(isset($_GET["getDetails"])){
    $id = $_GET["getDetails"];

    $cols = Array ("id", "first_name","middle_name","last_name","email","username", "role");
    $database->where ("id", $id);
    $userDB = $database->getOne(ACCOUNTS, $cols);
    echo json_encode($userDB);
}

?>