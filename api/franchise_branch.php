<?php
require_once("config.php");
require_once("logs.php");

if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    if($data->formAction == "add"){
        //default branch code
        $branch_code = generateRefno($database, "1", FRANCHISE_BRANCH_TABLE, "branch_code", "B-");
        
        $insertData = Array (
            "branch_code" =>  $branch_code,
            "branch_name" => $data->branch_name,
            "branch_location" => $data->branch_location
        );

        $id = $database->insert (FRANCHISE_BRANCH_TABLE, $insertData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Franchise Branch added successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "edit"){

        $updateData = Array (
            "branch_name" => $data->branch_name,
            "branch_location" => $data->branch_location
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (FRANCHISE_BRANCH_TABLE, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Franchise branch details modified successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "delete"){
        $updateData = Array (
            "is_deleted" => 1
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (FRANCHISE_BRANCH_TABLE, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Franchise Branch Account deleted succesfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "add"){
        saveLog($database,"{$data->formAction} ACCOUNT: {$data->branch_name}");
    }else{
        saveLog($database,"{$data->formAction} ACCOUNT ID {$data->modifyId}");
    }
}

//GET METHODS
if(isset($_GET["get"])){
    //SELECT *
    $database->where("is_deleted", 0);
    $branchData = $database->get(FRANCHISE_BRANCH_TABLE);
    echo json_encode($branchData);
}

if(isset($_GET["getDetails"])){
    $id = $_GET["getDetails"];
    //SELECT WHERE
    $database->where ("id", $id);
    $branchData = $database->getOne(FRANCHISE_BRANCH_TABLE);
    echo json_encode($branchData);
}

?>