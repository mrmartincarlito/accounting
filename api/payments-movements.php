<?php
require_once("config.php");
require_once("logs.php");

function calculateBillingBalance($database, $data){
        //get franchise information
        $franchise = getFranchise($data["franchise_id"]);

        //check whether the status is approved
        if($data["status"] == "APPROVED"){
            //update total billing balance of the franchise
            $billingBalance = $franchise["billing_balance"] - $data["total_amount"];
    
            $updateFranchiseOrderBalance = array(
                "billing_balance" => $billingBalance,
                "total_amount" => $franchise["order_balance"] + $billingBalance,
                "last_payment_amount" => $data["total_amount"],
                "last_payment_date" => $data["date_paid"]
            );
    
            //update franchise with new values
            $database->where("id", $data["franchise_id"]);
            $database->update(FRANCHISEE, $updateFranchiseOrderBalance);
        }
}

function calculateOrderBalance($database, $data){
    //get franchise information
    $franchise = getFranchise($data["franchise_id"]);

    //check whether the status is approved
    if($data["status"] == "APPROVED"){
        //update total order balance of the franchise
        $orderBalance = $franchise["order_balance"] - $data["total_amount"];

        $updateFranchiseOrderBalance = array(
            "order_balance" => $orderBalance,
            "total_amount" => $franchise["billing_balance"] + $orderBalance,
            "last_payment_amount" => $data["total_amount"],
            "last_payment_date" => $data["date_paid"]
        );

        //update franchise with new values
        $database->where("id", $data["franchise_id"]);
        $database->update(FRANCHISEE, $updateFranchiseOrderBalance);
    }

}

function addBilling($database, $id, $data, $process){
    if($process == 'new'){
        //new billing
        $franchise = getFranchise($data["franchise_id"]);

        $newTotalAmount = $franchise["billing_balance"] + $data["total_amount"];

        updateBilling($database, $franchise, $newTotalAmount, $franchise["order_balance"] + $newTotalAmount );

    }

    if($process == 'update'){
        //get current billing
        $database->where("id", $id);
        $bill = $database->getOne(BILLING_FRANCHISE);

        if($bill["status"] == "UNPAID") {
            $franchise = getFranchise($data["franchise_id"]);

            //deduct previous
            $deductPreviousBill = $franchise["billing_balance"] - $bill["total_amount"];
            updateBilling($database, $franchise, $deductPreviousBill, $franchise["order_balance"] + $deductPreviousBill);
            
            //add new current
            $franchise = getFranchise($data["franchise_id"]); //get again updated franchise
            $newTotalAmount = $franchise["billing_balance"] + $data["total_amount"];
    
            updateBilling($database, $franchise, $newTotalAmount, $franchise["order_balance"] + $newTotalAmount );
        }

        if($bill["status"] == "PAID"){
            // update billing payments update or and invoice number

            $database->where("bill_id", $bill["id"]);
            $database->update(BILLING_PAYMENTS, array(
                "or_number" => $data["or_number"],
                "invoice_number" => $data["invoice_number"]
            ));
        }

    }

    if($process == 'delete'){
        //delete billing
        $database->where("id", $id);
        $bill = $database->getOne(BILLING_FRANCHISE);

        $franchise = getFranchise($data["franchise_id"]);

        //deduct previous
        $deductPreviousBill = $franchise["billing_balance"] - $bill["total_amount"];
        updateBilling($database, $franchise, $deductPreviousBill, $franchise["order_balance"] + $deductPreviousBill);
    }
}

function updateBilling($database, $franchise, $bill_balance, $total_amount){
    $updateData = array(
        "billing_balance" => $bill_balance,
        "total_amount" => $franchise["order_balance"] + $total_amount
    );

    $database->where("id", $franchise["id"]);
    $database->update(FRANCHISEE, $updateData);
}

function setBillingToPaid($database, $id, $or, $inv){
    $database->where("id", $id);
    $database->update(BILLING_FRANCHISE, array(
        "status" => "PAID",
        "or_number" => $or,
        "invoice_number" => $inv
    ));
}