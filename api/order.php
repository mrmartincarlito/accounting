<?php
require_once("config.php");
require_once("logs.php");
require_once("inventory.php");

$loggedUser = json_decode(getLoggedUserDetails($database));

if(isset($_POST['saveOrder'])){
    $items = json_decode($_POST['saveOrder']);

    $total_amount = 0;
    $refNo = generateRefno($database, "1", ORDERS, "order_ref", "O-");

    foreach($items->orders as $item){
        $total_amount = $total_amount + str_replace("," , "" , $item->total_amount); 
    }

    if ($_POST['dispose'] == "true") {
        doDisposalOfItems($items);
        return;
    }

    if(empty($items->franchisee_id)){
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => "Please select franchisee"
        ));
        return;
    }

    $franchisee = getFranchise($items->franchisee_id);
    $deliveryCharge = 0;

    if ($items->mode_of_transaction == "Delivery") {
        $deliveryCharge =  $franchisee["delivery_charge"];
    }
    

    $id = $database->insert(ORDERS, array(
        "order_ref" => $refNo,
        "franchisee_id" => $items->franchisee_id,
        "total_amount" => ($total_amount - (is_numeric($items->discount_amount) ? $items->discount_amount : 0)) + $deliveryCharge,
        "added_by" => $loggedUser->username,
        "date_ordered" => $database->now(),
        "status" => "APPROVED",
        "discount_amount" => $items->discount_amount,
        "discount_voucher" => $items->discount_voucher,
        "due_date" => date('Y-m-d', strtotime("+1 day")),
        "delivery_charge" => $deliveryCharge,
        "delivery_method" => $items->mode_of_transaction
    ));

    $database->rawQuery("Update ".FRANCHISEE." set order_balance = order_balance + ".($total_amount + $deliveryCharge). ",
    total_amount = order_balance + billing_balance 
    where id = ".$items->franchisee_id);

    if($id){
        foreach($items->orders as $item){
            $database->insert(ORDER_ITEMS, array(
                "order_id" => $id,
                "order_ref" => $refNo,
                "item_id" => $item->item_id,
                "price" => $item->price,
                "qty" => $item->qty,
                "total_amount" => $item->total_amount
            ));
        }

        echo json_encode(Array (
            "type" => "success",
            "title" => "Successful!",
            "text" => "Successfully Placed your order please track order status!"
        ));
    }else{
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => $database->getLastError()
        ));
    }
}

function doDisposalOfItems($items) {

    global $database;
    global $loggedUser;

    $refNo = generateRefno($database, "1", ORDERS, "order_ref", "X-");

    $id = $database->insert(ORDERS, array(
        "order_ref" => $refNo,
        "franchisee_id" => NULL,
        "total_amount" => 0,
        "added_by" => $loggedUser->username,
        "date_ordered" => $database->now(),
        "status" => "DISPOSED"
    ));

    if($id){
        foreach($items->orders as $item){
            $database->insert(ORDER_ITEMS, array(
                "order_id" => $id,
                "order_ref" => $refNo,
                "item_id" => $item->item_id,
                "price" => $item->price,
                "qty" => $item->qty,
                "total_amount" => $item->total_amount
            ));

            //get items on product items
            //$database->where("batchno_", $item->batchno);
            $database->where("item_id", $item->item_id);
            $database->where("is_available" , 1);
            $itemize = $database->get(PRODUCT_ITEMIZE, $item->qty);

            //set product itemize with order ref
            foreach($itemize as $it){
                $database->where("id", $it['id']);
                $database->update(PRODUCT_ITEMIZE, array(
                    "is_available" => 0,
                    "order_ref" => $refNo
                ));
            }

            updateProductStocks($database, $item->item_id, $item->qty, "DISPOSAL (-)", $refNo, "-"); 
        }

        echo json_encode(Array (
            "type" => "success",
            "title" => "Successful!",
            "text" => "Successfully Disposed Items"
        ));
    }else{
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => $database->getLastError()
        ));
    }
}

if(isset($_GET["get"])){
    $primaryKey = 'id';
    $columns = array(
        array( 'db' => 'id', 'dt' => 0 ),
        array( 'db' => 'order_ref',  'dt' => 1 ),
        array(  'db' => 'franchisee_id',  
                'dt' => 2,
                'formatter' => function ($data, $row){

                    $franchisee = getFranchise($data);

                    return $franchisee["name"];
                }
        ),
        array( 'db' => 'total_amount',   'dt' => 3 , 'formatter' => function($data ,$row) { return number_format($data);}),
        array(  'db' => 'status',   
                'dt' => 4,
                'formatter' => function ($data, $row){

                    return convertStatusColor($data);
                }
            ),
        array( 'db' => 'added_by',   'dt' => 5 ),
        array( 'db' => 'date_ordered',   'dt' => 6 ),
        array( 'db' => 'due_date',   'dt' => 7 ),
        array( 'db' => 'remarks',   'dt' => 8 ),
        array(  'db' => 'id',   
                'dt' => 9 ,
                'formatter' => function($data ,$row) {
                    // $receiving  = "";

                    // if($row["status"] == "APPROVED"){
                    //     $receiving = '<li><a href="#" onclick="receiveItems('.$data.')">Receiving of Orders</a></li>';
                    // }

                    // return ' <div class="btn-group dropdown">
                    //             <button style="color:white;" aria-expanded="false" data-toggle="dropdown" class="btn bg-theme dropdown-toggle waves-effect waves-light" type="button">Take Action <span class="caret"></span></button>
                    //             <ul role="menu" class="dropdown-menu animated">
                    //                 <li><a href="#" onclick="loadOrderItems('.$data.')">View Order</a></li>
                    //                 '.$receiving.'
                    //             </ul>
                    //         </div>';

                    return '<button type="button" class="btn btn-primary" onclick="loadOrderItems('.$data.')"><i class="fa fa-eye m-r-5"></i> <span>VIEW ORDER</span></button>';
                }
            ),
    );
    
    $condition = "is_deleted = 0";

    if(isset($_GET['status'])){
        $condition .= " and status = '{$_GET['status']}'";
    }

    echo json_encode(
        SSPCustom::simpleCustom( $_GET, $sqlSSPDetails, ORDERS , $primaryKey, $columns, $condition )
    );
}

if(isset($_POST['previewOrder'])){
    $items = json_decode($_POST['previewOrder']);

    $response = array();

    foreach($items as $item){
        $product = getProduct($item);

        array_push($response, $product);
    }

    echo json_encode($response);
}

if(isset($_GET['viewOrderedItems'])){
    $order_id = $_GET['viewOrderedItems'];

    if(isset($_GET['order_ref'])){
        $database->where("order_ref", $_GET['order_ref']);
    }else{
        $database->where("id", $order_id);
    }
    $response = array();

    $orderInfo = $database->getOne(ORDERS);

    $franchisee = getFranchise($orderInfo["franchisee_id"]);

    $database->where("order_id", $orderInfo["id"]);
    $orders = $database->get(ORDER_ITEMS);
    
    foreach($orders as $item){
        $values["id"] = $item["id"];
        $values["order_id"] = $item["order_id"];
        $values["item_id"] = $item["item_id"];
        $values["remarks"] = $orderInfo["remarks"];
        $values["description"] = getProduct($item["item_id"])["description"];
        $values["franchisee_owner"] = $franchisee["name"];
        $values["franchisee_id"] = $franchisee["id"];
        $values["special_price_tag"] = $franchisee["special_price_tag"];
        $values["discount_voucher"] = $orderInfo["discount_voucher"];
        $values["discount_amount"] = $orderInfo["discount_amount"]; 
        $values["delivery_charge"] = $orderInfo["delivery_charge"];
        $values["status"] = $orderInfo["status"];

        $values["total_amount"] = $item["total_amount"];
        $values["qty"] = $item["qty"] + $item["rec_qty"];
        
        $values["price"] = $item["price"];
        $values["order_ref_no"] = $item["order_ref"];

        array_push($response, $values);
    }

    echo json_encode($response);
}

if(isset($_GET['cancelOrder'])){
    $order_ref = $_GET['cancelOrder'];
    $remarks = $_GET['remarks'];

    $database->where("order_ref", $order_ref);
    $order = $database->getOne(ORDERS);

    if($order["status"] == "RECEIVED" || $order["status"] == "Partial Received"){
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error",
            "text" => "You cannot cancel a RECEIVED order"
        ));
        return;
    }

    if($order["status"] == "APPROVED"){
        $database->rawQuery("Update ".FRANCHISEE." set order_balance = order_balance - ".$order['total_amount']. ", 
        total_amount = order_balance + billing_balance 
        where id = ".$order["franchisee_id"]);
    }

    if ($order['status'] == "DISPOSED") {
        // $database->where("order_ref", $order_ref);
        // $database->update(PRODUCT_ITEMIZE, array(
        //     "is_available" => 1,
        //     "order_ref" => NULL
        // ));

        echo json_encode(Array (
            "type" => "error",
            "title" => "Error",
            "text" => "This was already disposed"
        ));
        return;
    }

    $database->where("order_ref", $order_ref);
    $id = $database->update(ORDERS, array(
        "status" => "CANCELLED",
        "remarks" => $remarks
    ));

    if($id){
        echo json_encode(Array (
            "type" => "success",
            "title" => "Successful!",
            "text" => "You just cancelled your order"
        ));
    }
}

if(isset($_POST['receiveOrder'])){
    $data = json_decode($_POST['receiveOrder']);

    $database->where("order_ref", $data->info->order_ref);
    $checkOrder = $database->getOne(ORDERS);

    if($checkOrder["status"] == "RECEIVED"){
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error",
            "text" => "This order has already been received"
        ));

        return;
    }
   
    $database->where("order_ref", $data->info->order_ref);
    $id = $database->update(ORDERS, array(
        "status" => "RECEIVED",
        "rec_total_amount" => str_replace("," , "" , $data->info->overall_total),
        "remarks" => $data->info->remarks,
        "date_delivered" => $data->info->date_delivered
    ));

    if($id){
        foreach($data->orders as $order){
            $database->where("id", $order->id);
            $database->update(ORDER_ITEMS, array(
                "rec_qty" => $order->qty,
                "rec_total_amount" => $order->total_amount
            ));
        }

        echo json_encode(Array (
            "type" => "success",
            "title" => "Successful",
            "text" => "Successfully received order!"
        ));

    }else{
        echo json_encode(Array (
            "type" => "success",
            "title" => "Successful!",
            "text" => "Something went while receiving please try again later"
        ));
    }
}

if(isset($_GET['approveOrder'])){
    $orderData = json_decode($_GET['approveOrder']);

    $database->where("order_ref", $orderData->order_ref);
    $order = $database->getOne(ORDERS);

    $franchisee = getFranchise($order['franchisee_id']);

    if($order["status"] == "APPROVED"){
        
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error",
            "text" => "This is already approved"
        ));

        return;
    }

    if($order["status"] == "RECEIVED"){
        
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error",
            "text" => "This is already received"
        ));

        return;
    }

    $totalAmount = str_replace(',', '', $orderData->overall_total) + $franchisee["delivery_charge"];

    $database->where("order_ref", $orderData->order_ref);
    $update = $database->update(ORDERS, array(
        "status" => "APPROVED",
        "approved_by" => $loggedUser->username,
        "approved_date_time" => $database->now(),
        "discount_amount" => $orderData->discount_amount,
        "discount_voucher" => $orderData->discount_voucher,
        "total_amount" => $totalAmount,
        "due_date" => date('Y-m-d', strtotime("+1 day")),
        "delivery_charge" => $franchisee["delivery_charge"]
    ));

    if ($update) {

        foreach($orderData->orders as $item){
            $database->where("id", $item->item_id);
            $database->update(ORDER_ITEMS, array(
                "price" => $item->price,
                "qty" => $item->qty,
                "total_amount" => $item->total_amount
            ));
        }

        $database->rawQuery("Update ".FRANCHISEE." set order_balance = order_balance + ".$totalAmount. ",
        total_amount = order_balance + billing_balance 
        where id = ".$order["franchisee_id"]);

        $response = json_encode(array(
            "type" => "success",
            "title" => "Successful!",
            "text" => "Successfully Approved Order",
        ));
        echo $response;
    } else {
        $response = json_encode(array(
            "type" => "error",
            "title" => "Error!",
            "text" => "Error while approving order " . $database->getLastError(),
        ));
        echo $response;
    }

}