<?php

include "env.php";
include "env.".ENV.".php";

// TABLES
define("ACCESS_LEVELS", "access_levels");
define("ACCOUNTS", "accounts");
define("LOGS", "logs");
define("CART", "cart");
define("CATEGORY", "category");
define("SUB_CATEGORY", "sub_category");
define("BILLING_FRANCHISE", "billing_franchise");
define("BILLING_PAYMENTS", "billing_payments");
define("FRANCHISE_RENEWAL_TABLE", "franchise_renewal_log");
define("EMP_TABLE", "employees");
define("EXPENSES", "expenses");
define("PAYROLL", "payroll");
define("ATTENDANCE_TABLE", "attendance");
define("EMP_PAYROLL", "payroll_employee");
define("FRANCHISEE_ACCOUNTS", "franchise_accounts");
define("RENEWAL_LOG", "franchise_renewal_log");
define("FRANCHISE_BRANCH_TABLE", "franchise_branch");
define("COMMISSARY", "commissary_accounts");
define("ORDERS", "orders");
define("ORDER_ITEMS", "order_items");
define("ORDER_PAYMENTS", "order_payments");
define("FRANCHISEE", "franchisee");
define("ORDER_RETURNS", "order_returns");
define("ORDER_RETURN_ITEMS", "order_return_items");
define("PURCHASE_ORDER", "purchase_order");
define("PURCHASE_ORDER_ITEMS", "purchase_order_items");
define("SUPPLIER_TABLE","supplier");
define("RECEIVING_PO","received_po");
define("RECEIVING_PO_ITEMS","received_po_items");
define("PRODUCT_TABLE", "products");
define("PRODUCT_ITEMIZE", "products_itemize");
define("FORMULATION", "formulation");
define("FORMULATION_DETAILS","formulation_details");
define("PRODUCTION", "production");
define("PRODUCTION_ITEMS", "production_items");
define("PRODUCT_JOURNAL", "product_journal");
define("BRANCHES", "franchise_branch");
define("PURCHASE_ORDER_PAYMENTS", "purchase_order_payments");
define("PRODUCT_DISPOSAL", "disposal");
define("PRODUCT_DISPOSAL_ITEMS", "disposal_items");

function generateRefno($database, $initialCount, $table, $columnRef, $pattern){
    $data = $database->get($table);

    if(!empty($data)){
        $database->orderBy("id","DESC");
        $fetchedData = $database->getOne($table);

        $columnValue = $fetchedData[$columnRef];
        $toIncrement = substr($columnValue, 2);
        $toIncrement = $toIncrement + 1;

        return $pattern.str_pad($toIncrement, 4, '0', STR_PAD_LEFT);
    }else{
        return $pattern.str_pad($initialCount, 4, '0', STR_PAD_LEFT);
    }
    
}

function convertStatusColor($data){
    $color = "";
    if($data == "APPROVED" || $data == "PAID"){
        $color = '<span class="label label-success">' . $data . '</span>';
    }

    if ($data == "DISPOSED") {
        $color = '<span class="label label-info">' . $data . '</span>';
    }

    if($data == "REJECTED" || $data == "UNPAID" || $data == "CANCELLED"){
        $color = '<span class="label label-danger">' . $data . '</span>';
    }

    if($data == "PENDING"){
        $color = '<span class="label label-info">' . $data . '</span>';
    }

    if($data == "RECEIVED"){
        $color = '<span class="label label-warning">' . $data . '</span>';
    }

    if($data == "Delivery" || $data == "Pick-up" || strpos($data, "Partial") !== false){
        $color = '<span class="label label-info">' . $data . '</span>';
    }

    return $color;
}

function checkApproval($database, $status){
    $loggedUser = json_decode(getLoggedUserDetails($database));

    if($status != "PENDING"){
        $database->where("id", $loggedUser->role);
        $database->where("access_approval_power", 1);
        $access = $database->getOne(ACCESS_LEVELS);

        if(empty($access)){
            return "NO ACCESS";
        }else{
            return "ACCESS";
        }
    }

    return "PROCEED";
}

function getBatch($itemId, $orderRef) {
    global $database;

    return $database->rawQuery("SELECT batch_no FROM ".PRODUCT_ITEMIZE." WHERE order_ref = '".$orderRef."' and item_id = ".$itemId." group by batch_no");
}

function getSupplier($id){
    global $database;

    $database->where ("id", $id);
    return $database->getOne(SUPPLIER_TABLE);
}

function getProduct($id){
    global $database;

    $database->where ("id", $id);
    return $database->getOne(PRODUCT_TABLE);
}

function getAllProducts(){
    global $database;

    return $database->get(PRODUCT_TABLE);
}

function getFranchise($id){
    global $database;

    $database->where ("id", $id);
    return $database->getOne(FRANCHISEE);
}

function getOrderItems($id){
    global $database;

    $database->where ("id", $id);
    return $database->getOne(ORDER_ITEMS);
}

function getAllOrderItems($id) {
    global $database;

    $database->where("order_id", $id);
    return $database->get(ORDER_ITEMS);
}

function getOrderItemsByRefnoAndItemId($refNo, $itemId){
    global $database;

    $database->where ("order_ref", $refNo);
    $database->where ("item_id", $itemId);
    return $database->getOne(ORDER_ITEMS);
}

function getBranches(){
    global $database;

    $database->where("is_deleted", 0);
    return $database->get(FRANCHISE_BRANCH_TABLE);
}

function getBranch($id) {
    global $database;

    $database->where("id", $id);
    return $database->getOne(BRANCHES);
}

function number_to_word( $num = '' )
{
    $num    = ( string ) ( ( int ) $num );
   
    if( ( int ) ( $num ) && ctype_digit( $num ) )
    {
        $words  = array( );
       
        $num    = str_replace( array( ',' , ' ' ) , '' , trim( $num ) );
       
        $list1  = array('','one','two','three','four','five','six','seven',
            'eight','nine','ten','eleven','twelve','thirteen','fourteen',
            'fifteen','sixteen','seventeen','eighteen','nineteen');
       
        $list2  = array('','ten','twenty','thirty','forty','fifty','sixty',
            'seventy','eighty','ninety','hundred');
       
        $list3  = array('','thousand','million','billion','trillion',
            'quadrillion','quintillion','sextillion','septillion',
            'octillion','nonillion','decillion','undecillion',
            'duodecillion','tredecillion','quattuordecillion',
            'quindecillion','sexdecillion','septendecillion',
            'octodecillion','novemdecillion','vigintillion');
       
        $num_length = strlen( $num );
        $levels = ( int ) ( ( $num_length + 2 ) / 3 );
        $max_length = $levels * 3;
        $num    = substr( '00'.$num , -$max_length );
        $num_levels = str_split( $num , 3 );
       
        foreach( $num_levels as $num_part )
        {
            $levels--;
            $hundreds   = ( int ) ( $num_part / 100 );
            $hundreds   = ( $hundreds ? ' ' . $list1[$hundreds] . ' Hundred' . ( $hundreds == 1 ? '' : '' ) . ' ' : '' );
            $tens       = ( int ) ( $num_part % 100 );
            $singles    = '';
           
            if( $tens < 20 ) { $tens = ( $tens ? ' ' . $list1[$tens] . ' ' : '' ); } else { $tens = ( int ) ( $tens / 10 ); $tens = ' ' . $list2[$tens] . ' '; $singles = ( int ) ( $num_part % 10 ); $singles = ' ' . $list1[$singles] . ' '; } $words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_part ) ) ? ' ' . $list3[$levels] . ' ' : '' ); } $commas = count( $words ); if( $commas > 1 )
        {
            $commas = $commas - 1;
        }
       
        $words  = implode( ', ' , $words );
       
        $words  = trim( str_replace( ' ,' , ',' , ucwords( $words ) )  , ', ' );
        if( $commas )
        {
            $words  = str_replace( ',' , ' and' , $words );
        }
       
        return $words;
    }
    else if( ! ( ( int ) $num ) )
    {
        return 'Zero';
    }
    return '';
}

function checkAccessReport($role) {
    global $database;

    $database->where("id", $role);
    return $database->getOne(ACCESS_LEVELS);

}