<?php
require_once("config.php");
require_once("logs.php");

if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    if($data->formAction == "add"){
        $insertData = Array (
            "category_id" => $data->category_id,
            "sub_category_name" => $data->sub_category_name
        );  

        $id = $database->insert (SUB_CATEGORY, $insertData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Sub - Category added successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "edit"){

        $updateData = Array (
            "category_id" => $data->category_id,
            "sub_category_name" => $data->sub_category_name
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (SUB_CATEGORY, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Sub - Category details modified successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "delete"){

        $updateData = Array (
            "is_deleted" => 1
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (SUB_CATEGORY, $updateData);

        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Sub - Category deleted succesfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "add"){
        saveLog($database,"{$data->formAction} SUB - CATEGORY NAME: {$data->sub_category_name}");
    }else{
        saveLog($database,"{$data->formAction} SUB - CATEGORY ID {$data->modifyId}");
    }
}

//GET METHODS
if(isset($_GET["get"])){
    $database->where ('sc.is_deleted', 0);
    if(isset($_GET["category"])){
        $category_name = $_GET["category"];
        $database->where ('c.category_name', $category_name);
    }
    $database->join ("category c", "c.id=sc.category_id", "LEFT");
    $sub_category = $database->get("sub_category sc", null, "sc.*, c.category_name");
    echo json_encode($sub_category);
}

if(isset($_GET["getDetails"])){
    $id = $_GET["getDetails"];

    $database->where ("id", $id);
    $sub_category = $database->getOne(SUB_CATEGORY);
    echo json_encode($sub_category);
}
